function sixty_str,value,separator,sign=sign,precision=precision

if(n_params() eq 1) then separator=':'
if(n_elements(precision) ne 1) then precision=3

s_val=size(value)
result=strarr(n_elements(value))

format_last='(f0'+strcompress(string(precision+3,'(i)'),/remove)+'.'+strcompress(string(precision,'(i)'),/remove)+')'

for i=0,n_elements(value)-1 do begin
    six_arr=abs(sixty(value[i]))

    sign_str = (value[i] lt 0)? '-' : ((keyword_set(sign))? '+' : '')
    result[i]=sign_str+string(six_arr[0],format='(i2.2)')+separator+string(six_arr[1],format='(i2.2)')+separator+string(six_arr[2],format=format_last)
endfor

if(s_val[0] eq 0) then result=result[0]
return,result

end
