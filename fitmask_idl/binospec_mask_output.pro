pro binospec_mask_output,filename,mask_params,slits,guidestars,$
    observatory=observatory,b=b

if(n_elements(observatory) ne 1) then $
    observatory={lon:-110.884556d,lat:31.688778d,altitude:2600d}

tab=string(9B)

if(not keyword_set(b)) then b=(mask_params.side eq 'b')? 1 : 0 ;;; side identification

openw,u,filename,/get_lun
printf,u,'Mask1'+((keyword_set(b))? 'b' : 'a')
printf,u,''
printf,u,'Guide',tab,mask_params.guide
printf,u,'start',tab,date_conv(mask_params.mjdstart+2400000.5d,'STRING')
printf,u,'minutes',tab,strcompress(string(mask_params.duration,format='(i)'),/remove_all)
printf,u,'utc',tab,date_conv(mask_params.mjdstart+2400000.5d +mask_params.duration/60d/24d/2d,'STRING')
printf,u,'ra',tab,sixty_str(mask_params.ra/15d,precision=4)
printf,u,'dec',tab,sixty_str(mask_params.dec,precision=3)

binospec_expand_mask_params,mask_params

printf,u,'ha',tab,sixty_str(mask_params.ha/15d,precision=4)
printf,u,'airmass',tab,string(mask_params.airmass,format='(f6.3)')
printf,u,'parang',tab,string(mask_params.parang,format='(f8.3)')
printf,u,'usepar',tab,'1'
printf,u,'pa',tab,string(mask_params.posa,format='(f8.3)')
printf,u,'rotator',tab,string(mask_params.rotator,format='(f8.3)')
printf,u,'scale',tab,'1.000219'
printf,u,'wavelen',tab,string(mask_params.wavelength,format='(f9.3)')
printf,u,'arc2mm',tab,string(mask_params.arc2mm,format='(f6.3)')
printf,u,'boxw',tab,string(mask_params.boxw,format='(f6.3)')
printf,u,'boxh',tab,string(mask_params.boxh,format='(f6.3)')
printf,u,'boxw_mm',tab,string(mask_params.boxw*mask_params.arc2mm,format='(f6.3)')
printf,u,'boxh_mm',tab,string(mask_params.boxh*mask_params.arc2mm,format='(f6.3)')
printf,u,'label',tab,mask_params.label
printf,u,'inst',tab,'bino.mdf'
printf,u,'rank',tab,strcompress(string(mask_params.rank,format='(i)'),/remove_all)
printf,u,'filter',tab,mask_params.filter
printf,u,'grism',tab,mask_params.grism
printf,u,'telepoly',tab+strcompress(string(mask_params.telepoly,format='(g16.5)'),/remove),format='(10a)'
printf,u,'telerevs',tab+strcompress(string(mask_params.telerevs,format='(g16.5)'),/remove),format='(10a)'
printf,u,''
printf,u,''
printf,u,'GuideStars',tab,'ra',tab,'dec',tab,'x',tab,'y',tab,'xmask',tab,'ymask',tab,'mag',tab,'wfs'
printf,u,'GuideStars',tab,'--',tab,'---',tab,'-',tab,'-',tab,'-----',tab,'-----',tab,'---',tab,'---'

for i=0,n_elements(guidestars)-1 do begin
    printf,u,'GuideStars',$
        tab,sixty_str(guidestars[i].ra/15d,':',precision=4),$
        tab,sixty_str(guidestars[i].dec,':',precision=3),$
        tab,string(guidestars[i].x,format='(f8.3)'),$
        tab,string(guidestars[i].y,format='(f8.3)'),$
        tab,string(guidestars[i].xmask,format='(f8.3)'),$
        tab,string(guidestars[i].ymask,format='(f8.3)'),$
        tab,string(guidestars[i].mag,format='(f7.3)'),$
        tab,string(guidestars[i].wfs,format='(i3)'),format='(17a)'
endfor

printf,u,''
printf,u,''
printf,u,''
corners = mask_params.corners   ;;;[-40.1,-75.2,40.1,75.2] 
;;;;;;;;; dirty hack here
if(corners[0] gt 0 and (not keyword_set(b))) then corners[[0,2]]-=2.0*56.236
if(corners[0] lt 0 and (keyword_set(b))) then corners[[0,2]]+=2.0*56.236
printf,u,'corners',tab,strjoin(string(corners,format='(f8.3)'),' ')
printf,u,'axis',tab,strjoin(string((corners[0:1]+corners[2:3])/2d,format='(f8.3)'),' ')
printf,u,''
printf,u,'slit',tab,'ra',tab,'dec',tab,'x',tab,'y',tab,'target',tab,'type',tab,'object',tab,'wstart',tab,'wend',tab,'height',tab,'width',tab,'offset',tab,'theta',tab,'bbox',tab,'polygon',format='(31a)'
printf,u,'----',tab,'--',tab,'---',tab,'-',tab,'-',tab,'------',tab,'----',tab,'------',tab,'------',tab,'----',tab,'------',tab,'-----',tab,'------',tab,'-----',tab,'----',tab,'-------',format='(31a)'
for i=0,n_elements(slits)-1 do begin
    printf,u,$
        strcompress(string(slits[i].slit,format='(i)'),/remove_all),$
        tab,sixty_str(slits[i].ra/15d,':',precision=4),$
        tab,sixty_str(slits[i].dec,':',precision=3),$
        tab,string(slits[i].x,format='(f8.3)'),$
        tab,string(slits[i].y,format='(f8.3)'),$
        tab,slits[i].target,$
        tab,slits[i].type,$
        tab,slits[i].object,$
        tab,string(slits[i].wstart,format='(f8.3)'),$
        tab,string(slits[i].wend,format='(f8.3)'),$
        tab,string(slits[i].height,format='(f8.3)'),$
        tab,string(slits[i].width,format='(f8.3)'),$
        tab,string(slits[i].offset,format='(f8.3)'),$
        tab,string(slits[i].theta,format='(f8.3)'),$
        tab,strjoin(string(slits[i].bbox,format='(f8.3)'),' '),$
        tab,strjoin(string(slits[i].polygon,format='(f8.3)'),' '),format='(31a)'
endfor

close,u
free_lun,u

end
