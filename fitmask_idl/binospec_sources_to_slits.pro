function binospec_sources_to_slits,objlist,mask_params,field_geometry,$
    slit_geometry,gs_geometry=gs_geometry,box_geometry=box_geometry,$
    slits_a=slits_a,slits_b=slits_b,guidestars=guidestars,uselst=uselst,$
    wfs_mag_lim=wfs_mag_lim,gs_min=gs_min,verbose=verbose

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; objlist is an array of the object structure:
;     .target -- target ID (longint)
;     .object -- object names (string)
;     .ra -- J2000 R.A., degrees (double precision)
;     .dec -- J2000 Dec, degrees (double precision)
;     .pmra -- R.A. proper motion mas/year (double precision)
;     .pmdec -- Dec proper motion mas/year (double precision)
;     .mag -- magnitude (float)
;     .type -- 'TARGET', 'BOX', 'GUIDE' (both for guide and WFS stars)
;     .priority -- priority for targets, larger numbers higher priority (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; field_geometry is a structure:
;     .mask_a -- bounbox of side a in the focal plane, mm (float[4])
;     .mask_b -- bounbox of side a in the focal plane, mm (float[4])
;     .guide -- boundboxes of guide star areas, mm (float[4,N])
;     .wfs -- boundbox of the WFS region, mm (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; slit_geometry is a structure:
;     .height -- slit height in arcsec (float)
;     .width  -- slit height in arcsec (flaat)
;     .gap    -- minimal acceptable gap between slits in arcsec (float)
;
; box_geometry is a structure of the same type as slit_geometry, .gap from slit_geometry will be used
; gs_geometry is a structure of the same type as slit_geometry
; wfs_mag_lim is the faint mag limit for wfs stars (which is brighter
; than the general guide star limit)  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if(n_elements(box_geometry) ne 1) then box_geometry=slit_geometry
if(n_elements(gs_geometry) ne 1) then gs_geometry=slit_geometry
if(n_elements(wfs_mag_lim) ne 1) then wfs_mag_lim=15.0
if(n_elements(gs_min) ne 1) then gs_min=1
guide_limits={wfs_mag_lim:wfs_mag_lim, wfs_bright_lim:12.75,guide_mag_lim:18.5,guide_bright_lim:14.0, ng_limit:3}

binospec_expand_mask_params,mask_params
if(keyword_set(uselst)) then begin
    ; no proper motion correction possible
    ra_corr=objlist.ra
    dec_corr=objlist.dec
    lst=(mask_params.ha - mask_params.ra)/15d
    if(lst lt 0) then lst+=24d
    if(lst ge 24) then lst-=24d
    binospec_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,lst=lst,objx,objy
 endif else begin
    ; proper motion correction
    mjd_in=mask_params.mjdstart+mask_params.duration/60d/24d/2d
    j_now=(mjd_in-51544.4d)/365.25d + 2000d
    ra_corr=objlist.ra+objlist.pmra*(j_now-objlist.epoch)/cos(objlist.dec/!radeg)/(1000d*3600d) ;milliarsec/yr
    dec_corr=objlist.dec+objlist.pmdec*(j_now-objlist.epoch)/(3600d*1000d)
    binospec_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,$
        mjd=mjd_in,objx,objy
 endelse
 
 target_id = where(objlist.type eq 'TARGET' or objlist.type eq 'BOX' or $
                   objlist.type eq 'SKY' or objlist.type eq 'STANDARD',n_slits)
gs_id = where(objlist.type eq 'GUIDE',n_guidestars)
slits=replicate(binospec_slit_stub(),n_slits)
guidestars=replicate(binospec_guidestar_stub(),n_guidestars)

slits.x=objx[target_id]
slits.y=objy[target_id]
slits.target=objlist[target_id].target
slits.object=objlist[target_id].object
slits.type=objlist[target_id].type
slit_height=slit_geometry.height*mask_params.arc2mm
slit_width=slit_geometry.width*mask_params.arc2mm
box_id=where(slits.type eq 'BOX',cbox)
if(cbox gt 0) then begin
    slit_height[box_id]=box_geometry.height*mask_params.arc2mm
    slit_width[box_id]=box_geometry.width*mask_params.arc2mm
endif
slits.ra=ra_corr[target_id]
slits.dec=dec_corr[target_id]
slits.pmra=objlist[target_id].pmra
slits.pmdec=objlist[target_id].pmdec
slits.epoch=objlist[target_id].epoch
slits.height=slit_height
slits.width=slit_width
for i=0,n_slits-1 do slits[i].bbox=[slits[i].x,slits[i].y,slits[i].width,slits[i].height]
slits.mag=objlist[target_id].mag
slits.priority=objlist[target_id].priority

guidestars.ra=ra_corr[gs_id]
guidestars.dec=dec_corr[gs_id]
guidestars.x=objx[gs_id]
guidestars.xmask=guidestars.x
guidestars.y=objy[gs_id]
;for k=0, n_guidestars-1 do if objx[gs_id[k]] > 0 then guidestars[k].xmask= 1.000079d*(objx[gs_id[k]]-56.236)+56.2356 else guidestars[k].xmask = 1.000079d*(objx[gs_id[k]]+56.236)-56.2356  ;56.2356 factor, to be consistent with BinoMask treatment of slits 
guidestars.ymask=guidestars.y;4321.72d*asin(objy[gs_id]/4321.72d)
guidestars.mag=objlist[gs_id].mag
guidestars.pmra=objlist[gs_id].pmra
guidestars.pmdec=objlist[gs_id].pmdec
guidestars.epoch=objlist[gs_id].epoch
guidestars.object=objlist[gs_id].object


id_mask_a = where(slits.bbox[0]-slits.bbox[2]/2. gt field_geometry.mask_a[0] and $
                  slits.bbox[0]+slits.bbox[2]/2. lt field_geometry.mask_a[2] and $
                  slits.bbox[1]-slits.bbox[3]/2. gt field_geometry.mask_a[1] and $
                  slits.bbox[1]+slits.bbox[3]/2. lt field_geometry.mask_a[3], c_mask_a, /null)
;if(c_mask_a lt 1) then begin
;    if(keyword_set(verbose)) then message,'Mask A is empty!  Configuration rejected',/inf
;    return,-1
;endif

slits_a=slits[id_mask_a]
if c_mask_a ge 1 then begin
   slits_a.bbox[0]=slits_a.x-56.2356 ;1.000079d*(slits_a.x-(field_geometry.mask_a[0]+field_geometry.mask_a[2])/2.0)
   slits_a.bbox[1]=slits_a.y ;4321.7162d*asin(slits_a.y/4321.7162d)
   slits_a.slit=lindgen(c_mask_a)+1l
   for i=0,c_mask_a-1 do slits_a[i].polygon=[slits_a[i].bbox[0]-slits_a[i].width/2.,slits_a[i].bbox[1]-slits_a[i].height/2.,$
                 slits_a[i].bbox[0]-slits_a[i].width/2.,slits_a[i].bbox[1]+slits_a[i].height/2.,$
                 slits_a[i].bbox[0]+slits_a[i].width/2.,slits_a[i].bbox[1]+slits_a[i].height/2.,$
                 slits_a[i].bbox[0]+slits_a[i].width/2.,slits_a[i].bbox[1]-slits_a[i].height/2.]
endif

id_mask_b = where(slits.bbox[0]-slits.bbox[2]/2. gt field_geometry.mask_b[0] and $
                  slits.bbox[0]+slits.bbox[2]/2. lt field_geometry.mask_b[2] and $
                  slits.bbox[1]-slits.bbox[3]/2. gt field_geometry.mask_b[1] and $
                  slits.bbox[1]+slits.bbox[3]/2. lt field_geometry.mask_b[3], c_mask_b, /null)
if(c_mask_b lt 1) and (c_mask_a lt 1) then begin
    if(keyword_set(verbose)) then message,'Both Mask sides are empty!  Configuration rejected',/inf
    return,-1
endif
slits_b=slits[id_mask_b]
if c_mask_b ge 1 then begin
  slits_b.bbox[0]=slits_b.x+56.2356 ;1.000079d*(slits_b.x-(field_geometry.mask_b[0]+field_geometry.mask_b[2])/2.0)
  slits_b.bbox[1]=slits_b.y ;4321.7162d*asin(slits_b.y/4321.7162d)
  slits_b.slit=lindgen(c_mask_b)+1l
  for i=0,c_mask_b-1 do slits_b[i].polygon=[slits_b[i].bbox[0]-slits_b[i].width/2.,slits_b[i].bbox[1]-slits_b[i].height/2.,$
                 slits_b[i].bbox[0]-slits_b[i].width/2.,slits_b[i].bbox[1]+slits_b[i].height/2.,$
                 slits_b[i].bbox[0]+slits_b[i].width/2.,slits_b[i].bbox[1]+slits_b[i].height/2.,$
                 slits_b[i].bbox[0]+slits_b[i].width/2.,slits_b[i].bbox[1]-slits_b[i].height/2.]
endif

;new function to select the guide stars
guidestars=binospec_filter_guides(guidestars, mask_params,gs_geometry,field_geometry, guide_limits, gs_min,valid=valid, verbose=verbose)
if (valid eq 0) then return, -1
if c_mask_b eq 0 then pri=total(slits_a.priority) else if c_mask_a eq 0 then pri=total(slits_b.priority) else pri=total(slits_a.priority)+total(slits_b.priority)
return,pri

end
