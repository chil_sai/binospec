function binospec_slit_stub

return,{slit:0,ra:0d,dec:0d,x:0d,y:0d,target:0,object:'none',type:'TARGET',$
        wstart:4000.0,wend:6000.0,height:0.0,width:0.0,offset:0.0,theta:0.0,$
        bbox:dblarr(4),polygon:dblarr(8),mag:0.0,priority:0.0,$
        pmra:0.0,pmdec:0.0,epoch:2000.0}

end
