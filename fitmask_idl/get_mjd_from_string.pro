function get_mjd_from_string,inpstr

str_arr=strsplit(inpstr,/extract)

inpstr_mod=inpstr
if(n_elements(str_arr) eq 4 and $
    (str_arr[0] eq 'Jan') or $
    (str_arr[0] eq 'Feb') or $
    (str_arr[0] eq 'Mar') or $
    (str_arr[0] eq 'Apr') or $
    (str_arr[0] eq 'May') or $
    (str_arr[0] eq 'Jun') or $
    (str_arr[0] eq 'Jul') or $
    (str_arr[0] eq 'Aug') or $
    (str_arr[0] eq 'Sep') or $
    (str_arr[0] eq 'Oct') or $
    (str_arr[0] eq 'Nov') or $
    (str_arr[0] eq 'Dec')) then $
        inpstr_mod=str_arr[1]+'-'+str_arr[0]+'-'+str_arr[3]+' '+str_arr[2]

jd=date_conv(inpstr_mod,'J')

return,jd-2400000.5d

end
