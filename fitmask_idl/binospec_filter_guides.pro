;subroutine to binospec_sources_to_slits to compute valid guide/wfs
;stars for the mask
function binospec_remove_guides, guidestars, gs_boxh,gs_boxw,gs_box_sep, wfs_sep, verbose=verbose, ng_limit=ng_limit
  if not keyword_set(ng_limit) then ng_limit=3
  wfs=guidestars[where(guidestars.wfs EQ 10,n_wfs,/null)]
  unassigned=guidestars[where(guidestars.wfs EQ -99,/null)]
  guides=guidestars[where((guidestars.wfs ge 1) and (guidestars.wfs le 8), n_guide,/null)]
  if n_guide eq 0 then return, guidestars ;nothing to be done
  printf, -2, 'n_wfs_start:', n_wfs
  ;FIRST CLEAN WFS
  if n_wfs ge 1 then begin
    wfs_rank_idx = multisort(wfs.mag)
    wfs_clean_idx = !null
    while n_elements(wfs_rank_idx) gt 0 do begin
     cur_wfs = wfs[wfs_rank_idx[0]]
     if n_elements(wfs_rank_idx) eq 1 then begin
        others=!null
        ;just check against unassigned
        if unassigned eq !null then bcnt2=0 else ounassigned=where((sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt wfs_sep) and ((unassigned.mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt 0.8*wfs_sep and ((unassigned.mag-cur_wfs.mag) lt 3.5)),bcnt2,/null)
        if bcnt2 eq 0 then wfs_clean_idx=[wfs_clean_idx,wfs_rank_idx[0]] else begin
           cur_wfs.wfs=-99
           unassigned=[unassigned,cur_wfs]
        endelse
        wfs_rank_idx=others
        continue
     endif else others=wfs_rank_idx[1:*]
        overlaps=where((sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0) lt wfs_sep) and ((wfs[others].mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0) lt 0.8*wfs_sep and ((wfs[others].mag-cur_wfs.mag) lt 3.5)),bcnt,complement=nonoverlaps,/null)
                                ;also check if there are any
                                ;unassigned stars that are bright
                                ;enough to interfere
        if unassigned eq !null then bcnt2=0 else ounassigned=where((sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt wfs_sep) and ((unassigned.mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt 0.8*wfs_sep and ((unassigned.mag-cur_wfs.mag) lt 3.5)),bcnt2,/null)
        ;printf,-2,bcnt,bcnt2
        if (bcnt eq 0) and (bcnt2 eq 0) then wfs_clean_idx=[wfs_clean_idx,wfs_rank_idx[0]] else begin
           ;printf, -2, sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0)/.167
           cur_wfs.wfs=-99
           unassigned=[unassigned,cur_wfs]
        endelse
        ;wfs[others[overlaps]].wfs=-99
        ;unassigned=[unassigned,guides[others[overlaps]]] ;need to add these for later checking to be complete
        others=others[nonoverlaps]
        wfs_rank_idx=others
     endwhile
     wfs=wfs[wfs_clean_idx]
 endif                           ;end wfs clean
  printf, -2, 'n_wfs_end:', n_elements(wfs)
  if n_elements(wfs) eq 0 then return, [guides,unassigned]
  
  ;NOW CLEAN THE GUIDE STARS
  guide_rank_idx = multisort(guides.wfs,guides.mag)
  guide_clean_idx = !null

;loop until we've consumed all items in guide_rank_idx
  while n_elements(guide_rank_idx) gt 0 do begin
     cur_guide = guides[guide_rank_idx[0]]
     if n_elements(guide_rank_idx) eq 1 then begin
        others=!null
        ;just check against unassigned
        if unassigned eq !null then bnct2=0 else bounassigned=where((abs(cur_guide.x-unassigned.x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-unassigned.y) lt (gs_boxh/2.0+gs_boxh/3.)) and ((unassigned.mag-cur_guide.mag) lt 3.0),bcnt2,/null)
        if (bcnt2 eq 0) then guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]] else begin
           cur_guide.wfs=-99
           unassigned=[unassigned,cur_guide]
        endelse
        guide_rank_idx=others
        continue
     endif else others=guide_rank_idx[1:*]
     overlaps=where((abs(cur_guide.x-guides[others].x) lt (gs_box_sep/2.+gs_boxw/2.)) and (abs(cur_guide.y-guides[others].y) lt (gs_box_sep/2.+gs_boxh/2.)),ocnt,complement=nonoverlaps,/null)
     ;printf, -2, 'ocount',ocnt
     if ocnt gt 0 then begin
        ;we've found some overlaps. If any of these are within the
        ;primary guide's box, plus a width/3 buffer for dithering, and within 3mags, then we need to discard *both*
        badoverlaps=where((abs(cur_guide.x-guides[others].x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-guides[others].y) lt (gs_boxh/2.0+gs_boxh/3.0)) and ((guides[others].mag-cur_guide.mag) lt 3.0),bcnt,/null)
        ;also check if there are any unassigned stars that will fall into this guide box
        if unassigned eq !null then bnct2=0 else bounassigned=where((abs(cur_guide.x-unassigned.x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-unassigned.y) lt (gs_boxh/2.0+gs_boxh/3.0)) and ((unassigned.mag-cur_guide.mag) lt 3.0),bcnt2,/null)
        ;printf,-2,bcnt,bcnt2
        if (bcnt eq 0) and (bcnt2 eq 0) then guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]] else begin
           cur_guide.wfs=-99
           unassigned=[unassigned,cur_guide]
        endelse
        guides[others[overlaps]].wfs=-99
        unassigned=[unassigned,guides[others[overlaps]]] ;need to add these for later checking to be complete
        others=others[nonoverlaps]
        guide_rank_idx=others
     endif else begin
        guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]]
        guide_rank_idx=others
     endelse
  endwhile

if(keyword_set(verbose)) then message,/inf,string(n_guide-n_elements(guide_clean_idx),form='(i4)')+' overlapping guides removed'
;finally, only pick up to 3 brightest guide in each quadrant, ignore
;wfs=5 through 8, as they are already limited to 1
guides=guides[guide_clean_idx]
if n_elements(guides) gt 0 then begin
  for i=1,4 do begin
   this=where(guides.wfs eq i,cnt,/null)
   if cnt gt ng_limit then begin
      magsort=sort(guides[this].mag)
      ;printf,-2, guides[this].mag, guides[this[magsort[0:2]]].mag
      guides[this[magsort[ng_limit:*]]].wfs=-99
   endif
  endfor
endif
guidestars=[guides,wfs,unassigned]

return, guidestars

end

function binospec_filter_guides,guidestars, mask_params,gs_geometry,field_geometry, maglimits, gs_min, valid=valid, verbose=verbose, nofilter=nofilter

if tag_exist(maglimits, 'ng_limit') then ng_limit=maglimits.ng_limit else ng_limit=3
valid=1
id_guide_list=!NULL
gs_boxh=mask_params.arc2mm*gs_geometry.height
gs_boxw=mask_params.arc2mm*gs_geometry.width
gs_box_sep=mask_params.arc2mm*20. ; a gcam box readout is 20arcsec box, so keep other guide boxes out of this image window
;if gs_min eq 1 then wfs_sep=mask_params.arc2mm*60. else wfs_sep=mask_params.arc2mm*90.
wfs_sep=mask_params.arc2mm*60.

;1.5 arcmin radius should be clear at small mag differences
gapmm=gs_geometry.gap*mask_params.arc2mm

;TBD: not yet tested whether current WFS valid range is appropriate
;for star centers, or if a buffer is needed
id_wfs = where(guidestars.x gt field_geometry.wfs[0] and $
               guidestars.x lt field_geometry.wfs[2] and $
               guidestars.y gt field_geometry.wfs[1] and $
               guidestars.y lt field_geometry.wfs[3] and $
               guidestars.mag lt maglimits.wfs_mag_lim and guidestars.mag gt maglimits.wfs_bright_lim, c_wfs)
if(c_wfs gt 0) then begin
        guidestars[id_wfs].wfs=10
endif else begin
   if(keyword_set(verbose)) then message,'Warning: no WFS stars found!!! Configuration rejected',/inf
    valid=0
    return,-1
endelse


for i=0,n_elements(field_geometry.guide[0,*])-1 do begin
   
   id_guide = where(guidestars.xmask gt field_geometry.guide[0,i] and $
                     guidestars.xmask lt field_geometry.guide[2,i] and $
                     guidestars.ymask gt field_geometry.guide[1,i] and $
                     guidestars.ymask lt field_geometry.guide[3,i] and $
                     guidestars.mag lt maglimits.guide_mag_lim and $
                     guidestars.mag gt maglimits.guide_bright_lim, c_guide)
    if(c_guide gt 0) then begin
        ;id_guide_list=[id_guide_list,id_guide]
        guidestars[id_guide].wfs=(i/2)+1 ;each guide quadrant now has 2 boxes in the field_geometry file, to avoid guides shining on the flexure ccd
    endif
 endfor
;printf, -2, field_geometry.guide
        ;next we throw out any stars that overlap with the bar code region
        bc_a=where((guidestars.wfs eq 3) and (guidestars.y-gs_box_sep/2. lt -103.675) and (guidestars.x+gs_box_sep/2. gt 92.975), bca_cnt)
        if bca_cnt ge 1 then begin
           guidestars[bc_a].wfs=-99
           ;for k=0,bca_cnt-1 do id_guide_list=id_guide_list[where(id_guide_list ne bc_a[k])]
        endif
        bc_b=where((guidestars.wfs eq 2) and (guidestars.y+gs_box_sep/2. gt 103.475) and (guidestars.x-gs_box_sep/2. lt -92.475), bcb_cnt)
        if bcb_cnt ge 1 then begin
           guidestars[bc_b].wfs=-99
           ;for k=0,bcb_cnt-1 do id_guide_list=id_guide_list[where(id_guide_list ne bc_b[k])]
        endif

        ;REMOVE ANY OVERLAPS
        if not keyword_set(nofilter) then guidestars=binospec_remove_guides(guidestars,gs_boxh,gs_boxw,gs_box_sep,wfs_sep, ng_limit=ng_limit, verbose=verbose)
        if n_elements(where(guidestars.wfs eq 10, /null)) eq 0 then begin
           valid=0
           return,-1
        endif
        id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 4),nguide,/null)
        ;printf,-2,guidestars[id_guide_list].wfs
        
        ;test if we need to go into single-star mode, only if gs_min eq 1
        if gs_min eq 1 then begin
           if nguide eq 0 then test_list=[-1] else test_list=guidestars[id_guide_list].wfs
           ;;; counting guide stars in each guide box, if any
           guide_num = histogram([test_list],bins=1,min=1,max=4)
           ;if gcam 1 is empty
           if (guide_num[0]+guide_num[2] eq 0) then begin
              printf, -2, 'guide 1 or 3'
              for i=0,7,4 do begin
                 id_guide = where(guidestars.xmask gt field_geometry.guide[0,i] and $
                     guidestars.xmask lt field_geometry.guide[2,i] and $
                     guidestars.ymask gt field_geometry.guide[1,i] and $
                     guidestars.ymask lt field_geometry.guide[3,i+1] and $
                     guidestars.mag lt maglimits.guide_mag_lim and $
                     guidestars.mag gt maglimits.guide_bright_lim, c_guide,/NULL) ;set the top bound to define the larger guide box 
                 if (c_guide gt 0) then begin
                     ;check again for conflict with barcode
                     bc_a=where((guidestars.wfs eq 3) and (guidestars.y-gs_box_sep/2. lt -103.675) and (guidestars.x+gs_box_sep/2. gt 92.975), bca_cnt)
                     if bca_cnt ge 1 then for k=0,bca_cnt-1 do id_guide=id_guide[where(id_guide ne bc_a[k],/NULL)]
                     c_guide=n_elements(id_guide)
                     if c_guide gt 0 then begin
                        ;id_guide_list=[id_guide_list,id_guide[0]]
                        guidestars[id_guide].wfs=(i/2)+1+4;add 4 to the number, so we can filter out any overlaps again before deciding on the one we want
                     endif
                 endif
              endfor
           endif
           ;if gcam2 is empty
           if (guide_num[1]+guide_num[3] eq 0) then begin
              printf, -2, 'guide 2 or 4'
              for i=2,7,4 do begin
                 id_guide = where(guidestars.xmask gt field_geometry.guide[0,i] and $
                     guidestars.xmask lt field_geometry.guide[2,i] and $
                     guidestars.ymask gt field_geometry.guide[1,i] and $
                     guidestars.ymask lt field_geometry.guide[3,i+1] and $
                     guidestars.mag lt maglimits.guide_mag_lim and $
                     guidestars.mag gt maglimits.guide_bright_lim, c_guide,/NULL) ;set the top bound to define the larger guide box 
                 if(c_guide gt 0) then begin
                    ;check again for conflict with barcode
                    bc_b=where((guidestars.wfs eq 2) and (guidestars.y+gs_box_sep/2. gt 103.475) and (guidestars.x-gs_box_sep/2. lt -92.475),bcb_cnt)
                    if bcb_cnt ge 1 then for k=0,bcb_cnt-1 do id_guide=id_guide[where(id_guide ne bc_b[k],/NULL)]
                    c_guide=n_elements(id_guide)
                     if c_guide gt 0 then begin
                      ;id_guide_list=[id_guide_list,id_guide[0]]
                      guidestars[id_guide[0]].wfs=(i/2)+1+4
                      ;break
                     endif
                 endif
              endfor
           endif
           if not keyword_set(nofilter) then guidestars=binospec_remove_guides(guidestars,gs_boxh,gs_boxw,gs_box_sep,wfs_sep, verbose=verbose, ng_limit=ng_limit)
        if n_elements(where(guidestars.wfs eq 10, /null)) eq 0 then begin
           valid=0
           return,-1
        endif
           id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 8),nguide,/null)
           if nguide eq 0 then test_list=[-1] else test_list=guidestars[id_guide_list].wfs
           ;after removing dups, pick *only* one for each gcam 
           guide_num = histogram([test_list],bins=1,min=1,max=8)
           ;printf,-2, guide_num
           if (guide_num[5]+guide_num[7] gt 0) then begin
              id_guide=where(guidestars.wfs eq 6 or guidestars.wfs eq 8, cnt)
              guidestars[id_guide[0]].wfs-=4
              if cnt gt 1 then guidestars[id_guide[1:*]].wfs=-99
           endif
           if (guide_num[4]+guide_num[6] gt 0) then begin
              id_guide=where(guidestars.wfs eq 5 or guidestars.wfs eq 7, cnt)
              guidestars[id_guide[0]].wfs-=4
              if cnt gt 1 then guidestars[id_guide[1:*]].wfs=-99
           endif   
        endif                   ;END gs_min=1 case

        id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 4),nguide,/null)
        if(nguide le 1) then begin
           if(keyword_set(verbose)) then message,'Warning: no guide stars found!!! Configuration rejected',/inf
           valid=0
           return,-1
        endif else begin
        ;;; counting guide stars in each guide box
           guide_num = histogram(guidestars[id_guide_list].wfs,bins=1,min=1,max=4)
           printf, -2, 'guide_num_final:',guide_num ;, guidestars[id_guide_list].wfs
           if((guide_num[0]+guide_num[2] lt gs_min) or $
              (guide_num[1]+guide_num[3] lt gs_min)) then begin
              if(keyword_set(verbose)) then message,'Warning: guide stars found but not in the right guiding areas. Configuration rejected',/inf
              valid=0
              return,-1
           endif
           
           id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 10),nguide,/null)
           valid=1
           return, guidestars[id_guide_list]
        endelse
     end
