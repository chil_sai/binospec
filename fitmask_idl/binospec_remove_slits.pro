function binospec_remove_slits, slits, mask_params, slit_geometry, verbose=verbose, optimize_type=optimize_type

ycent = (mask_params.corners[1]+mask_params.corners[3])/2d
xcent = (mask_params.corners[0]+mask_params.corners[2])/2d
gapmm=slit_geometry.gap*mask_params.arc2mm
;printf, -2, gapmm, slit_geometry.gap, mask_params.arc2mm, 'gap'
if (not keyword_set(optimize_type)) then slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.x-xcent))) else begin
   case optimize_type of
      1: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.x-xcent)))
      2: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.y-ycent)))
      3: slits_rank_idx = reverse(multisort(slits.priority,randomu(seed,n_elements(slits.priority))))
      else: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.x-xcent)))
   endcase
endelse

n_slits=n_elements(slits)

slits_clean_idx = [slits_rank_idx[0]]

for i=1l,n_slits-1l do begin
    cur_slit = slits[slits_rank_idx[i]]
    r_edge1 = slits[slits_clean_idx].bbox[1]-slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]+cur_slit.bbox[3]/2.0)
    bad_r_edge1=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] le 0, cbad_r_edge1)
    if(cbad_r_edge1 gt 0) then r_edge1[bad_r_edge1]=!values.f_nan
    l_edge1 = -(slits[slits_clean_idx].bbox[1]+slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]-cur_slit.bbox[3]/2.0))
    bad_l_edge1=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] ge 0, cbad_l_edge1)
    if(cbad_l_edge1 gt 0) then l_edge1[bad_l_edge1]=!values.f_nan
    r_edge2 = slits[slits_clean_idx].bbox[1]-slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]-cur_slit.bbox[3]/2.0)
    bad_r_edge2=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] le 0, cbad_r_edge2)
    if(cbad_r_edge2 gt 0) then r_edge2[bad_r_edge2]=!values.f_nan
    l_edge2 = -(slits[slits_clean_idx].bbox[1]+slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]+cur_slit.bbox[3]/2.0))
    bad_l_edge2=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] ge 0, cbad_l_edge2)
    if(cbad_l_edge2 gt 0) then l_edge2[bad_l_edge2]=!values.f_nan
    if((min(r_edge1,/nan) gt gapmm or finite(min(r_edge1,/nan)) ne 1) and $
       (min(l_edge1,/nan) gt gapmm or finite(min(l_edge1,/nan)) ne 1) and $
       (min(r_edge2,/nan) gt gapmm or finite(min(r_edge2,/nan)) ne 1) and $
       (min(l_edge2,/nan) gt gapmm or finite(min(l_edge2,/nan)) ne 1)) then slits_clean_idx=[slits_clean_idx,slits_rank_idx[i]]
endfor

if(keyword_set(verbose)) then message,/inf,string(n_slits-n_elements(slits_clean_idx),form='(i4)')+' overlapping slits removed'

return,slits[slits_clean_idx[sort(slits_clean_idx)]]

end
