function binospec_mask_optimize,objlist,mask_params,field_geometry,$
    slit_geometry,gs_geometry=gs_geometry,rotator_limits=rotator_limits,$
    posa_arr=posa_arr,ra_arr=ra_arr,dec_arr=dec_arr,$
    mask_params_out=mask_params_out,slits_a=slits_a,slits_b=slits_b,guidestars=guidestars,$
    merit_arr=merit_arr,uselst=uselst,gs_min=gs_min,verbose=verbose, optimize_type=optimize_type,obstype=obstype, extend_slits=extend_slits

if(n_elements(posa_arr) eq 0) then posa_arr=-179.0+findgen(360)
if(n_elements(ra_arr) eq 0) then ra_arr=[mask_params.ra]
if(n_elements(dec_arr) eq 0) then dec_arr=[mask_params.dec]
if(n_elements(rotator_limits) ne 2) then rotator_limits=[-160.0,170.0]  

n_pa=n_elements(posa_arr)
n_ra=n_elements(ra_arr)
n_dec=n_elements(dec_arr)

merit_arr=fltarr(n_pa,n_ra,n_dec)

for x=0,n_ra-1 do begin
    for y=0,n_dec-1 do begin
        for p=0,n_pa-1 do begin
            mask_params_cur=mask_params
            mask_params_cur.ra=ra_arr[x]
            mask_params_cur.dec=dec_arr[y]
            mask_params_cur.posa=posa_arr[p]
            binospec_expand_mask_params,mask_params_cur,rotator_range=rotator_range
            if(binospec_check_rotator(rotator_range,rotator_limits) ne 1) then begin
               merit_arr[p,x,y]=-1.0
               continue
            endif
            if obstype ne 'imaging' then begin
              b=binospec_sources_to_slits(objlist,mask_params_cur,field_geometry,$
                slit_geometry,gs_geometry=gs_geometry, $
                slits_a=slits_a,slits_b=slits_b,guidestars=guidestars,uselst=uselst,gs_min=gs_min,verbose=verbose)
              if(b lt 0) then begin
                 merit_arr[p,x,y]=-2.0
                 continue
              endif
              numa=n_elements(slits_a)
              numb=n_elements(slits_b)
              mask_params_cur.corners=field_geometry.mask_a
              if numa ge 1 then slits_a_clean=binospec_remove_slits(slits_a,mask_params_cur,slit_geometry,verbose=verbose, optimize_type=optimize_type) else slits_a_clean=!null
              mask_params_cur.corners=field_geometry.mask_b
              if numb ge 1 then slits_b_clean=binospec_remove_slits(slits_b,mask_params_cur,slit_geometry,verbose=verbose, optimize_type=optimize_type) else slits_b_clean=!null
              mask_params_cur.corners=field_geometry.mask_a
              if numa eq 0 then pri=total(slits_b_clean.priority) else if numb eq 0 then pri=total(slits_a_clean.priority) else pri=total(slits_a_clean.priority)+total(slits_b_clean.priority)
              merit_arr[p,x,y]=pri
           endif else begin
                b=binospec_guides_only(objlist,mask_params_cur,field_geometry,$
                  gs_geometry=gs_geometry, guidestars=guidestars,$
                  uselst=uselst,gs_min=gs_min,verbose=verbose)
              if(b lt 0) then continue
              merit_arr[p,x,y]=1.0
           endelse
        endfor
    endfor
endfor

m=max(merit_arr,idxm)
;printf,-2, n_elements(where(merit_arr eq 1.0))
;printf, -2, merit_arr
if(m le 0.0) then begin
    message,/inf,'No objects could be placed!'
    return,-1
endif
idx=array_indices(merit_arr,idxm)
if(n_elements(idx) ne 3) then idx=[idx,0l]
if(n_elements(idx) ne 3) then idx=[idx,0l]
mask_params_out=mask_params
mask_params_out.posa=posa_arr[idx[0]]
mask_params_out.ra=ra_arr[idx[1]]
mask_params_out.dec=dec_arr[idx[2]]
if obstype ne 'imaging' then begin
  b=binospec_sources_to_slits(objlist,mask_params_out,field_geometry,$
    slit_geometry,gs_geometry=gs_geometry,$
    slits_a=slits_a,slits_b=slits_b,guidestars=guidestars, gs_min=gs_min)
  numa=n_elements(slits_a)
  numb=n_elements(slits_b)
  if numa ge 1 then slits_a=binospec_remove_slits(slits_a,mask_params_out,slit_geometry, optimize_type=optimize_type) else slits_a=!null
  mask_params_out.corners=field_geometry.mask_b
  if numb ge 1 then slits_b=binospec_remove_slits(slits_b,mask_params_out,slit_geometry, optimize_type=optimize_type) else slits_b=!null
            if numa eq 0 then m_val=total(slits_b_clean.priority) else if numb eq 0 then m_val=total(slits_a_clean.priority) else m_val=total(slits_a_clean.priority)+total(slits_b_clean.priority)
            if keyword_set(extend_slits) then begin
               if slits_a ne !null then binospec_extend_slits, slits_a, slit_geometry=slit_geometry, mask_params=mask_params
               if slits_b ne !null then binospec_extend_slits, slits_b, slit_geometry=slit_geometry, mask_params=mask_params
            endif
  endif else begin
  b=binospec_guides_only(objlist,mask_params_out,field_geometry,$
                  gs_geometry=gs_geometry, guidestars=guidestars,$
                  uselst=uselst,gs_min=gs_min,verbose=verbose)
  return, 1.0
endelse

;print,'Best parameters (R.A., Dec, PA):',mask_params_out.ra,mask_params_out.dec,mask_params_out.posa
;print,'Merit function:',m_val

return,m_val
end
