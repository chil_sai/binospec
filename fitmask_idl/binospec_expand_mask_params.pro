;;; Computes hour angle, parallactic angle, airmass, and rotator position.
;;; The existing structure fields are populated with the computed values

pro binospec_expand_mask_params,mask_params,observatory,rotator_range=rotator_range

if(n_params() eq 1) then $
    observatory={lon:-110.884556d,lat:31.688778d,altitude:2600d}

eq2hor,mask_params.ra,mask_params.dec,mask_params.mjdstart+2400000.5d +mask_params.duration/60d/24d/2d,$
    alt,az,ha,lat=observatory.lat,lon=observatory.lon,altitude=observatory.altitude
airmass=1d/sin(alt/!radeg)
ha_start=ha-mask_params.duration/60d/2d*15d
ha_end=ha+mask_params.duration/60d/2d*15d

parang=-!radeg*atan(-sin([ha_start,ha,ha_end]/!radeg),$
                  cos(mask_params.dec/!radeg)*tan(observatory.lat/!radeg)-$
                  sin(mask_params.dec/!radeg)*cos([ha_start,ha,ha_end]/!radeg))

mask_params.ha = (ha gt 180.0 ? ha-360. : ha)
mask_params.ha = ((mask_params.ha lt (-180.0)) ? mask_params.ha+360. : mask_params.ha)
mask_params.airmass = airmass
mask_params.parang = parang[1]
mask_params.rotator = (-mask_params.posa+parang[1])  - 360.0*(fix(parang[1]-mask_params.posa)/180)

rotator_range=(parang-mask_params.posa) - 360.0*(fix(parang-mask_params.posa)/180)
printf,-2,rotator_range
printf, -2, parang
end
