function binospec_read_field_geometry,filename
tab=string(9B)

f_conf=read_asc(filename,/str,pattern=tab)

guide_reg=where(f_conf[0,*] eq 'guide',n_guide)
mask_a_reg=where(f_conf[0,*] eq 'mask_a')
mask_b_reg=where(f_conf[0,*] eq 'mask_b')
wfs_reg=where(f_conf[0,*] eq 'wfs')
field_geometry={$
    mask_a:double(strsplit(f_conf[1,mask_a_reg],/extr)),$
    mask_b:double(strsplit(f_conf[1,mask_b_reg],/extr)),$
    wfs:double(strsplit(f_conf[1,wfs_reg],/extr)),$
    guide:dblarr(4,n_guide)}

for i=0,n_guide-1 do $
    field_geometry.guide[*,i]=double(strsplit(f_conf[1,guide_reg[i]],/extr))

return,field_geometry
end
