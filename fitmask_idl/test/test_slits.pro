tab=string(9B)
gs_list=read_asc('test/kurtz1.guides',12,/str,pattern=tab)
t_list=read_asc('test/kurtz1.targets',2,/str,pattern=tab,/preserve_null)
n_t=n_elements(t_list[0,*])
n_gs=n_elements(gs_list[0,*])
field_geometry = binospec_read_field_geometry('config/field_geometry.cfg')


objlist=replicate($
    {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,mag:-1.0,type:'TARGET',priority:0.0},$
    n_t+n_gs)

objlist[0:n_t-1].target=transpose(long(t_list[0,*]))
objlist[0:n_t-1].object=strcompress('o'+transpose(t_list[0,*]),/remove_all)
objlist[0:n_t-1].ra=double(transpose(t_list[1,*]))*15d
objlist[0:n_t-1].dec=double(transpose(t_list[2,*]))
objlist[0:n_t-1].mag=float(transpose(t_list[6,*]))
objlist[0:n_t-1].type='TARGET'
objlist[0:n_t-1].priority=1.0

objlist[n_t:*].target=100001l +lindgen(n_gs)
objlist[n_t:*].object='guide'
for i=0l,n_gs-1l do begin
    get_coords,instr=gs_list[0,i]+'  '+gs_list[1,i],crdtmp
    objlist[n_t+i].ra=crdtmp[0]*15d
    objlist[n_t+i].dec=crdtmp[1]
endfor
objlist[n_t:*].mag=float(transpose(gs_list[3,*]))
objlist[n_t:*].type='GUIDE'
objlist[n_t:*].priority=0.0

mask_params=binospec_mask_input('../test_data/kurtz1_a.msk')
mask_params.corners=field_geometry.mask_a

slit_geometry={height:6.0,width:0.5,gap:0.75}
gs_geometry={height:10.0,width:10.0,gap:0.75}

b=binospec_mask_optimize(objlist,mask_params,field_geometry,$
        slit_geometry,gs_geometry=gs_geometry,$
        slits_a=slits_a_clean,slits_b=slits_b_clean,guidestars=guidestars,$
;        posa_arr=-179.0,$
;        ra_arr=mask_params.ra,$
;        dec_arr=mask_params.dec,$
        posa_arr=-179.0+findgen(360),$
        ra_arr=mask_params.ra+0.015d*(dindgen(5)-2d),$
        dec_arr=mask_params.dec+0.015d*(dindgen(5)-2d),$
        mask_params_out=mask_params_out,merit_arr=merit_arr,/verbose)

;;binospec_mask_to_ds9reg,mask_params_cur,slits_a,slits_b,guidestars,field_geometry,'test/test.reg'
binospec_mask_to_ds9reg,mask_params_out,slits_a_clean,slits_b_clean,guidestars,field_geometry,'test/test_clean.reg'

binospec_mask_output,'test/test_a_6.msk',mask_params_out,slits_a_clean,guidestars
binospec_mask_output,'test/test_b_6.msk',mask_params_out,slits_b_clean,guidestars,/b

end
