ta=read_asc('../test_data/kurtz1_a.msk',50,/str) 
tb=read_asc('../test_data/kurtz1_b.msk',50,/str) 

x0a=double(transpose(ta[3,*]))
y0a=double(transpose(ta[4,*]))

x0b=double(transpose(tb[3,*]))
y0b=double(transpose(tb[4,*]))

rda=dblarr(2,n_elements(x0a))
rdb=dblarr(2,n_elements(x0b))

for i=0,n_elements(ta[0,*])-1 do begin
    get_coords,instr=ta[1,i]+' '+ta[2,i],crdtmp
    ra=crdtmp[0]*15d
    dec=crdtmp[1]
;    precess,ra,dec,2013.5,2000.
    rda[*,i]=[ra,dec]
endfor

for i=0,n_elements(tb[0,*])-1 do begin
    get_coords,instr=tb[1,i]+' '+tb[2,i],crdtmp
    ra=crdtmp[0]*15d
    dec=crdtmp[1]
;    precess,ra,dec,2013.5,2000.
    rdb[*,i]=[ra,dec]
endfor

racen=179.99290d
deccen=9.9982775d
;precess,racen,deccen,2013.5,2000.
rdcen=[racen,deccen]
pa=-179d

mjd_hecto=get_mjd_from_string('Dec  7 12:07:30 2016')
mjd_bino=get_mjd_from_string('Jun 16 04:40:52 2013')
;lst_hecto=rdcen[0]/15d +ten(-2,10,04) +7.5/60d
;lst_bino=ten(14,45,58) 
binospec_radec2xy,transpose(rda[0,*]),transpose(rda[1,*]),rdcen[0],rdcen[1],pa,xa,ya ,mjd=mjd_bino ;lst=lst_bino
binospec_radec2xy,transpose(rdb[0,*]),transpose(rdb[1,*]),rdcen[0],rdcen[1],pa,xb,yb ,mjd=mjd_bino ;lst=lst_bino
binospec_radec2xy,transpose(rda[0,*]),transpose(rda[1,*]),rdcen[0],rdcen[1],pa,xah,yah ,mjd=mjd_hecto, lst=lst_h
binospec_radec2xy,transpose(rdb[0,*]),transpose(rdb[1,*]),rdcen[0],rdcen[1],pa,xbh,ybh ,mjd=mjd_hecto,/nomask

tha=read_asc('../test_data/k_a.out',36,/str)
tha=tha[*,where(fix(tha[0,*]) gt 0)]
idxa=transpose(fix(tha[0,*])-1l)
cidxa=n_elements(idxa)

thb=read_asc('../test_data/k_b.out',36,/str)
thb=thb[*,where(fix(thb[0,*]) gt 0)]
idxb=transpose(fix(thb[0,*])-1l)
cidxb=n_elements(idxb)

rota=(187.89d)/!radeg
xha= double(tha[3,*])*cos(rota) - double(tha[4,*])*sin(rota)
yha= double(tha[3,*])*sin(rota) + double(tha[4,*])*cos(rota)
xhb= double(thb[3,*])*cos(rota) - double(thb[4,*])*sin(rota)
yhb= double(thb[3,*])*sin(rota) + double(thb[4,*])*cos(rota)


;!p.font=1
;!x.thick=3
;!y.thick=3
;!p.thick=3
;set_plot,'ps'
;device,filename='test/bino_pos_comparison.eps',/col,/encap,xsize=20,ys=18
plot,xa,ya,/iso,psym=4,xs=1,ys=1,xr=[-100,100],yr=[-90,90],chars=1.5,xtitle='X, mm',ytitle='Y, mm'
oplot,xb,yb,psym=4
oplot,x0a,y0a,col=254,psym=1 
oplot,x0b,y0b,col=254,psym=1 

oplot,xha,yha,col=40,psym=6 
oplot,xhb,yhb,col=40,psym=6,thick=3 

scl=1000.0
for i=0,n_elements(xa)-1 do arrow,/data,xa[i],ya[i],xa[i]+(x0a[i]-xa[i])*scl,ya[i]+(y0a[i]-ya[i])*scl,col=128,hsize=10.
for i=0,n_elements(xb)-1 do arrow,/data,xb[i],yb[i],xb[i]+(x0b[i]-xb[i])*scl,yb[i]+(y0b[i]-yb[i])*scl,col=128,hsize=10.
for i=0,n_elements(idxa)-1 do $
    arrow,/data,xa[idxa[i]],yah[idxa[i]],xah[idxa[i]]+(xha[i]-xah[idxa[i]])*scl,yah[idxa[i]]+(yha[i]-yah[idxa[i]])*scl,col=40,hsi=10.
for i=0,n_elements(idxb)-1 do $
    arrow,/data,xb[idxb[i]],ybh[idxb[i]],xbh[idxb[i]]+(xhb[i]-xbh[idxb[i]])*scl,ybh[idxb[i]]+(yhb[i]-ybh[idxb[i]])*scl,col=40,hsi=10.,thick=3
arrow,/data,0,0,0,0.0167*scl,col=80,thick=3,hsize=10.
xyouts,/data,5,0,'0.1 arcsec',ori=90.0,chars=1.5,col=80
print,'Side B RMS(x,y,d):',sqrt(total((xhb-xbh[idxb])^2)/double(cidxb)),sqrt(total((yhb-ybh[idxb])^2)/double(cidxb)),$
          sqrt(total((xhb-xbh[idxb])^2+(yhb-ybh[idxb])^2)/double(cidxb))
;device,/close
;set_plot,'x'
!p.font=-1
!x.thick=0
!y.thick=0
!p.thick=0
end
