function binospec_mask_input,filename,slits=slits,guidestars=guidestars
    tab=string(9B)

    openr,u,filename,/get_lun
    str=''
    count_lines=0ll
    count_hdr=0ll
    count_guide=0ll

    slits_str=['']
    guidestars_str=['']
    slitflag=0

    mask_params={ $
        side:'a',$
        label:'noname',$
        guide:'Ok',$
        mjdstart:0d,$
        duration:0d,$
        ra:0d,$
        dec:0d,$
        ha:0d,$
        airmass:0d,$
        parang:0d,$
        posa:0d,$
        rotator:0d,$
        wavelength:5000d,$
        arc2mm:0d,$
        boxw:0d,$
        boxh:0d,$
        corners:dblarr(4),$
        inst:'',$
        rank:1,$
        filter:'',$
        grism:'',$
        telepoly:dblarr(7),$
        telerevs:dblarr(7) $
    }

    WHILE ~ EOF(u) DO BEGIN 
        readf,u,str
        count_lines=count_lines+1
        str_arr=strsplit(str,tab,/extr)
        if((n_elements(str_arr) eq 1) and (strmid(str,0,5) eq 'Mask1')) then mask_params.side=strmid(str,5,1)
        if(str_arr[0] eq 'start') then mask_params.mjdstart=get_mjd_from_string(str_arr[1])
        if(str_arr[0] eq 'Guide') then mask_params.guide=str_arr[1]
        if(str_arr[0] eq 'minutes') then mask_params.duration=double(str_arr[1])
        if(str_arr[0] eq 'ra') then begin
            get_coords,instr=str_arr[1]+'  00:00:00',crdtmp
            mask_params.ra=crdtmp[0]*15d
        endif
        if(str_arr[0] eq 'dec') then begin
            get_coords,instr='00:00:00  '+str_arr[1],crdtmp
            mask_params.dec=crdtmp[1]
        endif
        if(str_arr[0] eq 'pa') then mask_params.posa=double(str_arr[1])
        if(str_arr[0] eq 'wavelen') then mask_params.wavelength=double(str_arr[1])
        if(str_arr[0] eq 'arc2mm') then mask_params.arc2mm=double(str_arr[1])
        if(str_arr[0] eq 'boxw') then mask_params.boxw=double(str_arr[1])
        if(str_arr[0] eq 'boxh') then mask_params.boxh=double(str_arr[1])
        if(str_arr[0] eq 'label') then mask_params.label=str_arr[1]
        if(str_arr[0] eq 'inst') then mask_params.inst=str_arr[1]
        if(str_arr[0] eq 'rank') then mask_params.rank=fix(str_arr[1])
        if(str_arr[0] eq 'filter') then mask_params.filter=str_arr[1]
        if(str_arr[0] eq 'grism') then mask_params.grism=str_arr[1]
        if(str_arr[0] eq 'telepoly') then mask_params.telepoly=double(str_arr[1:*])
        if(str_arr[0] eq 'telerevs') then mask_params.telerevs=double(str_arr[1:*])

        if(str_arr[0] eq 'corners') then mask_params.corners=double((strsplit(str_arr[1],/extr)))

        if(str_arr[0] eq 'GuideStars') then begin
            if(strmid(strjoin(str_arr[1:*],''),0,2) ne '--') then $
                guidestars_str=[guidestars_str,str]
        endif
        if(str_arr[0] eq 'slit' or slitflag eq 1) then begin
            slitflag=1
            if(strmid(strjoin(str_arr[1:*],''),0,2) ne '--') then $
                slits_str=[slits_str,str]
        endif
    endwhile
    close,u
    free_lun,u

    guidestars_str=guidestars_str[1:*]
    slits_str=slits_str[1:*]
    
    hdr_guide=guidestars_str[0]
    gs_hdr_fields=(strcompress(strsplit(hdr_guide,tab,/extract,/preserve_null),/remove_all))[1:*]
    n_gs_fields=n_elements(gs_hdr_fields)
    gs_idx_ra=where(gs_hdr_fields eq 'ra')
    gs_idx_dec=where(gs_hdr_fields eq 'dec')
    gs_idx_x=where(gs_hdr_fields eq 'x')
    gs_idx_y=where(gs_hdr_fields eq 'y')
    gs_idx_xmask=where(gs_hdr_fields eq 'xmask')
    gs_idx_ymask=where(gs_hdr_fields eq 'ymask')
    gs_idx_mag=where(gs_hdr_fields eq 'mag')
    gs_idx_wfs=where(gs_hdr_fields eq 'wfs')

    n_guidestars=n_elements(guidestars_str)-1l
    gs_data=strarr(n_gs_fields,n_guidestars)
    for i=0l,n_guidestars-1l do gs_data[*,i]=(strsplit(guidestars_str[i+1],tab,/extract,/preserve_null))[1:*]

    guidestars=replicate(binospec_guidestar_stub(),n_guidestars)
    if(gs_idx_x      ge 0) then guidestars.x=transpose(float(gs_data[gs_idx_x,*]))
    if(gs_idx_y      ge 0) then guidestars.y=transpose(float(gs_data[gs_idx_y,*]))
    if(gs_idx_xmask  ge 0) then guidestars.xmask=transpose(float(gs_data[gs_idx_xmask,*]))
    if(gs_idx_ymask  ge 0) then guidestars.ymask=transpose(float(gs_data[gs_idx_ymask,*]))
    if(gs_idx_mag    ge 0) then guidestars.mag=transpose(float(gs_data[gs_idx_mag,*]))
    if(gs_idx_wfs    ge 0) then guidestars.wfs=transpose(float(gs_data[gs_idx_wfs,*]))

    for i=0,n_guidestars-1 do begin
        get_coords,instr=gs_data[gs_idx_ra[0],i]+' '+gs_data[gs_idx_dec[0],i],crdtmp
        guidestars[i].ra=crdtmp[0]*15d
        guidestars[i].dec=crdtmp[1]
    endfor


    hdr_slits=slits_str[0]
    hdr_fields=strcompress(strsplit(hdr_slits,tab,/extract,/preserve_null),/remove_all)
    n_fields=n_elements(hdr_fields)
    idx_slit=where(hdr_fields eq 'slit')
    idx_ra=where(hdr_fields eq 'ra')
    idx_dec=where(hdr_fields eq 'dec')
    idx_x=where(hdr_fields eq 'x')
    idx_y=where(hdr_fields eq 'y')
    idx_target=where(hdr_fields eq 'target')
    idx_object=where(hdr_fields eq 'object')
    idx_type=where(hdr_fields eq 'type')
    idx_wstart=where(hdr_fields eq 'wstart')
    idx_wend=where(hdr_fields eq 'wend')
    idx_height=where(hdr_fields eq 'height')
    idx_width=where(hdr_fields eq 'width')
    idx_offset=where(hdr_fields eq 'offset')
    idx_theta=where(hdr_fields eq 'theta')
    idx_bbox=where(hdr_fields eq 'bbox')
    idx_polygon=where(hdr_fields eq 'polygon')

    n_slits=n_elements(slits_str)-1l
    slit_data=strarr(n_fields,n_slits)
    for i=0l,n_slits-1l do slit_data[*,i]=strsplit(slits_str[i+1],tab,/extract,/preserve_null)

    slits=replicate(binospec_slit_stub(),n_slits)
    if(idx_slit   ge 0) then slits.slit=transpose(fix(slit_data[idx_slit,*]))
    if(idx_x      ge 0) then slits.x=transpose(float(slit_data[idx_x,*]))
    if(idx_y      ge 0) then slits.y=transpose(float(slit_data[idx_y,*]))
    if(idx_target ge 0) then slits.target=transpose(fix(slit_data[idx_target,*]))
    if(idx_object ge 0) then slits.object=transpose(slit_data[idx_object,*])
    if(idx_type   ge 0) then slits.type=transpose(slit_data[idx_type,*])
    if(idx_wstart ge 0) then slits.wstart=transpose(double(slit_data[idx_wstart,*]))
    if(idx_wend   ge 0) then slits.wend=transpose(double(slit_data[idx_wend,*]))
    if(idx_height ge 0) then slits.height=transpose(float(slit_data[idx_height,*]))
    if(idx_width  ge 0) then slits.width=transpose(float(slit_data[idx_width,*]))
    if(idx_offset ge 0) then slits.offset=transpose(float(slit_data[idx_offset,*]))
    if(idx_theta  ge 0) then slits.theta=transpose(float(slit_data[idx_theta,*]))

    for i=0,n_slits-1 do begin
        slits[i].bbox=double(strsplit(slit_data[idx_bbox,i],/extract))
        slits[i].polygon=double(strsplit(slit_data[idx_polygon,i],/extract))
        get_coords,instr=slit_data[idx_ra[0],i]+' '+slit_data[idx_dec[0],i],crdtmp
        slits[i].ra=crdtmp[0]*15d
        slits[i].dec=crdtmp[1]
    endfor

    binospec_expand_mask_params,mask_params
    return,mask_params
end
