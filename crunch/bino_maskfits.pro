function bino_maskfits, mask_id, filename
  if mask_id lt 1 then return, 1
  if isa(filename,/string) and filename ne '' then begin
     if not file_test(filename) then begin
        extension=1
     endif else begin
        extension=8
        if not file_test(filename, /write) then begin
           print, 'file exists but is not writeale'
           return, 1
        endif
     endelse
     
;side 1
st='select masks.pk as mask_id, masks.mask_name,long(masks.center_coords) * 180. / pi() as centerra, lat(masks.center_coords) * 180. / pi() as centerdec, masks.pa as centerpa, masks.design_wavecen,masks.design_grism as design_grating,masks.design_ha, (masks.obswindow_start + (masks.obswindow_end - masks.obswindow_start) / 2)::varchar as design_time, masks.design_airmass,masks.guidebox_width,masks.guidebox_len,instrument_rev.arc2mm as mm_per_arcsec, (instrument_rev.mask_corners_a)::varchar as mask_corners,(instrument_rev.guide_a1)::varchar as guide_area1a, (instrument_rev.guide_a2)::varchar as guide_area2a,(instrument_rev.guide_b1)::varchar as guide_area1b, (instrument_rev.guide_b2)::varchar as guide_area2b,(instrument_rev.wfs)::varchar,targs.side_parity,targs.centerxy[0] as slitx,targs.centerxy[1] as slity,targs.target_name, targs.target_pk as target_id,long(targs.coord2000) * 180. / pi() as ra, lat(targs.coord2000) * 180. / pi() as dec,  (targs.slitxy)::varchar as slitxy, targs.slit_pa, targs.slit_length, targs.slit_width,targs.mag FROM binospec_ops.masks, (binospec_ops.target_assignments LEFT JOIN binospec_ops.targets ON target_assignments.target_pk=targets.pk) as targs,binospec_ops.instrument_rev WHERE masks.pk='+strtrim(string(mask_id),2)+' and targs.design_id=masks.sidea_design_id and masks.inst_rev_id=instrument_rev.pk and targs.centerxy[1] < 75.0 and targs.centerxy[1] > -75.0'


dat=pgsql_query(st, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

if isa(dat, 'STRUCT') then begin
  numa=fix(n_elements(dat))
  ttype=strarr(numa)
  ttype[*]='TARGET'
endif else numa=0

;side 2
st='select masks.pk as mask_id, masks.mask_name,long(masks.center_coords) * 180. / pi() as centerra, lat(masks.center_coords) * 180. / pi() as centerdec, masks.pa as centerpa, masks.design_wavecen,masks.design_grism as design_grating,masks.design_ha, (masks.obswindow_start + (masks.obswindow_end - masks.obswindow_start) / 2)::varchar as design_time, masks.design_airmass,masks.guidebox_width,masks.guidebox_len,instrument_rev.arc2mm as mm_per_arcsec, (instrument_rev.mask_corners_b)::varchar as mask_corners,(instrument_rev.guide_a1)::varchar as guide_area1a, (instrument_rev.guide_a2)::varchar as guide_area2a,(instrument_rev.guide_b1)::varchar as guide_area1b, (instrument_rev.guide_b2)::varchar as guide_area2b,(instrument_rev.wfs)::varchar,targs.side_parity,targs.centerxy[0] as slitx,targs.centerxy[1] as slity,targs.target_name,targs.target_pk as target_id,long(targs.coord2000) * 180. / pi() as ra, lat(targs.coord2000) * 180. / pi() as dec,  (targs.slitxy)::varchar as slitxy, targs.slit_pa, targs.slit_length, targs.slit_width,targs.mag FROM binospec_ops.masks, (binospec_ops.target_assignments LEFT JOIN binospec_ops.targets ON target_assignments.target_pk=targets.pk) as targs,binospec_ops.instrument_rev WHERE masks.pk='+strtrim(string(mask_id),2)+' and targs.design_id=masks.sideb_design_id and masks.inst_rev_id=instrument_rev.pk and targs.centerxy[1] < 75.0 and targs.centerxy[1] > -75.0'

datb=pgsql_query(st, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

if isa(datb, 'STRUCT') then begin
   numb=fix(n_elements(datb))
   ttypeb=strarr(numb)
   ttypeb[*]='TARGET'
endif else numb=0

;guides and wfs
g_a='select guide_assignments.centerxy[0] as guidex,guide_assignments.centerxy[1] as guidey,guide_targets.gtarget_name,guide_targets.pk as gtarget_id,guide_assignments.gcam_side,long(guide_targets.coord2000) * 180. / pi() as ra, lat(guide_targets.coord2000) * 180. / pi() as dec,  (guide_assignments.slitxy)::varchar as slitxy,guide_targets.mag FROM binospec_ops.masks, binospec_ops.guide_assignments, binospec_ops.guide_targets WHERE masks.pk='+strtrim(string(mask_id),2)+' and guide_assignments.design_id=masks.sidea_design_id and guide_assignments.gtarget_pk=guide_targets.pk'
dat_ga=pgsql_query(g_a, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

g_b='select guide_assignments.centerxy[0] as guidex,guide_assignments.centerxy[1] as guidey,guide_targets.gtarget_name,guide_targets.pk as gtarget_id,guide_assignments.gcam_side,long(guide_targets.coord2000) * 180. / pi() as ra, lat(guide_targets.coord2000) * 180. / pi() as dec,  (guide_assignments.slitxy)::varchar as slitxy, guide_targets.mag FROM binospec_ops.masks, binospec_ops.guide_assignments, binospec_ops.guide_targets WHERE masks.pk='+strtrim(string(mask_id),2)+' and guide_assignments.design_id=masks.sideb_design_id and guide_assignments.gtarget_pk=guide_targets.pk'
dat_gb=pgsql_query(g_b, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

wfs='select wfs_assignments.wfs_xy[0] as wfsx,wfs_assignments.wfs_xy[1] as wfsy,guide_targets.gtarget_name,guide_targets.pk as gtarget_id,long(guide_targets.coord2000) * 180. / pi() as ra, lat(guide_targets.coord2000) * 180. / pi() as dec, guide_targets.mag FROM binospec_ops.masks, binospec_ops.wfs_assignments, binospec_ops.guide_targets WHERE masks.pk='+strtrim(string(mask_id),2)+' and wfs_assignments.mask_pk=masks.pk and wfs_assignments.gtarget_pk=guide_targets.pk'
dat_wfs=pgsql_query(wfs, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

if isa(dat_ga, 'STRUCT') then begin
   gtype=strarr(n_elements(dat_ga))
   gtype[*]='GSTAR'+strtrim(dat_ga.gcam_side,2)+'A'
   numga=fix(n_elements(dat_ga))
endif else numga=0
if isa(dat_gb, 'STRUCT') then begin
   gtype2=strarr(n_elements(dat_gb))
   gtype2[*]='GSTAR'+strtrim(dat_gb.gcam_side,2)+'B'
   numgb=fix(n_elements(dat_gb))
endif else numgb=0
if isa(dat_wfs, 'STRUCT') then begin
   wtype=strarr(n_elements(dat_wfs))
   wtype[*]='WFS'
   numw=fix(n_elements(dat_wfs))
endif else numw=0
numg=numga+numgb
numgw=numg+numw

;common info for sides a+b
mask_corners=fltarr(4)
mask_corners_b=fltarr(4)
guide_areaa=fltarr(4,2)
guide_areab=fltarr(4,2)
wfs=fltarr(4)
if numa gt 0 then mask_ref=dat[0] else if numb gt 0 then mask_ref=datb[0] else return, 0 ;if neither numa nor numb, don't write any extensions
  tmp=((mask_ref.mask_corners).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  mask_corners[*]=float(tmp3)
  if numa eq 0 then begin
     mask_corners_b=mask_corners
     minXc=min(mask_corners[[0,2]],max=maxXc)
     mask_corners[0]=-maxXc
     mask_corners[2]=-minXc
  endif
  if numb gt 0 then begin
    tmp=((datb[0].mask_corners).split(','))
    tmp2=tmp.replace('(','')
    tmp3=tmp2.replace(')','')
    mask_corners_b[*]=float(tmp3)
  endif else begin 
    mask_corners_b=mask_corners
    minXc=min(mask_corners[[0,2]],max=maxXc)
    mask_corners_b[0]=-maxXc
    mask_corners_b[2]=-minXc
  endelse
  tmp=((mask_ref.guide_area1a).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  guide_areaa[*,0]=float(tmp3)
  tmp=((mask_ref.guide_area2a).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  guide_areaa[*,1]=float(tmp3)
  tmp=((mask_ref.guide_area1b).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  guide_areab[*,0]=float(tmp3)
  tmp=((mask_ref.guide_area2b).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  guide_areab[*,1]=float(tmp3)
  tmp=((mask_ref.wfs).split(','))
  tmp2=tmp.replace('(','')
  tmp3=tmp2.replace(')','')
  wfs[*]=float(tmp3)
  timestamptovalues, mask_ref.design_time.replace(' ','T')+'Z', day=day, hour=hour,minute=minute,month=month,second=second,year=year
mjd=julday(month,day,year,hour,minute,second)-2400000.5d

;arrays for side a
if numa+numga+numw gt 0 then polyx=fltarr(numa+numga+numw,4) else polyxy=!null
polyy=polyx
if numa gt 0 then begin
  slit_id=indgen(numa)+1
  slit_x=float(dat.slitx)
  slit_y=float(dat.slity)
  slit_width=float(dat.slit_width)
  slit_length=float(dat.slit_length)
  slit_pa=float(dat.slit_pa)
  target_name=dat.target_name
  target_type=ttype
  target_id=dat.target_id
  ra=dat.ra
  dec=dat.dec
  mag=dat.mag
endif else begin
  slit_id=!null
  slit_x=!null
  slit_y=!null
  slit_width=!null
  slit_length=!null
  slit_pa=!null
  target_name=!null
  target_type=!null
  target_id=!null
  ra=!null
  dec=!null
  mag=!null
endelse
for n=0, numa-1 do begin
   tmp=((dat[n].slitxy).split(','))
   tmp2=tmp.replace('(','')
   tmp3=tmp2.replace(')','')
   polyx[n,*]=tmp3[[0,2,4,6]]
   polyy[n,*]=tmp3[[1,3,5,7]]
endfor
if numga gt 0 then begin
   for n=0, numga-1 do begin
      tmp=((dat_ga[n].slitxy).split(','))
      tmp2=tmp.replace('(','')
      tmp3=tmp2.replace(')','')
      polyx[n+numa,*]=tmp3[[0,2,4,6]]
      polyy[n+numa,*]=tmp3[[1,3,5,7]]
   endfor
   slit_id=[slit_id,intarr(numga)]
   slit_x=[slit_x,dat_ga.guidex]
   slit_y=[slit_y,dat_ga.guidey]
   slit_width=[slit_width,fltarr(numga)+mask_ref.guidebox_width]
   slit_length=[slit_length,fltarr(numga)+mask_ref.guidebox_len]
   slit_pa=[slit_pa,fltarr(numga)]
   target_name=[target_name,dat_ga.gtarget_name]
   target_type=[target_type,gtype]
   target_id=[target_id,dat_ga.gtarget_id]
   ra=[ra,dat_ga.ra]
   dec=[dec,dat_ga.dec]
   mag=[mag,dat_ga.mag]
endif
if numw gt 0 then begin
   for n=0, numw-1 do begin
      polyx[n+numa+numga,*]=replicate(dat_wfs[n].wfsx,4)
      polyy[n+numa+numga,*]=replicate(dat_wfs[n].wfsy,4)
   endfor
   slit_id=[slit_id,intarr(numw)]
   slit_x=[slit_x,dat_wfs.wfsx]
   slit_y=[slit_y,dat_wfs.wfsy]
   slit_width=[slit_width,fltarr(numw)]
   slit_length=[slit_length,fltarr(numw)]
   slit_pa=[slit_pa,fltarr(numw)]
   target_name=[target_name,dat_wfs.gtarget_name]
   target_type=[target_type,wtype]
   target_id=[target_id,dat_wfs.gtarget_id]
   ra=[ra,dat_wfs.ra]
   dec=[dec,dat_wfs.dec]
   mag=[mag,dat_wfs.mag]
endif

   fits_tab={INST:'bino', MASK_ID:fix(mask_ref.mask_id), MASK_NAME:mask_ref.mask_name, CENTERRA:mask_ref.centerra, CENTERDEC:mask_ref.centerdec, CENTERPA:mask_ref.centerpa, DESIGN_WAVECEN:mask_ref.design_wavecen, DESIGN_GRATING:mask_ref.design_grating, DESIGN_TIME:mask_ref.design_time.replace(' ','T')+'Z', DESIGN_MJD:mjd, DESIGN_HA:float(mask_ref.design_ha), MASK_SIDE:'a', MASK_CORNERS:mask_corners,GUIDE_AREA:guide_areaa,WFS_AREA:wfs,NTARGETS:numa,NGUIDES:numga,NWFS:numw,MM_PER_ARCSEC:mask_ref.mm_per_arcsec,SLIT_ID:slit_id,SLITX:slit_x, SLITY:slit_y, SLIT_WIDTH:slit_width, SLIT_LENGTH:slit_length, SLIT_PA:slit_pa,POLY_X:polyx,POLY_Y:polyy,TARGET_NAME:target_name,TARGET_TYPE:target_type, TARGET_ID:target_id, RA:ra, DEC:dec, MAG:mag}

;write fits table
mwrfits, fits_tab, filename
fxhmodify,  filename,'EXTNAME','BINOMASK1',extension=extension
endif

;arrays for side b
if numb+numgb+numw gt 0 then polyx=fltarr(numb+numgb+numw,4) else polyxy=!null
polyy=polyx
if numb gt 0 then begin
  slit_id=indgen(numb)+1
  slit_x=float(datb.slitx)
  slit_y=float(datb.slity)
  slit_width=float(datb.slit_width)
  slit_length=float(datb.slit_length)
  slit_pa=float(datb.slit_pa)
  target_name=datb.target_name
  target_type=ttypeb
  target_id=datb.target_id
  ra=datb.ra
  dec=datb.dec
  mag=datb.mag
endif else begin
  slit_id=!null
  slit_x=!null
  slit_y=!null
  slit_width=!null
  slit_length=!null
  slit_pa=!null
  target_name=!null
  target_type=!null
  target_id=!null
  ra=!null
  dec=!null
  mag=!null
endelse
for n=0, numb-1 do begin
   tmp=((datb[n].slitxy).split(','))
   tmp2=tmp.replace('(','')
   tmp3=tmp2.replace(')','')
   polyx[n,*]=tmp3[[0,2,4,6]]
   polyy[n,*]=tmp3[[1,3,5,7]]
endfor
if numgb gt 0 then begin
   for n=0, numgb-1 do begin
      tmp=((dat_gb[n].slitxy).split(','))
      tmp2=tmp.replace('(','')
      tmp3=tmp2.replace(')','')
      polyx[n+numb,*]=tmp3[[0,2,4,6]]
      polyy[n+numb,*]=tmp3[[1,3,5,7]]
   endfor
   slit_id=[slit_id,intarr(numgb)]
   slit_x=[slit_x,dat_gb.guidex]
   slit_y=[slit_y,dat_gb.guidey]
   slit_width=[slit_width,fltarr(numgb)+mask_ref.guidebox_width]
   slit_length=[slit_length,fltarr(numgb)+mask_ref.guidebox_len]
   slit_pa=[slit_pa,fltarr(numgb)]
   target_name=[target_name,dat_gb.gtarget_name]
   target_type=[target_type,gtype2]
   target_id=[target_id,dat_gb.gtarget_id]
   ra=[ra,dat_gb.ra]
   dec=[dec,dat_gb.dec]
   mag=[mag,dat_gb.mag]
endif
if numw gt 0 then begin
   for n=0, numw-1 do begin
      polyx[n+numb+numgb,*]=replicate(dat_wfs[n].wfsx,4)
      polyy[n+numb+numgb,*]=replicate(dat_wfs[n].wfsy,4)
   endfor
   slit_id=[slit_id,intarr(numw)]
   slit_x=[slit_x,dat_wfs.wfsx]
   slit_y=[slit_y,dat_wfs.wfsy]
   slit_width=[slit_width,fltarr(numw)]
   slit_length=[slit_length,fltarr(numw)]
   slit_pa=[slit_pa,fltarr(numw)]
   target_name=[target_name,dat_wfs.gtarget_name]
   target_type=[target_type,wtype]
   target_id=[target_id,dat_wfs.gtarget_id]
   ra=[ra,dat_wfs.ra]
   dec=[dec,dat_wfs.dec]
   mag=[mag,dat_wfs.mag]
endif

   fits_tab={INST:'bino', MASK_ID:fix(mask_ref.mask_id), MASK_NAME:mask_ref.mask_name, CENTERRA:mask_ref.centerra, CENTERDEC:mask_ref.centerdec, CENTERPA:mask_ref.centerpa, DESIGN_WAVECEN:mask_ref.design_wavecen, DESIGN_GRATING:mask_ref.design_grating, DESIGN_TIME:mask_ref.design_time.replace(' ','T')+'Z', DESIGN_MJD:mjd, DESIGN_HA:float(mask_ref.design_ha), MASK_SIDE:'b', MASK_CORNERS:mask_corners_b,GUIDE_AREA:guide_areab,WFS_AREA:wfs,NTARGETS:numb,NGUIDES:numgb,NWFS:numw,MM_PER_ARCSEC:mask_ref.mm_per_arcsec,SLIT_ID:slit_id,SLITX:slit_x, SLITY:slit_y, SLIT_WIDTH:slit_width, SLIT_LENGTH:slit_length, SLIT_PA:slit_pa,POLY_X:polyx,POLY_Y:polyy,TARGET_NAME:target_name,TARGET_TYPE:target_type, TARGET_ID:target_id, RA:ra, DEC:dec, MAG:mag}


mwrfits, fits_tab, filename
fxhmodify,  filename,'EXTNAME','BINOMASK2',extension=(extension+1)

return, 0
end
