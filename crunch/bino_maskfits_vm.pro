pro bino_maskfits_vm
  ;mask_id=command_line_args()
  filename=command_line_args()
  hdr=headfits(filename, ext=7, errmsg=err) ;only do this if 8 extensions present
  ;print, err
  if (err eq '') then begin
    mname=sxpar(hdr,'MASK')
    disperse1=sxpar(hdr,'DISPERS1')
    disperse2=sxpar(hdr,'DISPERS2')
    ;print, mname,disperse1,disperse2
    ;if (disperse1 ne 'mirror') OR (disperse2 ne 'mirror') then begin
    if strtrim(mname) ne 'imaging' then begin
      query="select masks.pk  FROM binospec_ops.masks WHERE masks.mask_name='"+strtrim(mname)+"'"
      result=pgsql_query(query, connect_info='host=localhost user=bino password=safT1st dbname=binospec',status=status)
      if status eq 0 then begin
        mask_id=fix(result.pk)
        try=bino_maskfits(mask_id,filename)
     endif
   endif
  endif
end
