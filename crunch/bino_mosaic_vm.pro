pro bino_mosaic_vm
  args=command_line_args()
  if n_elements(args) ne 2 then begin
     print, 'needs input filename and output filename'
     exit, status=1
  endif
  filename=args[0]
  outname=args[1]
  bino_mosaic, filename, outname,/sub
end
