function bino_fix_header, filename
  ;if mask_id lt 1 then return, 1
  if isa(filename,/string) and filename ne '' then begin
     if not file_test(filename) then begin
        print, 'file not found'
        return, 1
     endif else begin
        if not file_test(filename, /write) then begin
           print, 'file exists but is not writeale'
           return, 1
        endif
     endelse
  endif else begin
     print, 'filename invalid'
     return, 1
  endelse
  if strmid(filename, 0,10) ne '/data/ccd/' then begin
     print, 'please pass a /data/ccd/ filename, updated fits header will go in /data/archive versions'
     return, 1
  endif else outfile='/data/archive/'+strmid(filename,10)
  ;load first extension header
  hdr=headfits(filename, ext=1)
image_table_query="select maskpk::varchar,progpk::varchar,maskname,cattargetid::varchar from binospec_ops.images where fitsfile='"+strtrim(filename,2)+"';"     
dat=pgsql_query(image_table_query, connect_info='host=localhost user=bino password=safT1st dbname=binospec')

if isa(dat, 'STRUCT') then begin
   if fix(n_elements(dat)) eq 1 then begin
      ;ok to continue
      if dat.maskpk ne '' then mask_id=long(dat.maskpk) else mask_id=!null
      if dat.progpk ne '' then prog_id=long(dat.progpk) else prog_id=!null
      ;print, mask_id, prog_id
      ;if this is blank, look it up using maskname
      if mask_id eq !null then begin
           ;;don't know why a postgres null comes back like this, but it does
           mask_query="select pk, prog_pk from binospec_ops.masks where mask_name='"+dat.maskname+"';"
           mask_dat=pgsql_query(mask_query, connect_info='host=localhost user=bino password=safT1st dbname=binospec')
           if isa(mask_dat, 'STRUCT') then begin
              mask_id=long(mask_dat.pk)
              prog_id=long(mask_dat.prog_pk)
           endif else mask_id=!null
        endif
      ;print, mask_id, prog_id
      ;stop
      if dat.cattargetid ne '' then cattarget_id=dat.cattargetid else cattarget_id=!null
      catid=sxpar(hdr, 'CATID')
      maskname=sxpar(hdr, 'MASK')
      if cattarget_id eq !null then begin
          ;if cattarget_id blank, look it up?
         cattarget_query="select id from scheduler.catalogtargets where objectid='"+catid+"';"
         cattarget_dat=pgsql_query(cattarget_query, connect_info='host=dbshare user=mmtobs password=safT1st dbname=binospec')
         if isa(cattarget_dat, 'STRUCT') then cattarget_id=max(cattarget_dat.id) else cattarget_id=!null
      endif
      ;print, mask_id, prog_id, cattarget_id
      ;prog_id=dat.progpk
      if (prog_id eq !null) and (cattarget_id ne !null) then begin
          ;if we found a cattarget_id, we can look up prog_id
          prog_query='select catalogs.programid from scheduler.catalog2catalogtargets, scheduler.catalogs where catalogs.id=catalog2catalogtargets.catalogid and catalog2catalogtargets.catalogtargetid='+string(cattarget_id)
          prog_dat=pgsql_query(prog_query, connect_info='host=dbshare user=mmtobs password=safT1st dbname=binospec')
          if isa(prog_dat, 'STRUCT') then prog_id=max(prog_dat.programid)
       endif
      ;print, prog_id
      if (prog_id ne !null)  then begin
         ;then we can look up PI name and PROPID string
         if ((mask_id ge 110) and (mask_id le 114) or mask_id eq 131) and (cattarget_id eq !null) then begin
                                ;prog_id is not correct in this case, so don't fill it in
            piname=' '
            propid=' '
         endif else begin
         ;proginfo_query='select trimesters.name,programs.number,observers.firstname,observers.lastname from scheduler.catalog2catalogtargets, scheduler.catalogs, scheduler.programs, scheduler.observers, scheduler.trimesters where catalogs.programid=programs.id and catalogs.id=catalog2catalogtargets.catalogid and programs.piid=observers.id and programs.trimesterid=trimesters.id and catalog2catalogtargets.catalogtargetid='+string(cattarget_id)
         proginfo_query='select trimesters.name,programs.number,observers.firstname,observers.lastname from scheduler.programs, scheduler.observers, scheduler.trimesters where programs.piid=observers.id and programs.trimesterid=trimesters.id and programs.id='+string(prog_id)
         proginfo=pgsql_query(proginfo_query, connect_info='host=dbshare user=mmtobs password=safT1st dbname=binospec')
         if isa(proginfo, 'STRUCT') then begin
            piname=strmid(proginfo.firstname,0,1)+proginfo.lastname
            propid=proginfo.name+'-'+proginfo.number
         endif else begin
            piname=' '
            propid=' '            
         endelse
        endelse
      endif else begin
        piname=' '
        propid=' '
     endelse   
   endif else begin
      ;more than one match in the images table
      print, 'error looking up file in images table: more than one match'
      return, 1
   endelse  
endif else begin
   ;no entry in the images table
   print, 'error looking up file in images table: no match'
   return, 1
endelse
  ;this is hard-coded to reflect the list of imaging masks and longslits,
  ;maybe not the best solution
  if (mask_id ge 110) and (mask_id le 114) or mask_id eq 131 or mask_id eq 798 then object=catid else object=maskname
  ;stop
  ;possible TBD: check IMAGETYP from the database, and update? Need to
  ;sort how db values get set/updated first
  
;now loop through the extension headers
  for i=1,8 do begin
     hdr=headfits(filename, ext=i)
     hdr=hdr[where(strtrim(hdr,2) ne '',/null)]
     if sxpar(hdr, 'GSEEING') eq 'NA' then sxaddpar,hdr,'GSEEING','NA'
     if sxpar(hdr, 'WSEEING') eq 'NA' then sxaddpar,hdr,'WSEEING','NA'
     old_ra=sxpar(hdr,'RA')
     old_dec=sxpar(hdr,'DEC')
     if (old_ra lt 0.0) and (old_dec lt -90.0) then begin
        sxaddpar, hdr, 'RA', '-1:00:00.0'
        sxaddpar, hdr, 'DEC', '-100:00:00.0'
        sxaddpar, hdr, 'CURRRA', '-1:00:00.0'
        sxaddpar, hdr, 'CURRDEC', '-100:00:00.0'
        sxaddpar, hdr, 'CATRA', '-1:00:00.0'
        sxaddpar, hdr, 'CATDEC', '-100:00:00.0'
      endif else begin
        sxaddpar, hdr, 'RA', ((dec2hms(old_ra)).trim()).replace(' ',':')
        sxaddpar, hdr, 'DEC', ((dec2hms(old_dec)).trim()).replace(' ',':')
        old_ra=sxpar(hdr,'CURRRA')
        old_dec=sxpar(hdr,'CURRDEC')
        sxaddpar, hdr, 'CURRRA', ((dec2hms(old_ra)).trim()).replace(' ',':')
        sxaddpar, hdr, 'CURRDEC', ((dec2hms(old_dec)).trim()).replace(' ',':')
        old_ra=sxpar(hdr,'CATRA')
        old_dec=sxpar(hdr,'CATDEC')
        sxaddpar, hdr, 'CATRA', ((dec2hms(old_ra)).trim()).replace(' ',':')
        sxaddpar, hdr, 'CATDEC', ((dec2hms(old_dec)).trim()).replace(' ',':')
     endelse
     sxaddpar, hdr, 'OBJECT', object
     sxaddpar, hdr, 'PI', piname
     sxaddpar, hdr, 'PROPID', propid
     modfits, outfile, 0, hdr, exten_no=i
  endfor
;side 2

;write fits table
;mwrfits, fits_tab, filename
;fxhmodify,  filename,'EXTNAME','BINOMASK1',extension=extension
;endif

return, 0
end
