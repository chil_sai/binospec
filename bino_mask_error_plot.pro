;plot mask error for the next 12 hours after the given start time
pro bino_mask_error_plot, mask_id, hours=hours, mjd=mjd
  if not keyword_set(mjd) then mjd=systime(/julian,/utc)-2400000.5d
  hour_arr=findgen(24)/2.
  if keyword_set(hours) then hour_arr=hour_arr+hours
  maxarr=fltarr(24)
  medarr=fltarr(24)
  airmass_out_arr=fltarr(24)+10.
  for i=0, 23  do begin
     mjd_set=mjd
     bino_mask_error, mask_id, mjd=mjd_set, hours=hour_arr[i], maxerr=max, mederr=med, airmass_out=airmass_out
     if max eq !null then maxarr[i]=-1.0 else maxarr[i]=max
     if med eq !null then medarr[i]=-1.0 else  medarr[i]=med
     if airmass_out ne !null then airmass_out_arr[i]=airmass_out
  endfor
  good=where((maxarr ge 0.0) and (airmass_out_arr le 3.5) and (airmass_out_arr ge 1.0))
  ;print, mjd, maxarr[good], medarr[good], airmass_out_arr[good], hour_arr[good]
  a=plot(hour_arr[good], maxarr[good], color='red', xtitle='Hours from MJD='+string(MJD), ytitle='Slit misalginment (arcsec)', title='Slit losses vs time. Blue=median slit, red=worst slit', symbol='*')
  b=plot(hour_arr[good], medarr[good], color='blue', symbol='diamond', /overplot)
  ;stop
end
