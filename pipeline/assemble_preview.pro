pro assemble_preview,logfile,image_type,suffix=suffix,force_image_type_meta=force_image_type_meta

    if(n_params() eq 1) then image_type='obj'
    if(n_elements(suffix) ne 1) then suffix='_qlook'
    wdir=def_wdir(logfile)
    
    image_type_meta=(image_type eq 'obj-sky' or $
                     image_type eq 'obj_abs' or $
                     image_type eq 'obj_abs_err' or $
                     image_type eq 'obj_counts' or $
                     image_type eq 'obj_counts_err')? 'obj' : image_type
    if(strmid(image_type,0,9) eq 'obj_clean') then image_type_meta=strmid(image_type,0,12)
    if(n_elements(force_image_type_meta) eq 1) then image_type_meta=force_image_type_meta
    h_a0=headfits(wdir+image_type_meta+'_dark.fits',ext=1)
    h_b0=headfits(wdir+image_type_meta+'_dark.fits',ext=2)

    mask_a=read_mask_bino_ms(wdir+image_type_meta+'_dark.fits',side='A')
    mask_b=read_mask_bino_ms(wdir+image_type_meta+'_dark.fits',side='B')
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      n_slits_b=0
    endif 
    
    if n_slits_a gt 0 then begin
       h_slit_a=headfits(wdir+image_type+'_slits_lin.fits',ext=1)
       sxaddpar,h_slit_a,'NAXIS2',sxpar(h_a0,'NAXIS2')
       img_preview_a=fltarr(sxpar(h_slit_a,'NAXIS1'),sxpar(h_a0,'NAXIS2'))
    endif
    if n_slits_b gt 0 then begin
       h_slit_b=headfits(wdir+image_type+'_slits_lin.fits',ext=1+n_slits_a)
       sxaddpar,h_slit_b,'NAXIS2',sxpar(h_b0,'NAXIS2')
       img_preview_b=fltarr(sxpar(h_slit_b,'NAXIS1'),sxpar(h_b0,'NAXIS2'))
    endif
    if n_slits_a eq 0 then begin
       img_preview_a=img_preview_b
       h_slit_a=h_slit_b
    endif
    if n_slits_b eq 0 then begin
       img_preview_b=img_preview_a
       h_slit_b=h_slit_a
    endif
    if n_slits_a gt 0 then begin
    for i=0,n_slits_a-1 do begin
        print,'Processing slit: ',i+1
        slit_lin=mrdfits(wdir+image_type+'_slits_lin.fits',i+1,h,/silent)
        y_off=sxpar(h,'YOFFSET')
        img_test=img_preview_a*0.0
        extract_slit_image,img_test,slit_lin,y_off,/f
        img_preview_a+=img_test
    endfor
    endif
    if n_slits_b gt 0 then begin
    for i=0,n_slits_b-1 do begin
        print,'Processing slit: ',i+1+n_slits_a
        slit_lin=mrdfits(wdir+image_type+'_slits_lin.fits',i+1+n_slits_a,h,/silent)
        y_off=sxpar(h,'YOFFSET')
        img_test=img_preview_b*0.0
        extract_slit_image,img_test,slit_lin,y_off,/f
        img_preview_b+=img_test
    endfor
    endif

    fname=wdir+image_type+suffix+'.fits'
    writefits,fname,0
    mwrfits,img_preview_a,fname,h_slit_a
    mwrfits,img_preview_b,fname,h_slit_b
end
