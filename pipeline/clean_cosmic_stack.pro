function clean_cosmic_stack,img_cube,bias=bias,nsig=nsig,$
    out_img_cube=out_img_cube,n_img_clean=n_img_clean,readn=readn,gain=gain,noclean=noclean

    if(n_elements(nsig) ne 1) then nsig=2.0
    if(n_elements(gain) ne 1) then gain=1.0
    if(n_elements(readn) ne 1) then readn=4.0

    s_c=size(img_cube)
    nx=s_c[1]
    ny=s_c[2]
    n_im=(s_c[0] eq 3)? s_c[3] : 1

    s_b=size(bias)
    if(s_b[0] eq 0) then begin
;        resistant_mean,img_cube,nsig,image_res,dim=3
;        return,image_res
        bias=0.0
    endif

    if(n_elements(n_img_clean) ne 1) then n_img_clean=n_im
    n_img_clean = (1 > n_img_clean < n_im)
    if(n_im eq 1) then begin
        if(~keyword_set(noclean)) then begin
            la_cosmic_array,img_cube,outarr=image_res,gain=gain,readn=readn,maskarray=mask_obj,/verbose,sigclip=nsig
            mask_obj=convol(mask_obj,bytarr(1,1)+1)
            bmask=where(mask_obj ne 0,cbmask)
            if(cbmask gt 0) then image_res[bmask]=!values.f_nan
        endif else image_res=img_cube
        if(arg_present(out_img_cube)) then out_img_cube=image_res
        return,image_res
    endif

    min_img=img_cube[*,*,0]
    for i=1,n_im-1 do min_img=(min_img < img_cube[*,*,i])
    min_img=min_img-bias

    min_std=sqrt(((min_img*gain)>0.01) + readn^2)/gain

    mask_cube=bytarr(nx,ny,n_im)+1
    image_res=fltarr(nx,ny)
    for i=0l,n_im-1l do begin
        maxdev = (n_elements(bias) eq -1)? 0 : min_std*nsig

        if(n_img_clean ne n_im and n_img_clean gt 1) then begin
            min_idx=(i lt (n_img_clean-1)/2)? i : i-(n_img_clean-1)/2
            max_idx=min_idx+n_img_clean-1
            while (max_idx gt (n_im-1l)) do begin
                max_idx--
                min_idx--
            endwhile

            print,'Cleaning cosmic ray hits using min/max_idx:',min_idx,max_idx
            min_img=img_cube[*,*,min_idx]
            for j=min_idx+1,max_idx do min_img=(min_img < img_cube[*,*,j])
            min_img=min_img-bias
        endif
        ;bad=where(abs(img_cube[*,*,i]-bias-min_img) gt min_std*nsig, cbad)
        if n_img_clean eq 1 then begin 
           img_cur=img_cube[*,*,i]
           la_cosmic_array,img_cur,outarr=this_image_res,gain=gain,readn=readn,maskarray=this_mask_obj,/verbose,sigclip=nsig
            this_mask_obj=convol(this_mask_obj,bytarr(1,1)+1)
            bmask=where(this_mask_obj ne 0,cbmask,complement=gmask)
            if(cbmask gt 0) then begin
               this_image_res[bmask]=!values.f_nan
               this_mask=mask_cube[*,*,i]
               this_mask[bmask]=0
               ;this_mask[gmask]=1
               mask_cube[*,*,i]=this_mask
            endif
            img_cube[*,*,i]=this_image_res
            image_res+=img_cube[*,*,i]*float(mask_cube[*,*,i])
        endif else begin
          q=djs_reject(img_cube[*,*,i]-bias,min_img,outmask=outmask,maxdev=maxdev)
          print,'img=',i+1,' cbad=',nx*ny-total(outmask)
          if(q eq 0) then begin
              mask_cube[*,*,i]=outmask
              ;mask_cube[bad+nx*ny*i]=0
              image_res+=img_cube[*,*,i]*float(mask_cube[*,*,i])
           endif
        endelse
    endfor
    image_res=image_res/float(total(mask_cube,3))
    print,'N_mask=',nx*ny*n_im-total(mask_cube)
    if(arg_present(out_img_cube)) then begin
        out_img_cube=img_cube
        for i=0l,n_im-1l do begin
            mask_cur=mask_cube[*,*,i]
            nbad_cur=where(mask_cur ne 1,cnbad_cur)
            if(cnbad_cur gt 0) then begin
                img_cur=img_cube[*,*,i]
                img_cur[nbad_cur]=image_res[nbad_cur]
                out_img_cube[*,*,i]=img_cur
            endif
        endfor
    endif

    return, image_res
end
