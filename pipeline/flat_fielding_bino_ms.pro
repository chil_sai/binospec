function bino_flat_model_sc_light,obj_cur,smooth_sci_bgr=smooth_sci_bgr,$
    correct_sci_bgr_level=correct_sci_bgr_level,longslit=longslit,flat=flat

    obj_sc_cur = obj_cur*0.0

    if(keyword_set(longslit)) then begin
        prof_bot=median(obj_cur[*,208:360],dim=2)
        min_prof_bot=min((median(prof_bot,49))[50:4000],/nan)
        prof_top=median(obj_cur[*,3600:3700],dim=2)
        min_prof_top=min((median(prof_top,49))[50:4000],/nan)
        prof_bot_final=prof_bot*0.0+(min_prof_top<min_prof_bot)
        prof_top_final=prof_top*0.0+(min_prof_top<min_prof_bot)
    endif else begin
        y_prof=median(obj_cur,dim=1)
        y_vec=dindgen(n_elements(y_prof))
        binsize=0.025
        hh_bot=total(histogram(y_prof[0:2055],min=0,max=10,bins=binsize),/cumulative)
        hh_top=total(histogram(y_prof[2056:*],min=0,max=10,bins=binsize),/cumulative)
        if(hh_bot[n_elements(hh_bot)-1] le 40 or hh_top[n_elements(hh_top)-1] le 40) then return,obj_sc_cur*0.0
        hh_bot=double(hh_bot)/hh_bot[n_elements(hh_bot)-1]
        hh_top=double(hh_top)/hh_top[n_elements(hh_top)-1]
        thr_bot_lo=min(where(hh_bot gt 0.15 and hh_bot lt 0.85),max=thr_bot_hi) ;; 0.06 0.75
        thr_top_lo=min(where(hh_top gt 0.15 and hh_top lt 0.85),max=thr_top_hi)
        thr_bot_lo*=binsize
        thr_bot_hi*=binsize
        thr_top_lo*=binsize
        thr_top_hi*=binsize
        zero_prof_bot=where(y_prof gt thr_bot_lo and y_prof lt thr_bot_hi and y_vec lt 2056, czero_prof_bot)
        yrange_bot=170+lindgen(31)
        resistant_mean,obj_cur[*,yrange_bot],2.0,x_prof_bot1,dim=2 ;; 120:210 ;;;;50:210
        if(czero_prof_bot gt 5) then resistant_mean,obj_cur[*,zero_prof_bot],2.0,x_prof_bot2,dim=2 else x_prof_bot2=x_prof_bot1*0.0
        zero_prof_top=where(y_prof gt thr_top_lo and y_prof lt thr_top_hi and y_vec ge 2056, czero_prof_top)
        yrange_top=3915+lindgen(46)
        resistant_mean,obj_cur[*,yrange_top],2.0,x_prof_top1,dim=2 ;; 3900:3950
        if(czero_prof_top gt 5) then resistant_mean,obj_cur[*,zero_prof_top],2.0,x_prof_top2,dim=2 else x_prof_top2=x_prof_top1*0.0
        prof_corr_bot=smooth(median(x_prof_bot2-x_prof_bot1,9),150,/nan,/edge_wrap)
        prof_corr_top=smooth(median(x_prof_top2-x_prof_top1,9),150,/nan,/edge_wrap)
        prof_bot_final=(x_prof_bot2-prof_corr_bot)
        prof_top_final=(x_prof_top2-prof_corr_top)
    endelse

    if(keyword_set(smooth_sci_bgr)) then begin
        prof_bot_final_sm=poly_smooth(prof_bot_final,7,deg=3)
        gsm_bot=where(finite(prof_bot_final_sm) eq 1, cgsm_bot)
        if(cgsm_bot gt 0) then prof_bot_final[gsm_bot]=prof_bot_final_sm[gsm_bot]
        prof_top_final_sm=poly_smooth(prof_top_final,7,deg=3)
        gsm_top=where(finite(prof_top_final_sm) eq 1, cgsm_top)
        if(cgsm_top gt 0) then prof_top_final[gsm_top]=prof_top_final_sm[gsm_top]
    endif

    if(keyword_set(correct_sci_bgr_level)) then begin
        ;; resistant_mean,obj_cur[1024:2047,2056],2.0,lev_top1 ;;; commented out because of potential problems if there is a slit edge at y=2055
        ;; resistant_mean,obj_cur[1024:2047,2055],2.0,lev_bot1
        resistant_mean,prof_top_final[1024:2047],2.0,lev_prof_top1
        resistant_mean,prof_bot_final[1024:2047],2.0,lev_prof_bot1
        prof_bot_final[0:2047]+=lev_prof_top1-lev_prof_bot1 ;;-(lev_top1-lev_bot1)
        ;; resistant_mean,obj_cur[2048:3071,2056],2.0,lev_top2
        ;; resistant_mean,obj_cur[2048:3071,2055],2.0,lev_bot2
        resistant_mean,prof_top_final[2048:3071],2.0,lev_prof_top2
        resistant_mean,prof_bot_final[2048:3071],2.0,lev_prof_bot2
        prof_bot_final[2048:*]+=lev_prof_top2-lev_prof_bot2 ;; -(lev_top2-lev_bot2)
    endif
    obj_sc_cur[*,0:2055]=(prof_bot_final # (dblarr(2056)+1d))
    obj_sc_cur[*,2056:*]=(prof_top_final # (dblarr(2056)+1d))

    return,obj_sc_cur
end


pro flat_fielding_bino_ms,logfile,image_type,$
    norm_slit=norm_slit,dymask=dymask,$
    sub_sc_flat=sub_sc_flat,sub_sc_sci=sub_sc_sci,debug=debug, pady=pady

message,'multislit flat-field correction ',/cont

if(n_elements(dymask) ne 1) then dymask=1

wdir=def_wdir(logfile)
grating=def_grating(logfile,filter=filter)
slit=def_slit(logfile)
flat_a=rotate((mrdfits(wdir+'flat_dark.fits',1,ha,/silent)),5)
flat_b=(mrdfits(wdir+'flat_dark.fits',2,hb,/silent))
Nx=sxpar(ha,'NAXIS1')
Ny=sxpar(ha,'NAXIS2')

dist_map_a=mrdfits(wdir+'dist_map_A.fits',1)
dist_map_b=mrdfits(wdir+'dist_map_B.fits',1)

flat_orig_a=flat_a
flat_r_a=poly_2d(flat_a,dist_map_a.kxwrap,dist_map_a.kywrap,1)
flat_a=flat_r_a
flat_r_a=0
flat_orig_b=flat_b
flat_r_b=poly_2d(flat_b,dist_map_b.kxwrap,dist_map_b.kywrap,1)
flat_b=flat_r_b
flat_r_b=0

flat_norm_a=flat_a+!values.f_nan
flat_sm_a=flat_norm_a
flat_norm_b=flat_b+!values.f_nan
flat_sm_b=flat_norm_b

mask_a=read_mask_bino_ms(wdir+'flat_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'flat_dark.fits',side='B')

longslit=(strlowcase(strmid(mask_a[0].mask_name,0,8)) eq 'longslit')? 1 : 0

good_sides=[1,1]
n_slits_a=n_elements(mask_a)
if n_slits_a eq 1 and not tag_exist(mask_a,'slit') then good_sides[0]=0
n_slits_b=n_elements(mask_b)
if n_slits_b eq 1 and not tag_exist(mask_b,'slit') then good_sides[1]=0

if good_sides[0] then begin
    if(longslit eq 1) then begin
        flat_a_sc=bino_flat_model_sc_light(flat_a,/longslit,/flat)
        flat_a=flat_a-flat_a_sc
        flat_orig_a_sc=bino_flat_model_sc_light(flat_orig_a,/longslit,/flat)
        flat_orig_a=flat_orig_a-flat_orig_a_sc
    endif
    y_slits_a=bino_get_slit_region(mask_a,Nx=Nx,Ny=Ny,pady=pady,dist_map=dist_map_a) 
endif else y_slits_a=-1
if good_sides[1] then begin
    if(longslit eq 1) then begin
        flat_b_sc=bino_flat_model_sc_light(flat_b,/longslit,/flat)
        flat_b=flat_b-flat_b_sc
        flat_orig_b_sc=bino_flat_model_sc_light(flat_orig_b,/longslit,/flat)
        flat_orig_b=flat_orig_b-flat_orig_b_sc
    endif
    y_slits_b=bino_get_slit_region(mask_b,Nx=Nx,Ny=Ny,pady=pady,dist_map=dist_map_b) 
endif else y_slits_b=-1

flat_ncoeff_a=flat_norm_a*0.0
flat_ncoeff_b=flat_norm_b*0.0
flat_thr=0.03

if good_sides[0] then begin
  for i=0,n_slits_a-1 do begin
    ymin_cur = y_slits_a[0,i]
    ymax_cur = y_slits_a[1,i]
    print,'Processing slit: ',i+1,'/',n_slits_a,ymin_cur,ymax_cur
    for y=ymin_cur,ymax_cur do begin
        line_cur=median(flat_a[*,y],15)
        line_cur[0:9]=0.0    ;;; cut the edges
        line_cur[Nx-10:Nx-1]=0.0
        flat_sm_a[*,y]=smooth(line_cur,31,/nan)
    endfor
    
    maxv=(n_elements(norm_slit) eq 1 or ymin_cur eq ymax_cur)? 1.0 : max(median(flat_sm_a[*,ymin_cur:ymax_cur],dim=2),/nan)
    flat_norm_a[*,ymin_cur:ymax_cur]=flat_a[*,ymin_cur:ymax_cur]/maxv
    flat_ncoeff_a[*,ymin_cur:ymax_cur]=maxv
  endfor

  if(n_elements(norm_slit) eq 1) then begin
    if(norm_slit lt 0) then begin
        max_vals=dblarr(n_slits_a)+!values.d_nan
        for i=0,n_slits_a-1 do $
            if(strcompress(mask_a[i].type,/remove_all) eq 'TARGET') then $
                max_vals[i]=max(flat_sm_a[*,y_slits_a[0,i]:y_slits_a[1,i]],/nan)
        d_ncoeff=median(max_vals)
    endif else d_ncoeff=max(flat_sm_a[*,y_slits_a[0,norm_slit]:y_slits_a[1,norm_slit]],/nan)
    flat_ncoeff_a=flat_ncoeff_a*d_ncoeff
  endif

  flat_ncoeff_geom_a=poly_2d(flat_ncoeff_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1)
  flat_norm_a=flat_orig_a/flat_ncoeff_geom_a
  
  ;flat_thr=0.03 ;; 0.05
  low_flat_a=where(median(flat_norm_a,5) lt flat_thr,clf_a)
  mask_flat_a=byte(flat_norm_a*0.0)
  if(clf_a gt 0) then begin
    mask_flat_a[low_flat_a]=1
    mask_flat_a=mask_flat_a+shift(mask_flat_a,0,dymask)+shift(mask_flat_a,0,-dymask)
    flat_norm_a[where(mask_flat_a gt 0)]=!values.f_nan
    ;normalize flat
    ;flat_norm_a[where(finite(flat_norm_a))]=flat_norm_a[where(finite(flat_norm_a))]/avg(flat_norm_a[where(finite(flat_norm_a))])
  endif
endif else flat_ncoeff_geom_a=flat_ncoeff_a

;;;; B side
if good_sides[1] then begin
  for i=0,n_slits_b-1 do begin
    ymin_cur = y_slits_b[0,i]
    ymax_cur = y_slits_b[1,i]
    print,'Processing slit: ',i+1,'/',n_slits_b,ymin_cur,ymax_cur
    for y=ymin_cur,ymax_cur do begin
        line_cur=median(flat_b[*,y],15)
        line_cur[0:9]=0.0    ;;; cut the edges
        line_cur[Nx-10:Nx-1]=0.0
        flat_sm_b[*,y]=smooth(line_cur,31,/nan)
    endfor
    
    maxv=(n_elements(norm_slit) eq 1 or ymin_cur eq ymax_cur)? 1.0 : max(median(flat_sm_b[*,ymin_cur:ymax_cur],dim=2),/nan)
    flat_norm_b[*,ymin_cur:ymax_cur]=flat_b[*,ymin_cur:ymax_cur]/maxv
    flat_ncoeff_b[*,ymin_cur:ymax_cur]=maxv
  endfor

  if(n_elements(norm_slit) eq 1) then begin
    if(norm_slit lt 0) then begin
        max_vals=dblarr(n_slits_b)+!values.d_nan
        for i=0,n_slits_b-1 do $
            if(strcompress(mask_b[i].type,/remove_all) eq 'TARGET') then $
                max_vals[i]=max(flat_sm_b[*,y_slits_b[0,i]:y_slits_b[1,i]],/nan)
        d_ncoeff=median(max_vals)
    endif else d_ncoeff=max(flat_sm_b[*,y_slits_b[0,norm_slit]:y_slits_b[1,norm_slit]],/nan)
    flat_ncoeff_b=flat_ncoeff_b*d_ncoeff
  endif

  flat_ncoeff_geom_b=poly_2d(flat_ncoeff_b,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)
  flat_norm_b=flat_orig_b/flat_ncoeff_geom_b

  ;flat_thr=0.03 ;; 0.05
  low_flat_b=where(median(flat_norm_b,5) lt flat_thr,clf_b)
  mask_flat_b=byte(flat_norm_b*0.0)
  if(clf_b gt 0) then begin
    mask_flat_b[low_flat_b]=1
    mask_flat_b=mask_flat_b+shift(mask_flat_b,0,dymask)+shift(mask_flat_b,0,-dymask)
    flat_norm_b[where(mask_flat_b gt 0)]=!values.f_nan
    ;normalize flat
    ;flat_norm_b[where(finite(flat_norm_b))]=flat_norm_b[where(finite(flat_norm_b))]/avg(flat_norm_b[where(finite(flat_norm_b))])
  endif
endif else flat_ncoeff_geom_b=flat_ncoeff_b

;clean_hdr,h
sxaddpar,ha,'datamin',0.8
sxaddpar,ha,'datamax',1.2
sxaddpar,hb,'datamin',0.8
sxaddpar,hb,'datamax',1.2
;mkhdr,h_fl,float(flat_norm_a),/extend
;h_fl=[h_fl[0:5],h]

writefits,wdir+'flat_norm.fits',0
mwrfits,float(flat_norm_a),wdir+'flat_norm.fits',ha
mwrfits,float(flat_norm_b),wdir+'flat_norm.fits',hb
writefits,wdir+'flat_ncoeff.fits',0
mwrfits,float(flat_ncoeff_geom_a),wdir+'flat_ncoeff.fits',ha
mwrfits,float(flat_ncoeff_geom_b),wdir+'flat_ncoeff.fits',hb

for n=0,n_elements(image_type)-1 do begin
    print,'reading  '+wdir+image_type[n]+'_dark.fits'
    h_pri=headfits(wdir+image_type[n]+'_dark.fits')
    obj_a=rotate(mrdfits(wdir+image_type[n]+'_dark.fits',1,ha,/silent),5)
    obj_b=mrdfits(wdir+image_type[n]+'_dark.fits',2,hb,/silent)

    if(keyword_set(sub_sc_sci) and ((strmid(image_type[n],0,3) eq 'obj') or (image_type[n] eq 'skyflatXX'))) then begin
        for i=0,1 do begin
;            obj_cur=(i eq 0)? obj_a : obj_b
            obj_cur=(i eq 0)? poly_2d(obj_a,dist_map_a.kxwrap,dist_map_a.kywrap,1) : poly_2d(obj_b,dist_map_b.kxwrap,dist_map_b.kywrap,1)
            obj_sc_cur=bino_flat_model_sc_light(obj_cur,/smooth_sci_bgr,/correct_sci_bgr_level,longslit=longslit)

            if(i eq 0) then $
                obj_a=obj_a-poly_2d(obj_sc_cur,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1) $
            else $
                obj_b=obj_b-poly_2d(obj_sc_cur,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)
        endfor
    endif
;    if(keyword_set(sub_sc_sci) and (image_type[n] ne 'arc')) then begin
;        obj_sc=crea_sc_bino_ms(logfile,image_type[n],debug=debug)
;        obj=obj-poly_2d(obj_sc,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
;    endif

    ;clean_hdr,h
    ;mkhdr,h_out,float(flat_norm_a),/extend
    ;h_out=[h_out[0:5],h]
    h_out_a=ha
    sxaddhist,'flat-field corrected',h_out_a
    h_out_b=hb
    sxaddhist,'flat-field corrected',h_out_b
    writefits,wdir+image_type[n]+'_ff.fits',0,h_pri
    mwrfits,float(obj_a/flat_norm_a),wdir+image_type[n]+'_ff.fits',h_out_a
    mwrfits,float(obj_b/flat_norm_b),wdir+image_type[n]+'_ff.fits',h_out_b
endfor

return

end

