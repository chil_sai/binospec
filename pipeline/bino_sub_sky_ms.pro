pro bino_sub_sky_ms,logfile,image_type,skylinecorr=skylinecorr,lsfcorr=lsfcorr,lsfskylineid=lsfskylineid

wdir=def_wdir(logfile)
mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 
nslit=n_slits_a+n_slits_b

inpfile=wdir+image_type+'_slits.fits'
skyfile=wdir+'sky_'+image_type+'_2d_bspl_slits.fits'
outfile=wdir+image_type+'-sky_slits.fits'

hpri=headfits(inpfile)
writefits,outfile,0,hpri

if(keyword_set(skylinecorr) and keyword_set(lsfcorr)) then begin
    if(n_elements(lsfskylineid) ne 1) then lsfskylineid=1
    lineid_str=string(lsfskylineid[0],format='(i2.2)')
    sl_obs1=mrdfits(wdir+'skyline_flux_data_'+lineid_str+'.fits',1,/silent)
    sl_obs2=mrdfits(wdir+'skyline_flux_data_'+lineid_str+'.fits',2,/silent)
    sl_mod1=mrdfits(wdir+'skyline_model_flux_data_'+lineid_str+'.fits',1,/silent)
    sl_mod2=mrdfits(wdir+'skyline_model_flux_data_'+lineid_str+'.fits',2,/silent)
    disper_a=mrdfits(wdir+'disper_table.fits',1,/silent)
    disper_b=mrdfits(wdir+'disper_table.fits',2,/silent)
endif

for i=0,nslit-1 do begin
    flat_line_slit=(keyword_set(skylinecorr))? mrdfits(wdir+'flat_emline_slits.fits',i+1,/silent) : 1d
    img=mrdfits(inpfile,i+1,himg,/silent)/flat_line_slit
    sky=mrdfits(skyfile,i+1,/silent)
    for y=0l,sxpar(himg,'NAXIS2')-1l do begin
        if(keyword_set(skylinecorr) and keyword_set(lsfcorr)) then begin
            side=strcompress(sxpar(himg,'SIDE'),/remove_all)
            y_off=sxpar(himg,'YOFFSET')
            nmpix=((side eq 'A')? disper_a[y+y_off].wl_sol[1] : disper_b[y+y_off].wl_sol[1])*0.1
            sig_obs=((side eq 'A')? sl_obs1[y+y_off].sigma : sl_obs2[y+y_off].sigma)/nmpix
            sig_mod=((side eq 'A')? sl_mod1[y+y_off].sigma : sl_mod2[y+y_off].sigma)/nmpix
            sigdiff=sqrt(sig_obs^2-sig_mod^2)
            if(finite(sigdiff) eq 1 and sigdiff gt 0.2) then begin ;; >~1pix FWHM difference
                krnl=psf_gaussian(ndim=1,npix=((2*fix(sigdiff*4.0)+1)>5),fwhm=sigdiff*2.355,/double,/norm)
                convsky=convol(sky[*,y],krnl)
                bsky=where(finite(convsky) ne 1 and finite(sky[*,y]) eq 1,cbsky)
                if(cbsky gt 0) then convsky[bsky]=(sky[*,y])[bsky]
                sky[*,y]=convsky
            endif
        endif
    endfor
    mwrfits,img-sky,outfile,himg,/silent
endfor

end
