pro safe_array_bounds,nmax_1,nmax_2,n0_1,n1_1,n0_2,n1_2

if(n0_1 lt 0) then begin
    n0_2 = -n0_1
    n0_1 = 0
endif

if(n0_2 lt 0) then begin
    n0_1 = -n0_2
    n0_2 = 0
endif

if(n1_1 gt nmax_1-1) then begin
    n1_2 = n1_2 + (nmax_1-1-n1_1)
    n1_1=nmax_1-1
endif

if(n1_2 gt nmax_2-1) then begin
    n1_1 = n1_1 + (nmax_2-1-n1_2)
    n1_2=nmax_2-1
endif

end
