pro binospec_quicklook,rawfilename,flat=rawflat,arc=rawarc,$
    tmpdir=tmpdir,skysubtarget=skysubtarget,noflat=noflat,bright=bright,$
    fullwlsol=fullwlsol,extapw=extapw,oh=oh,$
    extract=extract,extr_optimal=extr_optimal,extr_detect=extr_detect,extr_estimate=extr_estimate,$
    abscal=abscal,countscal=countscal,series=series,split1d=split1d,n_apwmax=n_apwmax

    if(n_elements(tmpdir) ne 1) then tmpdir='../test_data/test_dir/'
    if(NOT file_test(tmpdir,/directory)) then file_mkdir, tmpdir
    if(n_elements(extapw) ne 1) then extapw=4

    n_obj=n_elements(rawfilename)
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    if n_flat eq 0 then noflat=1 else noflat=0
    if n_flat eq 0 then countscal=1 else countscal=1 ;always make counts files
    if n_arc eq 0 then fullwlsol=0 else fullwlsol=1
    if(n_obj gt 1) then begin
        for i=0,n_obj-1 do $
            bino_mosaic,rawfilename[i],tmpdir+'obj_dark_'+string(i,format='(i2.2)')+'.fits',/sub,/mask
        bino_combine,tmpdir+'obj_dark_'+string(findgen(n_obj),format='(i2.2)')+'.fits',tmpdir+'obj_dark.fits',nsig=7.0
    endif else bino_mosaic,rawfilename,tmpdir+'obj_dark.fits',/sub,/mask
    if(n_flat gt 1) then begin
        for i=0,n_flat-1 do $
            bino_mosaic,rawflat[i],tmpdir+'flat_dark_'+string(i,format='(i2.2)')+'.fits',/sub,/mask
        bino_combine,tmpdir+'flat_dark_'+string(findgen(n_flat),format='(i2.2)')+'.fits',tmpdir+'flat_dark.fits',nsig=7.0
    endif else if n_flat eq 1 then bino_mosaic,rawflat,tmpdir+'flat_dark.fits',/sub,/mask
    if(n_arc gt 1) then begin
        for i=0,n_arc-1 do $
            bino_mosaic,rawarc[i],tmpdir+'arc_dark_'+string(i,format='(i2.2)')+'.fits',/sub,/mask
        bino_combine,tmpdir+'arc_dark_'+string(findgen(n_arc),format='(i2.2)')+'.fits',tmpdir+'arc_dark.fits',nsig=7.0
    endif else if n_arc eq 1 then bino_mosaic,rawarc,tmpdir+'arc_dark.fits',/sub,/mask

    h1=headfits(rawfilename[0],ext=1)
    grating=strcompress(strmid(sxpar(h1,'DISPERS1'),1),/remove_all)
    dithpos=strcompress(sxpar(h1,'IELOFF'),/remove_all) ;get telescope instrument offset from header in arcsec

    m1=mrdfits(rawfilename[0],9,hm1)
    m2=mrdfits(rawfilename[0],10,hm2)

    if n_flat ge 1 then begin
      fits_open, tmpdir+'flat_dark.fits', fcb
      n_ext=fcb.nextend
      fits_close, fcb
      if n_ext eq 2 then begin
          mwrfits,m1,tmpdir+'flat_dark.fits',hm1
          mwrfits,m2,tmpdir+'flat_dark.fits',hm2
       endif
    endif

    if n_arc ge 1 then begin
      fits_open, tmpdir+'arc_dark.fits', fcb
      n_ext=fcb.nextend
      fits_close, fcb
      if n_ext eq 2 then begin
          mwrfits,m1,tmpdir+'arc_dark.fits',hm1
          mwrfits,m2,tmpdir+'arc_dark.fits',hm2
      endif
    endif

    logfile=tmpdir+'test_logfile.txt'
    openw,u,logfile,/get_lun
    printf,u,'SIMPLE  =                                 T / FITS-like header'
    printf,u,'LONGSTRN= ''OGIP 1.0''           / The OGIP long string convention may be used.'
    printf,u,'RAW_DIR = ''/data/bino/Archive/rawdata/2017.0503/'''
    printf,u,'R_DIR   = '''+tmpdir+''''
    printf,u,'W_DIR   = '''+tmpdir+''''
    printf,u,'RAWEXT  = ''.gz'''
    printf,u,'INSTRUME= ''BINOSPEC'' / spectrograph name'
    printf,u,'SLIT    = ''mos'' / slit name or MOS'
    printf,u,'GRATING = '''+grating+''' / grating'
    printf,u,'FILTER  = '''' / filter'
    printf,u,'DITHPOS = '+dithpos+' / current dithering position in arcsec'
    printf,u,'BRIGHT  = '+string(keyword_set(bright),format='(i1)')
    printf,u,'EXTAPW  = '+string(extapw,format='(i2)')
    printf,u,'END'
    close,u
    free_lun,u

    ndeg=(grating eq '270')? 4 : 3
    mask_a=read_mask_bino_ms(tmpdir+'obj_dark.fits',side='A')
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then n_slits_a=0
    nx=sxpar(h1,'NAXIS1')
    ny=sxpar(h1,'NAXIS2')
    if n_slits_a gt 0 then begin
      slit_reg_a=bino_get_slit_region(mask_a,nx=nx,ny=ny,slit_trace=slit_trace)
      med_slit_length=abs(median(slit_reg_a[1,*]-slit_reg_a[0,*]))
   endif else begin
    mask_b=read_mask_bino_ms(tmpdir+'obj_dark.fits',side='B')
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then n_slits_b=0
    if n_slits_b gt 0 then begin
       slit_reg_b=bino_get_slit_region(mask_b,nx=nx,ny=ny,slit_trace=slit_trace)
      med_slit_length=abs(median(slit_reg_b[1,*]-slit_reg_b[0,*]))
    endif
   endelse

    if(med_slit_length le 40) then y_ndeg=1 else $
    if(med_slit_length gt 40 and med_slit_length le 250) then y_ndeg=2 else $
    if(med_slit_length gt 240 and med_slit_length le 1000) then y_ndeg=3 else $
    if(med_slit_length gt 1000 and med_slit_length le 2000) then y_ndeg=4 else y_ndeg=5
    if n_flat eq 0 then imtype='obj' else imtype='flat'
    distortion_bino_ms,logfile,imtype,side='A',/quick
    distortion_bino_ms,logfile,imtype,side='B',/quick
    flat_types='obj'
    if not keyword_set(noflat) and n_flat ge 1 then flat_types=[flat_types,'flat']
    if keyword_set(fullwlsol) and n_arc ge 1 then flat_types=[flat_types,'arc']
    if not keyword_set(noflat) and n_flat ge 1 then flat_fielding_bino_ms,logfile,flat_types
    extract_2d_slits,logfile,'obj',unproc=((n_elements(rawflat) ne 1) or keyword_set(noflat)),noflat=noflat
    if keyword_set(fullwlsol) and n_arc ge 1 then extract_2d_slits,logfile,'arc',unproc=((n_elements(rawflat) ne 1) or keyword_set(noflat)),noflat=noflat
    if not keyword_set(noflat) and n_flat ge 1 then extract_2d_slits,logfile,/nflat
    bino_create_wavesol_ms,logfile,quick=~keyword_set(fullwlsol),ndeg=ndeg,y_ndeg=y_ndeg,oh=oh
    file_copy,tmpdir+'obj_slits.fits',tmpdir+'obj-sky_slits.fits',/overwrite
    bino_linearisation,logfile,'obj-sky',subskytarget=skysubtarget, noflat=noflat
    bino_linearisation,logfile,'obj',noflat=noflat ;; ,subskytarget=skysubtarget
    if keyword_set(fullwlsol) and n_arc ge 1 then bino_linearisation,logfile,'arc',noflat=noflat
    ;assemble_preview,logfile,'obj-sky' lets just used counts files
    if keyword_set(fullwlsol) and n_arc ge 1 then assemble_preview,logfile,'arc'

    if keyword_set(countscal) then begin 
      bino_fluxcal,logfile,/counts,n_exp=n_obj,noflat=noflat
      if(keyword_set(series)) then $
        for i=0,n_obj-1 do $
            bino_fluxcal,logfile,/counts,image_type='obj-sky_clean'+string(i,format='(i03)'),$
                nosky_image_type='obj_clean'+string(i,format='(i03)'), noflat=noflat
    endif
    if(keyword_set(abscal)) then bino_fluxcal,logfile,n_exp=n_obj
    if(keyword_set(abscal) and keyword_set(series)) then $
        for i=0,n_obj-1 do $
            bino_fluxcal,logfile,image_type='obj-sky_clean'+string(i,format='(i03)'),$
                                 nosky_image_type='obj_clean'+string(i,format='(i03)')
    if keyword_set(countscal) then assemble_preview,logfile,'obj_counts'
    if(keyword_set(series)) then for i=0,n_obj-1 do assemble_preview,logfile,'obj_clean'+string(i,format='(i03)')+'_counts'

    if(keyword_set(extract)) then begin
        ;skip for speed
        ;bino_extract_1d_slits,logfile,'obj',n_apwmax=n_apwmax,$
        ;    optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate
        ;bino_extract_1d_slits,logfile,'obj-sky',n_apwmax=n_apwmax,$
        ;    optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate,split1d=split1d
        if keyword_set(countscal) then bino_extract_1d_slits,logfile,'obj_counts',n_apwmax=n_apwmax,$
            optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate, split1d=split1d
        if(keyword_set(series) and keyword_set(countscal)) then for i=0,n_obj-1 do $
            bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_counts',$
                n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate,split1d=split1d
        if(keyword_set(abscal)) then begin
            bino_extract_1d_slits,logfile,'obj_abs',n_apwmax=n_apwmax,$
                optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate, split1d=split1d
            if(keyword_set(series)) then for i=0,n_obj-1 do $
                bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_abs',$
                    n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate,split1d=split1d
            ;; quick and dirty fix
            h0=headfits(tmpdir+'obj_abs_slits_extr.fits')
            i1=mrdfits(tmpdir+'obj_abs_slits_extr.fits',1,h1,/silent)
            i1_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',1,/silent)
            i2_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',2,h2,/silent)
            i3_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',3,h3,/silent)
            i4_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',4,h4,/silent)
            i5_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',5,h5,/silent)
            writefits,tmpdir+'obj_abs_slits_extr.fits',0,h0
            mwrfits,i1,tmpdir+'obj_abs_slits_extr.fits',h1,/silent
            mwrfits,i2_r*i1/i1_r,tmpdir+'obj_abs_slits_extr.fits',h2,/silent
            mwrfits,i3_r*i1/i1_r,tmpdir+'obj_abs_slits_extr.fits',h3,/silent
            mwrfits,i4_r,tmpdir+'obj_abs_slits_extr.fits',h4,/silent
            mwrfits,i5_r,tmpdir+'obj_abs_slits_extr.fits',h5,/silent
            if(keyword_set(series)) then begin
                for i=0,n_obj-1 do begin
                    bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_abs',$
                        n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate
                    ;; quick and dirty fix
                    h0=headfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits')
                    i1=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',1,h1,/silent)
                    i1_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',1,/silent)
                    i2_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',2,h2,/silent)
                    i3_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',3,h3,/silent)
                    i4_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',4,h4,/silent)
                    i5_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',5,h5,/silent)
                    writefits,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',0,h0
                    mwrfits,i1,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h1,/silent
                    mwrfits,i2_r*i1/i1_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h2,/silent
                    mwrfits,i3_r*i1/i1_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h3,/silent
                    mwrfits,i4_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h4,/silent
                    mwrfits,i5_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h5,/silent
                endfor
            endif
        endif
    endif
end
