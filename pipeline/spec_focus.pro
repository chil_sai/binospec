function spec_focus_fft,img,nx,ny,med_w=med_w,verbose=verbose,$
    xminarr=xminarr,xmaxarr=xmaxarr,yminarr=yminarr,ymaxarr=ymaxarr,clean=clean,$
    skippix_x=skippix_x,skippix_y=skippix_y
    if(n_elements(med_w) ne 1) then med_w=9

    s_im=size(img)
    sx=s_im[1]
    sy=s_im[2]

    f_thr=5.0

    if(n_elements(skippix_x) ne 1) then skippix_x=0
    if(n_elements(skippix_y) ne 1) then skippix_y=250
    overlap_x=0.2
    overlap_y=0.0

    res=dblarr(nx,ny)+!values.d_nan
    xminarr=lonarr(nx,ny)-1l
    xmaxarr=lonarr(nx,ny)-1l
    yminarr=lonarr(nx,ny)-1l
    ymaxarr=lonarr(nx,ny)-1l
    
    for x=0,nx-1 do begin
        xmin=(x eq 0)? skippix_x : (double(x)-overlap_x)*sx/nx
        xmax=(x eq nx-1)? sx-skippix_x-1 : (double(x)+1.0+overlap_x)*sx/nx
        xmin=fix(xmin) > 0
        xmax=fix(xmax) < (sx-1)
;        if(keyword_set(verbose)) then print,'xfrag=',x
        xvec=dindgen(xmax-xmin+1)-fix((xmax-xmin)/2.0)
        for y=0,ny-1 do begin
            ymin=(y eq 0)? skippix_y : (double(y)-overlap_y)*sy/ny
            ymax=(y eq ny-1)? sy-skippix_y-1 : (double(y)+1.0+overlap_y)*sy/ny
            ymin=fix(ymin) > 0
            ymax=fix(ymax) < (sy-1)
            xminarr[x,y]=xmin
            xmaxarr[x,y]=xmax
            yminarr[x,y]=ymin
            ymaxarr[x,y]=ymax
            im_frag=img[xmin:xmax,ymin:ymax]
            fft_prof=dblarr(xmax-xmin+1)
            im_frag_fft=im_frag*0d
            g_sig_arr=dblarr(ymax-ymin+1)+!values.d_nan
            for yy=0,ymax-ymin do begin
                im_frag_fft[*,yy]=shift(abs(fft(im_frag[*,yy])),fix((xmax-xmin)/2))
                if(med_w gt 1) then im_frag_fft[*,yy]=median(im_frag_fft[*,yy],med_w)
                maxfft=max(im_frag_fft[*,yy],min=minfft)
                if(maxfft gt f_thr) then begin
                    fft_prof+=im_frag_fft[*,yy]
                    if(keyword_set(clean)) then begin
                        g1=gaussfit(xvec,im_frag_fft[*,yy],gk,nterms=4)
                        g_sig_arr[yy]=(xmax-xmin+1)/gk[2]/2.355
                        if(g_sig_arr[yy]) gt 12.0 then g_sig_arr[yy]=!values.d_nan
                    endif
                endif
            endfor
            if(max(fft_prof) gt f_thr) then begin
                if(keyword_set(clean)) then begin
                    med_sig=median(g_sig_arr)
                    g_sig=where(finite(g_sig_arr) eq 1 and $
                                abs(g_sig_arr-med_sig) lt 0.3*med_sig,cg_sig)
                    if(cg_sig gt 0) then $
                        fft_prof=total(im_frag_fft[*,g_sig],2) $
                    else $
                        message,/inf,'Not enough data to detemine focus at x='+string((xmin+xmax)/2,format='(i4)')+',y='+string((ymin+ymax)/2,format='(i4)')
                endif
                g=gaussfit(xvec,fft_prof,gk,nterms=4)
                res[x,y]=(xmax-xmin+1)/gk[2]/2.355
            endif
        endfor
    endfor

    return,res
end

function spec_focus,filename,nx=nx,ny=ny,med_w=med_w,verbose=verbose,$
    xminarr=xminarr,xmaxarr=xmaxarr,yminarr=yminarr,ymaxarr=ymaxarr,clean=clean

    if(n_elements(nx) ne 1) then nx=8
    if(n_elements(ny) ne 1) then ny=8

    res_arr=dblarr(nx,ny,2)+!values.d_nan
    im1=float(mrdfits(filename,1,h1,/sil))
    im2=float(mrdfits(filename,2,h2,/sil))
    if(keyword_set(verbose)) then message,/inf,'Processing sideA'
    res_arr[*,*,0]=spec_focus_fft(im1,nx,ny,med_w=med_w,verbose=verbose,xminarr=xminarr,xmaxarr=xmaxarr,yminarr=yminarr,ymaxarr=ymaxarr,clean=clean)
    if(keyword_set(verbose)) then print,res_arr[*,*,0],format='('+strcompress(string(nx,format='(i)'),/remove_all)+'f7.2)'
    if(keyword_set(verbose)) then message,/inf,'Processing sideB'
    res_arr[*,*,1]=spec_focus_fft(im2,nx,ny,med_w=med_w,verbose=verbose,clean=clean)
    if(keyword_set(verbose)) then print,res_arr[*,*,1],format='('+strcompress(string(nx,format='(i)'),/remove_all)+'f7.2)'


    return,res_arr
end
