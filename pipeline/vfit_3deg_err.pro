function vfit_3deg_err, u, err=uerr, degree1, degree2, degree3, KX=kx_f, $
    IRREGULAR=irreg_p, MAX_DEGREE=max_p, zero_coeff=zero_coeff
;+
; NAME:
;	VFIT_3DEG
;
; PURPOSE:
;	This function determines a polynomial fit to a surface sampled
;	  over a regular or irregular grid.
;
; CATEGORY:
;	Curve and surface fitting.
;
; CALLING SEQUENCE:
;	Result = SFIT(Data, Degree1, Degree2)    ;Regular input grid
;	Result = SFIT(Data, Degree1, Degree2, /IRREGULAR)  ;Irregular input grid
;
; INPUTS:
; 	Data:	The array of data to fit. If IRREGULAR
; 	is not set, the data are assumed to be sampled over a regular 2D
; 	grid, and should be in an Ncolumns by Nrows array.  In this case, the
; 	column and row subscripts implicitly contain the X and Y
; 	location of the point.  The sizes of the dimensions may be unequal.
;	If IRREGULAR is set, Data is a [3,n] array containing the X,
;	Y, and Z location of each point sampled on the surface.  
;
;	Degree:	The maximum degree of fit (in one dimension).
;
; KEYWORDS:
; 	IRREGULAR: If set, Data is [3,n] array, containing the X, Y,
; 	  and Z locations of n points sampled on the surface.  See
; 	  description above.
; 	MAX_DEGREE: If set, the Degree parameter represents the
; 	    maximum degree of the fitting polynomial of all dimensions
; 	    combined, rather than the maximum degree of the polynomial
; 	    in a single variable. For example, if Degree is 2, and
; 	    MAX_DEGREE is not set, then the terms returned will be
; 	    [[K, y, y^2], [x, xy, xy^2], [x^2, x^2 y, x^2 y^2]].
; 	    If MAX_DEGREE is set, the terms returned will be in a
; 	    vector, [K, y, y^2, x, xy, x^2], in which no term has a
; 	    power higher than two in X and Y combined, and the powers
; 	    of Y vary the fastest. 
;
;
; OUTPUT:
;	This function returns the fitted array.  If IRREGULAR is not
;	set, the dimensions of the result are the same as the
;	dimensions of the Data input parameter, and contain the
;	calculated fit at the grid points.  If IRREGULAR is set, the
;	result contains n points, and contains the value of the
;	fitting polynomial at the sample points.
;
; OUTPUT KEYWORDS:
;	Kx:	The array of coefficients for a polynomial function
;		of x and y to fit data. If MAX_DEGREE is not set, this
;		parameter is returned as a (Degree+1) by (Degree+1)
;		element array.  If MAX_DEGREE is set, this parameter
;		is returned as a (Degree+1) * (Degree+2)/2 element
;		vector. 
;
; PROCEDURE:
; 	Fit a 2D array Z as a polynomial function of x and y.
; 	The function fitted is:
;  	    F(x,y) = Sum over i and j of kx[j,i] * x^i * y^j
; 	where kx is returned as a keyword.  If the keyword MAX_DEGREE
; 	is set, kx is a vector, and the total of the X and Y powers will
; 	not exceed DEGREE, with the Y powers varying the fastest.
;
;-

   on_error, 2

   s = size(u)
   if(n_elements(uerr) ne s[2]) then uerr=u[3,*]*0+1d
   irreg = keyword_set(irreg_p)
   max_deg = keyword_set(max_p)
   if(n_params() eq 2) then begin
      degree2=degree1
      degree3=degree1
   endif
   degrees=[degree1,degree2,degree3]
   srtdeg=sort(degrees) ;; determine the largest dimension
   n2 = (degree1+1)*(degree2+1)*(degree3+1) ;# of coeff to solve
   if(n_elements(zero_coeff) ne n2) then begin
       if(n_elements(zero_coeff) ne 0) then message,/inf,'zero_coeff has wrong size'
       zero_coeff=bytarr(degree1+1,degree2+1,degree3+1)
   endif
   if(max_deg) then begin
       for i=0,degree1 do $
           for j=0,degree2 do $
               for k=0,degree3 do $
                   if((i+j+k) gt degrees[srtdeg[2]]) then zero_coeff[i,j,k]=1
   endif
   good_coeff=where(zero_coeff eq 0, n2_red)

   if irreg then begin
       if (s[0] ne 2) or (s[1] ne 4) then $
         message, 'For IRREGULAR grids, input must be [4,n]'
       m = n_elements(u) / 4    ;# of points
       x = double(u[0,*])       ;Do it in double...
       y = double(u[1,*])
       z = double(u[2,*])
       uu = double(u[3,*])
   endif else begin             ;Regular
       if s[0] ne 3 then message, 'For regular grids, input must be [nx, ny, nz]'
       nx = s[1]
       ny = s[2]
       nz = s[3]
       m = nx * ny * nz		;# of points to fit
       x = reform(reform(dindgen(nx) # replicate(1d, ny),nx*ny) # $
                  replicate(1d,nz),nx,ny,nz) ;X values at each point
       y = reform(reform(replicate(1d,nx) # dindgen(ny),nx*ny) # $
                  replicate(1d,nz),nx,ny,nz) ; Y values at each point
       z = reform(replicate(1d,nx*ny) # $
                  dindgen(nz),nx,ny,nz) ; Z values at each point
   endelse
;read,aaa
   if n2_red gt m then message, 'Fitting degrees of '+strtrim(degree1,2)+','+strtrim(degree2,2)+','+strtrim(degree3,2)+$
     ' requires ' + strtrim(n2_red,2) + ' points.'
   ut = dblarr(n2_red, m, /nozero)
   k = 0L
   k_f = 0L
   for h=0,degree3 do begin
       for j=0,degree2 do begin ;Fill each column of basis
           for i=0, degree1 do begin
               ;if max_deg and (i+j+h gt degrees[srtdeg[2]]) then continue
               k_f = k_f + 1L
               if ((reform(zero_coeff))[k_f-1L] ne 0) then continue
;               print,'filling ',i,j,h,' zc=',zero_coeff[i,j,h]
               ut[k, 0] = reform(x^i * y^j * z^h, 1, m)/transpose(uerr)
               k = k + 1L
           endfor
       endfor
   endfor

;   kk = invert(ut # transpose(ut)) # ut
;read,a

   kx = la_least_squares(ut,reform(irreg ? uu : u, m)/reform(uerr,m),/double,resid=chi2)

;   kx = double(kk # (reform(irreg ? uu : u, m, 1)/uerr)) ;Coefficients
   kx_f = dblarr(n2)
   kx_f[good_coeff] = kx
;   if max_deg eq 0 then kx = reform(kx_f, degree1+1, degree2+1, degree3+1)
   kx_f = reform(kx_f, degree1+1, degree2+1, degree3+1)

   return, irreg ? reform(reform(kx_f[good_coeff],n2_red) # ut, m)*uerr : $ ;Return the fit
     reform((reform(kx_f[good_coeff],n2_red) # ut)*uerr, nx, ny, nz)
end
