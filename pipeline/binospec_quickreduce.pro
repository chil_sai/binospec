pro binospec_quickreduce,rawfilename,flat=rawflat,arc=rawarc,skyflat=rawskyflat,tmpdir=tmpdir,$
    sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat,split1d=split1d,noadjust=noadjust, $
    ndeg_wl=ndeg_wl,skylinecorr=skylinecorr,wl_skyline=wl_skyline,lsfcorr=lsfcorr,barycorr=barycorr,$
    skysubtarget=skysubtarget,bright=bright,extapw=extapw,skysubalgorithm=skysubalgorithm,oh=oh,$
    extract=extract,extr_optimal=extr_optimal,extr_detect=extr_detect,extr_estimate=extr_estimate,$
    abscal=abscal,series=series,n_stack=n_stack,n_img_clean=n_img_clean,norm_sci=norm_sci,n_apwmax=n_apwmax,$
    mask_slit_individual=mask_slit_individual,skipsteps=skipsteps,smooth_overscan=smooth_overscan,debug=debug, dithpos=dithpos, no3d=no3d, pady=pady


    if(n_elements(tmpdir) ne 1) then tmpdir='../test_data/test_dir/'
    if(NOT file_test(tmpdir,/directory)) then file_mkdir, tmpdir
    if(n_elements(extapw) ne 1) then extapw=2
    if(n_elements(skysubalgorithm) ne 1) then skysubalgorithm=1 ;; non-local Kelson

    ;;; recommended skylines (1 or more): 435.8335d, 557.734d, 630.0304d, 686.3951d, 734.0881d, 799.3327d. 846.5353d, 888.5843d, 950.2808d
    if(n_elements(wl_skyline) lt 1) then skylinecorr=0
    if(n_elements(skipsteps) eq 0) then skipsteps=['none']
    if(n_elements(n_stack) eq 0) then n_stack=1
    if not keyword_set(pady) then pady=2

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'distort',skipdist)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
    if(~keyword_set(skippri)) then begin
        if(n_obj gt 1) then begin
            for i=0,n_obj_total-1 do $
                bino_mosaic,rawfilename[i],tmpdir+'obj_dark_'+string(i,format='(i3.3)')+'.fits',/sub,/mask,smooth_overscan=smooth_overscan
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    bino_combine,tmpdir+'obj_dark_'+string(findgen(n_stack)+n_stack*i,format='(i3.3)')+'.fits',tmpdir+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=7.0,norm=norm_sci
                bino_combine,tmpdir+'obj_dark_'+string(findgen(n_obj_total),format='(i3.3)')+'.fits',tmpdir+'obj_dark.fits',nsig=7.0,norm=norm_sci
            endif else begin
                bino_combine,tmpdir+'obj_dark_'+string(findgen(n_obj),format='(i3.3)')+'.fits',tmpdir+'obj_dark.fits',nsig=7.0,series=series,n_img_clean=n_img_clean,norm=norm_sci
            endelse
        endif else begin 
           bino_mosaic,rawfilename,tmpdir+'obj_dark_000.fits',/sub,/mask,smooth_overscan=smooth_overscan
           bino_combine,tmpdir+'obj_dark_'+string(findgen(n_obj),format='(i3.3)')+'.fits',tmpdir+'obj_dark.fits',nsig=7.0,series=series,n_img_clean=n_img_clean
        endelse
        if(n_flat gt 1) then begin
            for i=0,n_flat-1 do $
                bino_mosaic,rawflat[i],tmpdir+'flat_dark_'+string(i,format='(i2.2)')+'.fits',/sub,/mask,smooth_overscan=smooth_overscan
            bino_combine,tmpdir+'flat_dark_'+string(findgen(n_flat),format='(i2.2)')+'.fits',tmpdir+'flat_dark.fits',nsig=7.0
        endif else bino_mosaic,rawflat,tmpdir+'flat_dark.fits',/sub,/mask,smooth_overscan=smooth_overscan
        if(n_arc gt 1) then begin
            for i=0,n_arc-1 do $
                bino_mosaic,rawarc[i],tmpdir+'arc_dark_'+string(i,format='(i2.2)')+'.fits',/sub,smooth_overscan=smooth_overscan
            bino_combine,tmpdir+'arc_dark_'+string(findgen(n_arc),format='(i2.2)')+'.fits',tmpdir+'arc_dark.fits',nsig=7.0
        endif else begin
            bino_mosaic,rawarc,tmpdir+'arc_dark_00.fits',/sub,smooth_overscan=smooth_overscan
            bino_combine,tmpdir+'arc_dark_'+string(findgen(n_arc),format='(i2.2)')+'.fits',tmpdir+'arc_dark.fits',nsig=20.0
        endelse
        if(n_skyflat gt 1) then begin
            for i=0,n_skyflat-1 do $
                bino_mosaic,rawskyflat[i],tmpdir+'skyflat_dark_'+string(i,format='(i2.2)')+'.fits',/sub,/mask,smooth_overscan=smooth_overscan
            bino_combine,tmpdir+'skyflat_dark_'+string(findgen(n_skyflat),format='(i2.2)')+'.fits',tmpdir+'skyflat_dark.fits',nsig=7.0,/norm
        endif else $
            if(n_skyflat gt 0) then bino_mosaic,rawskyflat,tmpdir+'skyflat_dark.fits',/sub,/mask,smooth_overscan=smooth_overscan
    endif

    h1=headfits(rawfilename[0],ext=1)
    grating=strcompress(strmid(sxpar(h1,'DISPERS1'),1),/remove_all)
    if not keyword_set(dithpos) then dithpos=strcompress(sxpar(h1,'IELOFF'),/remove_all) else dithpos=strcompress(string(dithpos),/remove);get telescope instrument offset from header in arcsec
    logfile=tmpdir+'test_logfile.txt'
    openw,u,logfile,/get_lun
    printf,u,'SIMPLE  =                                 T / FITS-like header'
    printf,u,'LONGSTRN= ''OGIP 1.0''           / The OGIP long string convention may be used.'
    printf,u,'RAW_DIR = ''/data/bino/Archive/rawdata/2017.0503/'''
    printf,u,'R_DIR   = '+quote_split_ogip_string(tmpdir)
    printf,u,'W_DIR   = '+quote_split_ogip_string(tmpdir)
    printf,u,'RAWEXT  = ''.gz'''
    printf,u,'INSTRUME= ''BINOSPEC'' / spectrograph name'
    printf,u,'SLIT    = '''+(strlowcase(strmid(sxpar(h1,'MASK'),0,8)) eq 'longslit'? 'longslit' : 'mos')+''' / slit name or MOS'
    printf,u,'GRATING = '''+grating+''' / grating'
    printf,u,'FILTER  = '''' / filter'
    printf,u,'DITHPOS = '+dithpos+' / current dithering position in arcsec'
    printf,u,'BRIGHT  = '+string(keyword_set(bright),format='(i1)')
    printf,u,'EXTAPW  = '+string(extapw,format='(i2)')
    printf,u,'END'
    close,u
    free_lun,u

    ndeg=(n_elements(ndeg_wl) eq 1)? ndeg_wl : ((grating eq '270')? 4 : 3)
    mask_a=read_mask_bino_ms(tmpdir+'obj_dark.fits',side='A')
    mask_b=read_mask_bino_ms(tmpdir+'obj_dark.fits',side='B')
    good_sides=[1,1]
    if n_elements(mask_a) eq 1 and not tag_exist(mask_a, 'slit') then good_sides[0]=0
    if n_elements(mask_b) eq 1 and not tag_exist(mask_b, 'slit') then good_sides[1]=0
    h_a=headfits(tmpdir+'obj_dark.fits',ext=1)
    nx=sxpar(h_a,'NAXIS1')
    ny=sxpar(h_a,'NAXIS2')
    h_b=headfits(tmpdir+'obj_dark.fits',ext=2)
    nx=sxpar(h_b,'NAXIS1')
    ny=sxpar(h_b,'NAXIS2')
    if good_sides[0] then begin
        slit_reg_a=bino_get_slit_region(mask_a,nx=nx_a,ny=ny_a,slit_trace=slit_trace_a, pady=pady)
        med_slit_length=abs(median(slit_reg_a[1,*]-slit_reg_a[0,*]))
        tot_slit_length_a=total(abs(slit_reg_a[1,*]-slit_reg_a[0,*]))
        sky_slit_length_a=tot_slit_length_a-1.0*n_elements(mask_a) ;-extapw*keyword_set(bright)
        if(n_elements(mask_slit_individual) gt 0) then begin
            mask_slit_a_idx=where(mask_slit_individual.side eq 'A' and mask_slit_individual.width gt 1,cmask_slit_a_idx)
            if(cmask_slit_a_idx gt 0) then $
                sky_slit_length_a-=total(2*mask_slit_individual[mask_slit_a_idx].width) $
            else $
                sky_slit_length_a-=n_elements(mask_a)*extapw*keyword_set(bright)
        endif else sky_slit_length_a-=n_elements(mask_a)*extapw*keyword_set(bright)

    endif else sky_slit_length_a=0
    if good_sides[1] then begin
        slit_reg_b=bino_get_slit_region(mask_b,nx=nx_b,ny=ny_b,slit_trace=slit_trace_b, pady=pady)
        if good_sides[0] eq 0 then med_slit_length=abs(median(slit_reg_b[1,*]-slit_reg_b[0,*]))
        tot_slit_length_b=total(abs(slit_reg_b[1,*]-slit_reg_b[0,*]))
        sky_slit_length_b=tot_slit_length_b-1.0*n_elements(mask_b) ;-extapw*keyword_set(bright)
        if(n_elements(mask_slit_individual) gt 0) then begin
            mask_slit_b_idx=where(mask_slit_individual.side eq 'B' and mask_slit_individual.width gt 1,cmask_slit_b_idx)
            if(cmask_slit_b_idx gt 0) then $
                sky_slit_length_b-=total(2*mask_slit_individual[mask_slit_b_idx].width) $
            else $
                sky_slit_length_b-=n_elements(mask_b)*extapw*keyword_set(bright)
        endif else sky_slit_length_b-=n_elements(mask_b)*extapw*keyword_set(bright)
    endif else sky_slit_length_b=0

    y_ndeg=-1

    if(~keyword_set(skipdist)) then begin
        distortion_bino_ms,logfile,side='A', pady=pady
        distortion_bino_ms,logfile,side='B', pady=pady
    endif
    exp_flat=(n_skyflat gt 0)? ['obj','arc','flat','skyflat'] : ['obj','arc','flat']
    if(keyword_set(series)) then exp_flat=[exp_flat,'obj_clean'+string(lindgen(n_obj),format='(i03)')]
    flat_fielding_bino_ms,logfile,exp_flat, norm_slit=-1, sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat,dymask=0, pady=pady
    extract_2d_slits,logfile,'obj', pady=pady
    if(keyword_set(series)) then for i=0,n_obj-1 do extract_2d_slits,logfile,'obj_clean'+string(i,format='(i03)'), pady=pady
    extract_2d_slits,logfile,'arc', pady=pady
    extract_2d_slits,logfile,/nflat, pady=pady
    if(n_skyflat gt 0) then extract_2d_slits,logfile,'skyflat', pady=pady
    if(~keyword_set(skipwl)) then bino_create_wavesol_ms,logfile,smy=3,ndeg=ndeg,y_ndeg=y_ndeg,oh=oh,debug=debug,adjusted=adjusted, no3d=no3d, pady=pady
    if keyword_set(skipwl) then begin
       if file_test(tmpdir+'/disper_table_adj.fits') then adjusted=1 else adjusted=0
    endif 
    if keyword_set(noadjust) then adjusted=0 
    bino_linearisation,logfile,'flatn',adj=adjusted
    bino_linearisation,logfile,'obj',/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted ;;,subskytarget=skysubtarget
    if(keyword_set(skylinecorr)) then begin 
        file_delete,tmpdir+'skyline_*flux_data*.fits',/allow_nonexistent,/quiet
        file_delete,tmpdir+'flat_emline*_slits.fits',/allow_nonexistent,/quiet
        bino_create_emline_flat,logfile,wl_skyline,/auto_slit_poly,/fit,adj=adjusted, pady=pady
        bino_linearisation,logfile,'obj',/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
    endif
    if(keyword_set(series)) then for i=0,n_obj-1 do bino_linearisation,logfile,'obj_clean'+string(i,format='(i03)'),/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
    bino_linearisation,logfile,'arc',/masklowflat,flatthr=0.5,adj=adjusted,skylinecorr=skylinecorr
    assemble_preview,logfile,'obj'
    if(keyword_set(series)) then for i=0,n_obj-1 do assemble_preview,logfile,'obj_clean'+string(i,format='(i03)')
    assemble_preview,logfile,'arc'
    if(n_skyflat gt 0) then begin
        bino_linearisation,logfile,'skyflat',/masklowflat,flatthr=0.5,adj=adjusted,skylinecorr=skylinecorr
        assemble_preview,logfile,'skyflat'
    endif
    if(skysubalgorithm eq 1) then begin
        skydegx=[0,0]
        nstages=[1,1]
        clipw=[0d,0d]
        if(med_slit_length le 9) then skysubtarget=0

        if(n_elements(mask_a) gt 3) then skydegx[0]+=2
        ;if(n_elements(mask_a) gt 20 and n_elements(mask_b) gt 20) then skydegx[0]+=1
        if(n_elements(mask_a) gt 100) then skydegx[0]+=1
        if(n_elements(mask_b) gt 3) then skydegx[1]+=2
        if(n_elements(mask_b) gt 100) then skydegx[1]+=1
        if(n_elements(mask_a) gt 3) then begin
            clipw[0]=25 ;;25
            nstages[0]=2
        endif 
        if(n_elements(mask_b) gt 3) then begin
            clipw[1]=25 ;;25
            nstages[1]=2
        endif

        skydegy=[3,3]
        if(sky_slit_length_a gt 800) then skydegy[0]+=1
        if(sky_slit_length_a gt 1500) then skydegy[0]+=1
        if(sky_slit_length_b gt 800) then skydegy[1]+=1
        if(sky_slit_length_b gt 1500) then skydegy[1]+=1
        ;skypix=(min([sky_slit_length_a,sky_slit_length_b])/2.0)
        print,'Useful pixels for sky subtraction (total A,B):', sky_slit_length_a,sky_slit_length_b
        ;skypix=skypix>500
        if(sky_slit_length_a/2 le 500) then skydegx[0]=2
        if(sky_slit_length_b/2 le 500) then skydegx[1]=2

        if(keyword_set(skylinecorr)) then begin
            clipw[0]=(clipw[0] gt 0)? 25 : 10
            clipw[1]=(clipw[1] gt 0)? 25 : 10
            ;;; skydegx/y need to be 2 in order to handle LSF variations
            if(grating eq '600') then begin
                degxzero = where(skydegx eq 0, cdegxzero)
                skydegx=(2 > skydegx < 4)
                if(cdegxzero gt 0) then skydegx[degxzero]=0
                skydegy=(3 > skydegy < 5)
            endif
            if(grating eq '1000') then skydegy=(skydegy < 3)
        endif

        bkspace = (grating eq '270')? 1.05 : (grating eq '600')? 0.5 : 0.35 ;;; 0.8*1.29=1.032 for 270 ;0.496=0.62*0.8
        ;;;;;bino_create_sky_ms,logfile,'obj',skydegx=skydegx,skydegy=skydegy,everyn=skypix ;;; 1000
        if(~keyword_set(skipskymod)) then $
            bino_create_sky_ms,logfile,'obj',skydegx=skydegx,skydegy=skydegy,skylinecorr=skylinecorr,$
                targbksp=bkspace,clipw=clipw,mask_slit_individual=mask_slit_individual,$
                sky_slit_length=sky_slit_length,nstages=nstages,nord=4,maxiter=5,adj=adjusted, pady=pady
         if(keyword_set(series)) then for i=0,n_obj-1 do  bino_create_sky_ms,logfile,'obj_clean'+string(i,format='(i03)'),skydegx=skydegx,skydegy=skydegy,skylinecorr=skylinecorr,$
                targbksp=bkspace,clipw=clipw,mask_slit_individual=mask_slit_individual,$
                sky_slit_length=sky_slit_length,nstages=nstages,nord=4,maxiter=5,adj=adjusted, pady=pady

        if(keyword_set(skylinecorr) and keyword_set(lsfcorr)) then begin 
            bino_linearisation,logfile,'sky_obj_2d_bspl',/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted
            bino_create_emline_flat,logfile,wl_skyline,image_type='sky_obj_2d_bspl',/auto_slit_poly,/fit,adj=adjusted,outsuff='_model', pady=pady
        endif
        bino_sub_sky_ms,logfile,'obj',skylinecorr=skylinecorr,lsfcorr=lsfcorr
        bino_linearisation,logfile,'obj-sky',subskytarget=skysubtarget,/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
        assemble_preview,logfile,'obj-sky'
        if(keyword_set(series)) then begin
           for i=0,n_obj-1 do begin
              bino_sub_sky_ms,logfile,'obj_clean'+string(i,format='(i03)'),skylinecorr=skylinecorr,lsfcorr=lsfcorr
              file_copy,tmpdir+'obj_clean'+string(i,format='(i03)')+'-sky_slits.fits',$
                          tmpdir+'obj-sky_clean'+string(i,format='(i03)')+'_slits.fits',/overwrite
              bino_linearisation,logfile,'obj-sky_clean'+string(i,format='(i03)'),subskytarget=skysubtarget,/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
           endfor
        endif
    endif else begin
        ;;; no sky subtraction unless skysubtarget is set
        file_copy,tmpdir+'obj_slits.fits',tmpdir+'obj-sky_slits.fits',/overwrite
        bino_linearisation,logfile,'obj-sky',subskytarget=skysubtarget,/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
        if(keyword_set(series)) then begin
            for i=0,n_obj-1 do begin
                file_copy,tmpdir+'obj_clean'+string(i,format='(i03)')+'_slits.fits',$
                          tmpdir+'obj-sky_clean'+string(i,format='(i03)')+'_slits.fits',/overwrite
                bino_linearisation,logfile,'obj-sky_clean'+string(i,format='(i03)'),subskytarget=skysubtarget,/masklowflat,flatthr=0.5,barycorr=barycorr,adj=adjusted,skylinecorr=skylinecorr
            endfor
        endif
    endelse
    bino_fluxcal,logfile,/counts,n_exp=n_obj
    if(keyword_set(series)) then $
        for i=0,n_obj-1 do $
            bino_fluxcal,logfile,/counts,image_type='obj-sky_clean'+string(i,format='(i03)'),$
                                         nosky_image_type='obj_clean'+string(i,format='(i03)')
    if(keyword_set(abscal)) then bino_fluxcal,logfile,n_exp=n_obj
    if(keyword_set(abscal) and keyword_set(series)) then $
        for i=0,n_obj-1 do $
            bino_fluxcal,logfile,image_type='obj-sky_clean'+string(i,format='(i03)'),$
                                 nosky_image_type='obj_clean'+string(i,format='(i03)')
    if(n_skyflat gt 0) then begin
        bino_fluxcal,logfile,image_type='skyflat',nosky_image_type='skyflat',/counts,n_exp=n_skyflat
        if(keyword_set(abscal)) then bino_fluxcal,logfile,image_type='skyflat',nosky_image_type='skyflat',n_exp=n_skyflat
        assemble_preview,logfile,'skyflat_counts',force='skyflat'
    endif
    assemble_preview,logfile,'obj_counts'
    if(keyword_set(series)) then for i=0,n_obj-1 do assemble_preview,logfile,'obj_clean'+string(i,format='(i03)')+'_counts'

    if(keyword_set(extract)) then begin
        bino_extract_1d_slits,logfile,'obj',n_apwmax=n_apwmax,$
            optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate
        bino_extract_1d_slits,logfile,'obj-sky',n_apwmax=n_apwmax,$
            optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate
        bino_extract_1d_slits,logfile,'obj_counts',n_apwmax=n_apwmax,$
            optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate, split1d=split1d
        if(keyword_set(series)) then for i=0,n_obj-1 do $
            bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_counts',$
                n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate,split1d=split1d
        if(keyword_set(abscal)) then begin
            bino_extract_1d_slits,logfile,'obj_abs',n_apwmax=n_apwmax,$
                optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate, split1d=split1d
            if(keyword_set(series)) then for i=0,n_obj-1 do $
                bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_abs',$
                    n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate,split1d=split1d
            ;; quick and dirty fix
            h0=headfits(tmpdir+'obj_abs_slits_extr.fits')
            i1=mrdfits(tmpdir+'obj_abs_slits_extr.fits',1,h1,/silent)
            i1_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',1,/silent)
            i2_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',2,h2,/silent)
            i3_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',3,h3,/silent)
            i4_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',4,h4,/silent)
            i5_r=mrdfits(tmpdir+'obj_counts_slits_extr.fits',5,h5,/silent)
            writefits,tmpdir+'obj_abs_slits_extr.fits',0,h0
            mwrfits,i1,tmpdir+'obj_abs_slits_extr.fits',h1,/silent
            mwrfits,i2_r*i1/i1_r,tmpdir+'obj_abs_slits_extr.fits',h2,/silent
            mwrfits,i3_r*i1/i1_r,tmpdir+'obj_abs_slits_extr.fits',h3,/silent
            mwrfits,i4_r,tmpdir+'obj_abs_slits_extr.fits',h4,/silent
            mwrfits,i5_r,tmpdir+'obj_abs_slits_extr.fits',h5,/silent
            if(keyword_set(series)) then begin
                for i=0,n_obj-1 do begin
                    bino_extract_1d_slits,logfile,'obj_clean'+string(i,format='(i03)')+'_abs',$
                        n_apwmax=n_apwmax, optimal=extr_optimal,detect=extr_detect,estimate=extr_estimate
                    ;; quick and dirty fix
                    h0=headfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits')
                    i1=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',1,h1,/silent)
                    i1_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',1,/silent)
                    i2_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',2,h2,/silent)
                    i3_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',3,h3,/silent)
                    i4_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',4,h4,/silent)
                    i5_r=mrdfits(tmpdir+'obj_clean'+string(i,format='(i03)')+'_counts_slits_extr.fits',5,h5,/silent)
                    writefits,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',0,h0
                    mwrfits,i1,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h1,/silent
                    mwrfits,i2_r*i1/i1_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h2,/silent
                    mwrfits,i3_r*i1/i1_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h3,/silent
                    mwrfits,i4_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h4,/silent
                    mwrfits,i5_r,tmpdir+'obj_clean'+string(i,format='(i03)')+'_abs_slits_extr.fits',h5,/silent
                endfor
            endif
        endif
    endif

end
