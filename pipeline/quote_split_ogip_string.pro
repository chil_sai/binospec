function quote_split_ogip_string,s,nc=nc
    if(n_elements(nc) ne 1) then nc=66
    l=strlen(s)

    s_arr=strarr(fix(l/nc)+1)
    for l=0,n_elements(s_arr)-1 do s_arr[l]=strmid(s,l*nc,nc)
    s_out=strjoin(s_arr,'&'''+string(10B)+'CONTINUE  ''',/single)
    return,''''+s_out+''''
end
