pro bino_adjust_wavesol_ms,logfile,slit=slit,pady=pady,$
    wlndeg=ndeg,ndeg_3d_x=ndeg_3d_x,ndeg_3d_y=ndeg_3d_y,$
    suff=suff,flag_3d_a=flag_3d_a,flag_3d_b=flag_3d_b

wdir=def_wdir(logfile)
grating=def_grating(logfile,filter=filter)
if(n_elements(suff) ne 1) then suff='_adj'

h0=headfits(wdir+'wl_data_3d.fits')
;np_a=sxpar(h0,'NP_A')
;np_b=sxpar(h0,'NP_B')

disper_all_a=mrdfits(wdir+'disper.fits',0,/silent)
disper_all_b=mrdfits(wdir+'disper.fits',1,/silent)
pix_mask_data_a=mrdfits(wdir+'disper_table.fits',1,/silent)
pix_mask_data_b=mrdfits(wdir+'disper_table.fits',2,/silent)

data_3d_a=mrdfits(wdir+'wl_data_3d.fits',1,/silent)
np_a=n_elements(data_3d_a[0,*])
data_3d_b=mrdfits(wdir+'wl_data_3d.fits',2,/silent)
np_b=n_elements(data_3d_b[0,*])
errdata_a=mrdfits(wdir+'wl_data_3d.fits',3,/silent)
errdata_b=mrdfits(wdir+'wl_data_3d.fits',4,/silent)

if(n_elements(ndeg_3d_x) ne 1) then ndeg_3d_x=(grating eq '1000' or grating eq '600')? 4 : 3
if(n_elements(ndeg_3d_y) ne 1) then ndeg_3d_y=(grating eq '1000' or grating eq '600')? 4 : 3
;if(n_elements(ndeg_3d_x) ne 1) then ndeg_3d_x=3
;if(n_elements(ndeg_3d_y) ne 1) then ndeg_3d_y=3

dist_map_a=mrdfits(wdir+'dist_map_A.fits',1)
dist_map_b=mrdfits(wdir+'dist_map_B.fits',1)

h1=headfits(wdir+'arc_dark.fits',ext=1)
h2=headfits(wdir+'arc_dark.fits',ext=2)
nx=sxpar(h1,'NAXIS1')
ny_a=sxpar(h1,'NAXIS2')
ny_b=sxpar(h2,'NAXIS2')
mask_a=read_mask_bino_ms(wdir+'arc_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'arc_dark.fits',side='B')

nxvec=dindgen(nx)

x_img_a=nxvec # (dblarr(ny_a)+1d)
y_img_a=(dblarr(nx)+1d) # dindgen(ny_a)
xmask_img_a=(dblarr(nx)+1d) # transpose(pix_mask_data_a.x_mask)
wl_img_a=dblarr(nx,ny_a)
for y=0,ny_a-1 do wl_img_a[*,y]=poly(nxvec,pix_mask_data_a[y].wl_sol)
x_img_a_r= rotate(poly_2d(x_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),0)
y_img_a_r= rotate(poly_2d(y_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),0)
xmask_img_a_r= rotate(poly_2d(xmask_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),0)
wl_img_a_r=rotate(poly_2d(wl_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),0)

x_img_b=nxvec # (dblarr(ny_b)+1d)
y_img_b=(dblarr(nx)+1d) # dindgen(ny_b)
xmask_img_b=(dblarr(nx)+1d) # transpose(pix_mask_data_b.x_mask)
wl_img_b=dblarr(nx,ny_b)
for y=0,ny_b-1 do wl_img_b[*,y]=poly(nxvec,pix_mask_data_b[y].wl_sol)
x_img_b_r= poly_2d(x_img_b,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)
y_img_b_r= poly_2d(y_img_b,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)
xmask_img_b_r= poly_2d(xmask_img_b,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)
wl_img_b_r=poly_2d(wl_img_b,dist_map_b.kxwrap_inv,dist_map_b.kywrap_inv,1)

y_scl_a=24.555832d
y_scl_b=24.548194d
if(tag_exist(dist_map_a,'mask_y_scl')) then y_scl_a=dist_map_a.mask_y_scl
if(tag_exist(dist_map_b,'mask_y_scl')) then y_scl_b=dist_map_b.mask_y_scl
zz1=1d
zz2=1d
mmpix_a=1d/y_scl_a
mmpix_b=1d/y_scl_b

good_sides=[1,1]
n_slits_a=n_elements(mask_a)
if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
  good_sides[0]=0
  n_slits_a=0
endif 
n_slits_b=n_elements(mask_b)
if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
  good_sides[1]=0
  n_slits_b=0
endif 
if(n_elements(slit) eq 0) then slit=findgen(n_slits_a+n_slits_b)
n_slits=n_slits_a+n_slits_b

if n_slits_a gt 0 then y_slits_a=bino_get_slit_region(mask_a,nx=nx,Ny=ny_a,dist_map=dist_map_a,slit_geom=slit_geom_a,pady=pady) else y_slits_a=-1
if n_slits_b gt 0 then y_slits_b=bino_get_slit_region(mask_b,nx=nx,Ny=ny_b,dist_map=dist_map_b,slit_geom=slit_geom_b,pady=pady) else y_slits_b=-1

pix_mask_str={y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
    w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1,$
    wl_sol:dblarr(ndeg+1)+!values.d_nan,wl_s_err:!values.d_nan}

nx_h2d=10
ny_h2d=10

if n_slits_a gt 0 then begin
  slit_tilt_a=dblarr(n_slits_a)
  for i=0,n_slits_a-1 do begin
      hdr_slit=headfits(wdir+'arc_slits.fits',ext=i+1)
      slit_tilt_a[i] = (sxpar(hdr_slit,'SLITTHET')-sxpar(hdr_slit,'MASKPA')) mod 180.0
      if(abs(slit_tilt_a[i]) lt 1e-2)then slit_tilt_a[i]=0d
  endfor
endif
if n_slits_b gt 0 then begin
  slit_tilt_b=dblarr(n_slits_b)
  for i=0,n_slits_b-1 do begin
      hdr_slit=headfits(wdir+'arc_slits.fits',ext=n_slits_a+i+1)
      slit_tilt_b[i] = (sxpar(hdr_slit,'SLITTHET')-sxpar(hdr_slit,'MASKPA')) mod 180.0
      if(abs(slit_tilt_b[i]) lt 1e-2)then slit_tilt_b[i]=0d
  endfor
endif
deg_corr=1 ;; degree of polynomial correction for tilted slits (0=constant; 1=linear)
deg_weight=2 ;6 ;; degree of cos(tilt)^n to reduce weights of tilted slits
if(flag_3d_a eq 1) and (np_a gt 2000) then begin
    pix_mask_data_a=replicate(pix_mask_str,ny_a)
    tags=tag_names(slit_geom_a[0])
    for i=0,n_elements(tags)-1 do pix_mask_data_a.(i)=slit_geom_a.(i)

    tilted_a = where(slit_tilt_a ne 0.0, n_tilted_a)
    ;;; applying distortion correction to ypix
    pixcrd_2d_a=long(data_3d_a[0,*]) + nx*long(data_3d_a[2,*])
    data_3d_a[0,*]-=(x_img_a_r[pixcrd_2d_a]- x_img_a[pixcrd_2d_a]) ;; (nx-1-x_img_a[pixcrd_2d_a]))
    data_3d_a[2,*]-=(y_img_a_r[pixcrd_2d_a]- y_img_a[pixcrd_2d_a])
    ; data_3d_a[3,*]-=(wl_img_a_r[pixcrd_2d_a]-wl_img_a[pixcrd_2d_a])

    w_err_a=dblarr(n_elements(errdata_a))+1d
    for j=0,n_slits_a-1 do begin
        slit_cur_idx=where(data_3d_a[4,*] eq j, cslit_cur_idx)
        if(cslit_cur_idx gt 0) then w_err_a[slit_cur_idx]=cos(slit_tilt_a[j]/!radeg)^deg_weight ;; decrease weights of tilted slits
    endfor
    errdata_a/=w_err_a

    maxiter=(n_tilted_a eq 0)? 0 : 3
    for iter=0,maxiter do begin
        print,'Wavelength adjustment fitting iteration: ',iter
        gdata=where((finite(total(data_3d_a,1)+errdata_a) eq 1) and $
                    (errdata_a gt 0) and (data_3d_a[0,*] gt 1) and $
                    (data_3d_a[0,*] lt nx-2),np_a_new)
        data_3d_a_fit=data_3d_a[*,gdata]
        errdata_a_fit=errdata_a[gdata]^1.0 ;;; test

        ;;; increasing weights of bright lines
        g_bright=where(errdata_a_fit lt 1.0/500.0^0.25,cg_bright)
        if(cg_bright gt 0) then errdata_a_fit[g_bright]/=1.0 ; 5.0
        ;;; weighting by slit density
        mask_xsize=mask_a[0].corners[2]-mask_a[0].corners[0]
        mask_ysize=4096.0-180.0*2 ;;mask_a[0].corners[3]-mask_a[0].corners[1]
        hist_slits=hist_2d(data_3d_a_fit[1,*],data_3d_a_fit[2,*],$
            min1=mask_a[0].corners[0],max1=mask_a[0].corners[2],$
            min2=180.0, max2=4096.0-180.0,$ ;; min2=mask_a[0].corners[1],max2=mask_a[0].corners[3],$
            bin1=mask_xsize/nx_h2d*1.0001,bin2=mask_ysize/ny_h2d*1.0001)
        max_hist_slits=max(hist_slits)
        for x=0,nx_h2d-1 do begin
            for y=0,ny_h2d-1 do begin
                if(hist_slits[x,y] gt 1) then begin
                    h2d_cell=where((data_3d_a_fit[1,*] ge mask_a[0].corners[0]+mask_xsize/nx_h2d*x) and $
                                   (data_3d_a_fit[1,*] lt mask_a[0].corners[0]+mask_xsize/nx_h2d*(x+1)) and $
                                   (data_3d_a_fit[2,*] ge 180.0+mask_ysize/ny_h2d*y) and $
                                   (data_3d_a_fit[2,*] lt 180.0+mask_ysize/ny_h2d*(y+1)),c_h2d_cell)
                    if(c_h2d_cell gt 0) then errdata_a_fit[h2d_cell]*=double(hist_slits[x,y])^1.0
                endif
            endfor
        endfor
        fit_3d=vfit_3deg_err(data_3d_a_fit[0:3,*],err=errdata_a_fit^0.0,ndeg,ndeg_3d_x,ndeg_3d_y,kx=kwl3d,/irreg,/max_deg)
;        gdata_fit=where(abs(data_3d_a_fit[3,*]-fit_3d) lt abs(kwl3d[1,0,0])*0.5*1.6, cgdata_fit)
;        fit_3d_i1=vfit_3deg_err(data_3d_a_fit[0:3,gdata_fit],err=errdata_a_fit[gdata_fit],ndeg,ndeg_3d_x,ndeg_3d_y+2*0,kx=kwl3d_i1,/irreg,max_deg=0)
        if(cg_bright gt 0) then errdata_a_fit[g_bright]*=5.0
        if(iter eq 0) then begin
            disper_all_a_old=disper_all_a
        endif
        for j=0,n_slits_a-1 do begin
            ymin_cur = y_slits_a[0,j]
            ymax_cur = y_slits_a[1,j]
            ys_pos=ymax_cur-ymin_cur+1

            pos_xmask = pix_mask_data_a[ymin_cur:ymax_cur].x_mask
            ypix=ymin_cur+dindgen(ys_pos)

            wlmap_slit=reform(poly3d(nxvec # (dblarr(ys_pos)+1d),(dblarr(nx)+1d) # pos_xmask,(dblarr(nx)+1d) # ypix,kwl3d,/irreg),nx,ys_pos)
            wlmap_slit_r=bino_rectify_slit(wlmap_slit,dist_map_a,ymin_cur,side='A',edge=(slit_tilt_a[j] eq 0.))
            disp_3d_fit_slit=dblarr(ndeg+1,ys_pos)+!values.d_nan
            for y=ymin_cur,ymax_cur do begin
                goodwl=where(finite(wlmap_slit_r[*,y-ymin_cur]) eq 1,cgoodwl)
                if(cgoodwl gt 10) then disp_3d_fit_slit[*,y-ymin_cur]=transpose(poly_fit(double(goodwl),wlmap_slit_r[goodwl,y-ymin_cur],ndeg))
            endfor

            if(slit_tilt_a[j] ne 0d) then begin
                ; adding wloffset to data_3d_a
                print,'Side:A slit,slit_tilt=',j,slit_tilt_a[j]
                dwl_cntpix_arr=dblarr(ys_pos)
                good_dwl=where(finite(dwl_cntpix_arr) eq 1 and disper_all_a_old[0,ymin_cur:ymax_cur] ne 0.0, cgood_dwl)
                slit_cur_idx=where(data_3d_a[4,*] eq j and $
                                   finite(total(data_3d_a[0:3,*],1)) eq 1, cslit_cur_idx)
                if(cslit_cur_idx gt 0) then begin
                    dwl_slit_pix=data_3d_a[3,slit_cur_idx]-poly3d(data_3d_a[0,slit_cur_idx],data_3d_a[1,slit_cur_idx],data_3d_a[2,slit_cur_idx],kwl3d,/irreg)
                    if(cgood_dwl gt 0 and cgood_dwl le 3) then begin
                        resistant_mean,dwl_slit_pix,3.0,wloffset_slit
                        dwl_cntpix_arr[*]=wloffset_slit
                    endif else if(cgood_dwl ge 4) then begin
                        min_y_a=min(data_3d_a[2,slit_cur_idx]-ymin_cur,max=max_y_a,/nan)
                        ndeg_tilt=(max_y_a-min_y_a gt 200)? 2 : 1
                        dwl_fit=poly_fit(data_3d_a[2,slit_cur_idx]-ymin_cur,dwl_slit_pix,ndeg_tilt) ;  robust_poly_fit(data_3d_a[2,slit_cur_idx]-ymin_cur,dwl_slit_pix,1)
                        dwl_cntpix_arr=poly(dindgen(ys_pos),dwl_fit)
                        wloffset_slit=transpose(poly(transpose(data_3d_a[2,slit_cur_idx]-ymin_cur),dwl_fit))
                    endif else wloffset_slit=0.0
                    data_3d_a[3,slit_cur_idx]-=wloffset_slit
                endif
                if(iter eq maxiter) then begin
                    disp_3d_fit_slit[0,*]-=dwl_cntpix_arr
                    dwl_cntpix_ori=dblarr(ys_pos,deg_corr+1)
                    if(deg_corr eq 0) then begin
                        for d=0,ndeg do dwl_cntpix_ori[*,0]+=transpose((disp_3d_fit_slit[d,*]-disper_all_a_old[d,ymin_cur:ymax_cur])*2048d^double(d))
                        disp_3d_fit_slit[0,*]-=dwl_cntpix_ori[*,0]
                    endif else if(deg_corr eq 1) then begin
                        for d=0,ndeg do begin
                            dwl_cntpix_ori[*,0]+=transpose((disp_3d_fit_slit[d,*]-disper_all_a_old[d,ymin_cur:ymax_cur])*1024d^double(d))
                            dwl_cntpix_ori[*,1]+=transpose((disp_3d_fit_slit[d,*]-disper_all_a_old[d,ymin_cur:ymax_cur])*3072d^double(d))
                        endfor
                        disp_3d_fit_slit[0,*]-=(dwl_cntpix_ori[*,0]*3072d -dwl_cntpix_ori[*,1]*1024d)/(3072d -1024d)
                        disp_3d_fit_slit[1,*]-=(dwl_cntpix_ori[*,1]-dwl_cntpix_ori[*,0])/(3072d -1024d)
                    endif
                endif
                print,'WL offset: ',(n_elements(dwl_cntpix_arr) eq 1)? dwl_cntpix_arr : median(dwl_cntpix_arr)
            endif
            disper_all_a[0:ndeg,ymin_cur:ymax_cur]=disp_3d_fit_slit[*,*]
            disper_all_a[ndeg+1,ymin_cur:ymax_cur]=0.01
            pix_mask_data_a[ymin_cur:ymax_cur].wl_sol=disp_3d_fit_slit[0:ndeg,*]
            pix_mask_data_a[ymin_cur:ymax_cur].wl_s_err=0.01
        endfor
    endfor
endif

if(flag_3d_b eq 1) and (np_b gt 2000) then begin
    pix_mask_data_b=replicate(pix_mask_str,ny_b)
    tags=tag_names(slit_geom_b[0])
    for i=0,n_elements(tags)-1 do pix_mask_data_b.(i)=slit_geom_b.(i)

    tilted_b = where(slit_tilt_b ne 0.0, n_tilted_b)
    ;;; applying distortion correction to ypix
    pixcrd_2d_b=long(data_3d_b[0,*]) + nx*long(data_3d_b[2,*])
    data_3d_b[0,*]-=(x_img_b_r[pixcrd_2d_b]- x_img_b[pixcrd_2d_b])
    data_3d_b[2,*]-=(y_img_b_r[pixcrd_2d_b]- y_img_b[pixcrd_2d_b])
    ; data_3d_b[3,*]-=(wl_img_b_r[pixcrd_2d_b]-wl_img_b[pixcrd_2d_b])

    w_err_b=dblarr(n_elements(errdata_b))+1d
    for j=0,n_slits_b-1 do begin
        slit_cur_idx=where(data_3d_b[4,*] eq j+n_slits_a, cslit_cur_idx)
        if(cslit_cur_idx gt 0) then w_err_b[slit_cur_idx]=cos(slit_tilt_b[j]/!radeg)^deg_weight ;; decrease weights of tilted slits
    endfor
    errdata_b/=w_err_b

    maxiter=(n_tilted_b eq 0)? 0 : 3
    for iter=0,maxiter do begin
        print,'Wavelength adjustment fitting iteration: ',iter
        gdata=where((finite(total(data_3d_b,1)+errdata_b) eq 1) and $
                    (errdata_b gt 0) and (data_3d_b[0,*] gt 1) and $
                    (data_3d_b[0,*] lt nx-2),np_b_new)
        data_3d_b_fit=data_3d_b[*,gdata]
        errdata_b_fit=errdata_b[gdata]^1.0 ;;; test

        ;;; increasing weights of bright lines
        g_bright=where(errdata_b_fit lt 1.0/500.0^0.25,cg_bright)
        if(cg_bright gt 0) then errdata_b_fit[g_bright]/=1.0 ; 5.0
        ;;; weighting by slit density
        mask_xsize=mask_b[0].corners[2]-mask_b[0].corners[0]
        mask_ysize=4096.0-180.0*2 ;;mask_b[0].corners[3]-mask_b[0].corners[1]
        hist_slits=hist_2d(data_3d_b_fit[1,*],data_3d_b_fit[2,*],$
            min1=mask_b[0].corners[0],max1=mask_b[0].corners[2],$
            min2=180.0, max2=4096.0-180.0,$ ;; min2=mask_b[0].corners[1],max2=mask_b[0].corners[3],$
            bin1=mask_xsize/nx_h2d*1.0001,bin2=mask_ysize/ny_h2d*1.0001)
        max_hist_slits=max(hist_slits)
        for x=0,nx_h2d-1 do begin
            for y=0,ny_h2d-1 do begin
                if(hist_slits[x,y] gt 1) then begin
                    h2d_cell=where((data_3d_b_fit[1,*] ge mask_b[0].corners[0]+mask_xsize/nx_h2d*x) and $
                                   (data_3d_b_fit[1,*] lt mask_b[0].corners[0]+mask_xsize/nx_h2d*(x+1)) and $
                                   (data_3d_b_fit[2,*] ge 180.0+mask_ysize/ny_h2d*y) and $
                                   (data_3d_b_fit[2,*] lt 180.0+mask_ysize/ny_h2d*(y+1)),c_h2d_cell)
                    if(c_h2d_cell gt 0) then errdata_b_fit[h2d_cell]*=double(hist_slits[x,y])^1.0
                endif
            endfor
        endfor
        fit_3d=vfit_3deg_err(data_3d_b_fit[0:3,*],err=errdata_b_fit^0.0,ndeg,ndeg_3d_x,ndeg_3d_y,kx=kwl3d,/irreg,/max_deg)
;        gdata_fit=where(abs(data_3d_b_fit[3,*]-fit_3d) lt abs(kwl3d[1,0,0])*0.5, cgdata_fit)
;        fit_3d_i1=vfit_3deg_err(data_3d_b_fit[0:3,gdata_fit],err=errdata_b_fit[gdata_fit],ndeg,ndeg_3d_x,ndeg_3d_y+2,kx=kwl3d_i1,/irreg,max_deg=0)
        if(cg_bright gt 0) then errdata_b_fit[g_bright]*=5.0
        if(iter eq 0) then begin
            disper_all_b_old=disper_all_b
        endif
        for j=0,n_slits_b-1 do begin
            ymin_cur = y_slits_b[0,j]
            ymax_cur = y_slits_b[1,j]
            ys_pos=ymax_cur-ymin_cur+1

            pos_xmask = pix_mask_data_b[ymin_cur:ymax_cur].x_mask
            ypix=ymin_cur+dindgen(ys_pos)

            wlmap_slit=reform(poly3d(nxvec # (dblarr(ys_pos)+1d),(dblarr(nx)+1d) # pos_xmask,(dblarr(nx)+1d) # ypix,kwl3d,/irreg),nx,ys_pos)
            wlmap_slit_r=bino_rectify_slit(wlmap_slit,dist_map_b,ymin_cur,side='B',edge=(slit_tilt_b[j] eq 0.))
            disp_3d_fit_slit=dblarr(ndeg+1,ys_pos)+!values.d_nan
            for y=ymin_cur,ymax_cur do begin
                goodwl=where(finite(wlmap_slit_r[*,y-ymin_cur]) eq 1,cgoodwl)
                if(cgoodwl gt 10) then disp_3d_fit_slit[*,y-ymin_cur]=transpose(poly_fit(double(goodwl),wlmap_slit_r[goodwl,y-ymin_cur],ndeg))
            endfor

            if(slit_tilt_b[j] ne 0d) then begin
                ; adding wloffset to data_3d_b
                print,'Side:B slit,slit_tilt=',j,slit_tilt_b[j]
                dwl_cntpix_arr=dblarr(ys_pos)
                good_dwl=where(finite(dwl_cntpix_arr) eq 1 and disper_all_b_old[0,ymin_cur:ymax_cur] ne 0.0, cgood_dwl)
                slit_cur_idx=where(data_3d_b[4,*] eq j+n_slits_a and $
                                   finite(total(data_3d_b[0:3,*],1)) eq 1, cslit_cur_idx)
                if(cslit_cur_idx gt 0) then begin
                    dwl_slit_pix=data_3d_b[3,slit_cur_idx]-poly3d(data_3d_b[0,slit_cur_idx],data_3d_b[1,slit_cur_idx],data_3d_b[2,slit_cur_idx],kwl3d,/irreg)
                    if(cgood_dwl gt 0 and cgood_dwl le 3) then begin
                        resistant_mean,dwl_slit_pix,3.0,wloffset_slit
                        dwl_cntpix_arr[*]=wloffset_slit
                    endif else if(cgood_dwl ge 4) then begin
                        min_y_b=min(data_3d_b[2,slit_cur_idx]-ymin_cur,max=max_y_b,/nan)
                        ndeg_tilt=(max_y_b-min_y_b gt 200)? 2 : 1
                        dwl_fit=poly_fit(data_3d_b[2,slit_cur_idx]-ymin_cur,dwl_slit_pix,ndeg_tilt) ;  robust_poly_fit(data_3d_b[2,slit_cur_idx]-ymin_cur,dwl_slit_pix,1)
                        dwl_cntpix_arr=poly(dindgen(ys_pos),dwl_fit)
                        wloffset_slit=transpose(poly(transpose(data_3d_b[2,slit_cur_idx]-ymin_cur),dwl_fit))
                    endif else wloffset_slit=0.0
                    data_3d_b[3,slit_cur_idx]-=wloffset_slit ; ++--
                endif
                if(iter eq maxiter) then begin
                    disp_3d_fit_slit[0,*]-=dwl_cntpix_arr ; -+-+
                    dwl_cntpix_ori=dblarr(ys_pos,deg_corr+1)
                    if(deg_corr eq 0) then begin
                        for d=0,ndeg do dwl_cntpix_ori[*,0]+=transpose((disp_3d_fit_slit[d,*]-disper_all_b_old[d,ymin_cur:ymax_cur])*2048d^double(d))
                        disp_3d_fit_slit[0,*]-=dwl_cntpix_ori[*,0]
                    endif else if(deg_corr eq 1) then begin
                        for d=0,ndeg do begin
                            dwl_cntpix_ori[*,0]+=transpose((disp_3d_fit_slit[d,*]-disper_all_b_old[d,ymin_cur:ymax_cur])*1024d^double(d))
                            dwl_cntpix_ori[*,1]+=transpose((disp_3d_fit_slit[d,*]-disper_all_b_old[d,ymin_cur:ymax_cur])*3072d^double(d))
                        endfor
                        disp_3d_fit_slit[0,*]-=(dwl_cntpix_ori[*,0]*3072d -dwl_cntpix_ori[*,1]*1024d)/(3072d -1024d)
                        disp_3d_fit_slit[1,*]-=(dwl_cntpix_ori[*,1]-dwl_cntpix_ori[*,0])/(3072d -1024d)
                    endif
                endif
                print,'WL offset: ',(n_elements(dwl_cntpix_arr) eq 1)? dwl_cntpix_arr : median(dwl_cntpix_arr)
            endif
            disper_all_b[0:ndeg,ymin_cur:ymax_cur]=disp_3d_fit_slit[*,*]
            disper_all_b[ndeg+1,ymin_cur:ymax_cur]=0.01
            pix_mask_data_b[ymin_cur:ymax_cur].wl_sol=disp_3d_fit_slit[0:ndeg,*]
            pix_mask_data_b[ymin_cur:ymax_cur].wl_s_err=0.01
        endfor
    endfor
endif

writefits,wdir+'disper'+suff+'.fits',disper_all_a
mwrfits,disper_all_b,wdir+'disper'+suff+'.fits'
writefits,wdir+'disper_table'+suff+'.fits',0
mwrfits,pix_mask_data_a,wdir+'disper_table'+suff+'.fits'
mwrfits,pix_mask_data_b,wdir+'disper_table'+suff+'.fits'

end
