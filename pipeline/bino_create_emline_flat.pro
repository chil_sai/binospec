pro bino_create_emline_flat,logfile,wl_line_arr,win_line_arr=win_line_arr,$
    image_type=image_type,outsuff=outsuff,pady=pady,$
    slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,fit=fit,adj=adj,nterms=nterms

    wdir=def_wdir(logfile)

    if(n_elements(outsuff) ne 1) then outsuff=''
    if(n_elements(image_type) ne 1) then image_type='obj'
    file_dark=(file_test(wdir+image_type+'_dark.fits'))? wdir+image_type+'_dark.fits' : wdir+'obj_dark.fits'
    h_a0=headfits(file_dark,ext=1)
    h_b0=headfits(file_dark,ext=2)

    mask_a=read_mask_bino_ms(file_dark,side='A')
    mask_b=read_mask_bino_ms(file_dark,side='B')
    dist_map_a=mrdfits(wdir+'dist_map_A.fits',1,/silent)
    dist_map_b=mrdfits(wdir+'dist_map_B.fits',1,/silent)

    good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 

    if n_slits_a gt 0 then h_slit_a=headfits(wdir+image_type+'_slits_lin.fits',ext=1) else h_slit_a=''
    if n_slits_b gt 0 then h_slit_b=headfits(wdir+image_type+'_slits_lin.fits',ext=1+n_slits_a) else h_slit_b=''
    ny_a=sxpar(h_a0,'NAXIS2')
    ny_b=sxpar(h_b0,'NAXIS2')
    disper_a=mrdfits(wdir+'disper_table.fits',1,/silent)
    disper_b=mrdfits(wdir+'disper_table.fits',2,/silent)

    n_wl_lines = n_elements(wl_line_arr)
    for i=0,n_wl_lines-1 do begin
        line_flux_data=bino_measure_emline(logfile,image_type,wl_line_arr[i],$
            win_line=(n_elements(win_line_arr) eq n_wl_lines)? win_line_arr[i] : win_line_arr,$
            slit_poly=slit_poly,auto_slit_poly=auto_slit_poly, nterms=nterms)

        f_line=wdir+'skyline'+outsuff+'_flux_data_'+string(i+1,format='(i02)')+'.fits'
        writefits,f_line,0,hpri
        mwrfits,line_flux_data.slits_a,f_line
        mwrfits,line_flux_data.slits_b,f_line
        ny_a=n_elements(line_flux_data.slits_a)
        ny_b=n_elements(line_flux_data.slits_b)
        if(i eq 0) then begin
            slits_a_data_all=dblarr(n_elements(line_flux_data.slits_a.sigma),n_wl_lines)
            slits_b_data_all=dblarr(n_elements(line_flux_data.slits_b.sigma),n_wl_lines)
        endif
        slits_a_data_all[*,i]=line_flux_data.slits_a.sigma*line_flux_data.slits_a.flux/median(line_flux_data.slits_a.sigma*line_flux_data.slits_a.flux)
        slits_b_data_all[*,i]=line_flux_data.slits_b.sigma*line_flux_data.slits_b.flux/median(line_flux_data.slits_b.sigma*line_flux_data.slits_b.flux)
        if(keyword_set(fit)) then begin
            if n_slits_a gt 0 then begin
            good_a=where((finite(slits_a_data_all[*,i]) eq 1) and $
                         (finite(line_flux_data.slits_a.x_mask) eq 1), c_good_a)
            fit_ydeg_a=5
            x_uni_idx_a=uniq(line_flux_data.slits_a[good_a].x_mask,sort(line_flux_data.slits_a[good_a].x_mask))
            fit_xdeg_a=(n_elements(x_uni_idx_a) ge 3)? 2 : 0
            t_a=robust_poly_fit(double(good_a),slits_a_data_all[good_a,i],fit_ydeg_a)
            if(fit_xdeg_a gt 0) then begin
                good_a_i2=where(abs(slits_a_data_all[*,i]-poly(dindgen(ny_a),t_a)) lt $
                                5.0*robust_sigma(slits_a_data_all[good_a,i]-poly(good_a,t_a)),cgood_a_i2)
                data_s_a=dblarr(3,cgood_a_i2)
                data_s_a[0,*]=line_flux_data.slits_a[good_a_i2].x_mask
                data_s_a[1,*]=line_flux_data.slits_a[good_a_i2].y_mask
                data_s_a[2,*]=transpose(slits_a_data_all[good_a_i2,i])
                pp_a=sfit_2deg(data_s_a,fit_xdeg_a,fit_ydeg_a,/irreg,kx=kx_a)
                slits_a_data_all[*,i]=poly2d(line_flux_data.slits_a.x_mask,line_flux_data.slits_a.y_mask,kx_a,$
                                             /irreg,deg1=fit_xdeg_a,deg2=fit_ydeg_a)
            endif else slits_a_data_all[*,i]=poly(dindgen(ny_a),t_a)
            endif
            if n_slits_b gt 0 then begin
            good_b=where((finite(slits_b_data_all[*,i]) eq 1) and $
                         (finite(line_flux_data.slits_b.x_mask) eq 1), c_good_b)
            fit_ydeg_b=5
            x_uni_idx_b=uniq(line_flux_data.slits_b[good_b].x_mask,sort(line_flux_data.slits_b[good_b].x_mask))
            fit_xdeg_b=(n_elements(x_uni_idx_b) ge 3)? 2 : 0
            t_b=robust_poly_fit(double(good_b),slits_b_data_all[good_b,i],fit_ydeg_b)
            if(fit_xdeg_b gt 0) then begin
                good_b_i2=where(abs(slits_b_data_all[*,i]-poly(dindgen(ny_b),t_b)) lt $
                                5.0*robust_sigma(slits_b_data_all[good_b,i]-poly(good_b,t_b)),cgood_b_i2)
                data_s_b=dblarr(3,cgood_b_i2)
                data_s_b[0,*]=line_flux_data.slits_b[good_b_i2].x_mask
                data_s_b[1,*]=line_flux_data.slits_b[good_b_i2].y_mask
                data_s_b[2,*]=transpose(slits_b_data_all[good_b_i2,i])
                pp_b=sfit_2deg(data_s_b,fit_xdeg_b,fit_ydeg_b,/irreg,kx=kx_b)
                slits_b_data_all[*,i]=poly2d(line_flux_data.slits_b.x_mask,line_flux_data.slits_b.y_mask,kx_b,$
                                             /irreg,deg1=fit_xdeg_b,deg2=fit_ydeg_b)
            endif else slits_b_data_all[*,i]=poly(dindgen(ny_b),t_b)
            endif
        endif
    endfor

    slits_a_data=(n_wl_lines eq 1)? slits_a_data_all[*,0] : median(slits_a_data_all,dim=2)
    slits_b_data=(n_wl_lines eq 1)? slits_b_data_all[*,0] : median(slits_b_data_all,dim=2)

    flat_corr_data={slits_a:slits_a_data, slits_b:slits_b_data}

    good_a=where(finite(flat_corr_data.slits_a+line_flux_data.slits_a.y_mask) eq 1, cgood_a)
    good_b=where(finite(flat_corr_data.slits_b+line_flux_data.slits_b.y_mask) eq 1, cgood_b)


    hpri=headfits(wdir+image_type+'_slits.fits')

    f_out=wdir+'flat_emline'+outsuff+'_slits.fits'
    writefits,f_out,0,hpri

    for sideext=1,2 do begin
        mask_cur=(sideext eq 1)? mask_a : mask_b
        ;h_obj=headfits(wdir+image_type+'_ff.fits',ext=sideext)
        h_obj=(sideext eq 1)? h_a0 : h_b0

        nx=sxpar(h_obj,'NAXIS1')
        ny=sxpar(h_obj,'NAXIS2')

        f_dt=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
        disp_rel=mrdfits(wdir+f_dt,sideext,/silent)
        line_flux_data_side=(sideext eq 1)? line_flux_data.slits_a : line_flux_data.slits_b
        flat_corr_data_side=(sideext eq 1)? flat_corr_data.slits_a : flat_corr_data.slits_b 
        good_side=(sideext eq 1)? good_a : good_b

        xx=findgen(nx)

        dist_map=(sideext eq 1)? dist_map_a : dist_map_b
        y_scl=((sideext eq 1)? 24.555832d : 24.548194d)
        if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl
        ;;;y_scl=24.5772 ;;  Y plate scale pix/mm

        nslit=(sideext eq 1)? n_slits_a : n_slits_b
        nslitoff=(sideext eq 1)? 0 : n_slits_a

        if nslit eq 0 then continue
        slit_reg=bino_get_slit_region(mask_cur,nx=nx,ny=ny,pady=pady,dist_map=dist_map,slit_trace=slit_trace)

        for j=0,nslit-1 do begin
            print,'Writing corrected flat for slit: ',j+1+nslitoff
            h_slit=headfits(wdir+image_type+'_slits.fits',ext=j+1+nslitoff)
            nx_cur=sxpar(h_slit,'NAXIS1')
            ny_cur=sxpar(h_slit,'NAXIS2')
            dy = (slit_trace[j].y_trace - (slit_reg[0,j]+slit_reg[1,j])/2d)/y_scl

            yoff=sxpar(h_slit,'YOFFSET')

            ymap=(dblarr(nx_cur)+1d) # (disp_rel[yoff:yoff+ny_cur-1].y_mask)
            ymap += (dy) # (dblarr(ny_cur)+1d)

            slit_map=interpol(flat_corr_data_side[good_side],line_flux_data_side[good_side].y_mask,ymap)

            mwrfits,slit_map,f_out,h_slit,/silent
        endfor

    endfor


end


