;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  This function returns the initial guess for the Binospec wavelength solution
;  Input parameters are:
;           grating -- grating ID, one of 'H', 'K', or 'HK'
;           xpos  -- X position of the slit (in a mask or a longslit)
;  Optional input keywords:
;           order -- order of dispersion. Defaults to 1
;  Return value:
;           polynomial coefficients of the wavelength solution
;           if xpos contains more than one element then the output array
;           will be two-dimensional (N_deg+1) by n_elements(xpos)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function bino_wlsol_iguess,grating,tilt,xpos0,ypos=ypos,side=side,order=order,period=period

if(n_elements(side) ne 1) then side='A'
if(n_elements(order) ne 1) then order=1
if(n_params() eq 1) then xpos0=[0.0]
if(n_elements(ypos) ne n_elements(xpos0)) then ypos=xpos0*0.0d
if(n_elements(period) ne 1) then period='2017B'

xpos=xpos0
;;; slit curvature correction
;for i=0,n_elements(xpos)-1 do $
;    xpos[i]=xpos0[i]+poly(ypos[0],[-0.0130282,0.00200443,-0.000107925])

if(grating eq '270') then begin
    dtilt = (side eq 'A')? 0.101832d -5d : 0.06768d -5d
    sclcorr = (side eq 'A')? 1d/1.0005d : 1d/1.002d
    camsclcorr = (side eq 'A')? 1d/1.001d : 1/1.0005d
    gpm=270d
    ndeg=4
endif

if(grating eq '600') then begin
    dtilt = (side eq 'A')? 0.1115d -5d : 0.0621d -5d
;    sclcorr = (side eq 'A')? 1d/1.0005d : 0.9993d/1.001431d
;    camsclcorr = (side eq 'A')? 1d : 1.001431d
    sclcorr = (side eq 'A')? 1d/1.0005d : 1d/1.002d ;;;1d/1.001431d
    camsclcorr = (side eq 'A')? 1d/1.001d : 1/1.0005d ;;;1.001431d
    gpm=600d
    ndeg=3
endif

if(grating eq '1000') then begin
    dtilt = (side eq 'A')? 0.1248d -5d: 0.0814d -5d
;    sclcorr = (side eq 'A')? 1d/1.0005d : 0.9997d/1.001431d
;    camsclcorr = (side eq 'A')? 1d : 1.001431d
    sclcorr = (side eq 'A')? 1d/1.0005d : 1d/1.002d ;;;1d/1.001431d
    camsclcorr = (side eq 'A')? 1d/1.001d : 1/1.0005d ;;;1.001431d
    gpm=1000d
    ndeg=3
endif

xeq3d=(side eq 'A')? xpos-56.236d : -(56.236d +xpos)
dphi=(side eq 'A')? +0.0528d : +0.0748d

wl_ini=bino_grateq3d(xeq3d,ypos,3500.0+findgen(100)*70d,tilt+dtilt,gpm,dphi=dphi,/pix,deg=ndeg,sclcorr=sclcorr,camsclcorr=camsclcorr)

dis_arr=wl_ini.wlsol

;print,'xpos,ypos,tilt',xpos-xcnt,ypos,tilt+dtilt,side,gpm,ndeg
;stop
return, dis_arr

end
