function read_mask_bino_ms,filename,side=side

    if(n_elements(side) ne 1) then side='A'
    if(side ne 'A') then side='B'
    n_ext=2
    n_ext+=(side eq 'B')? 2 : 1

    mask_fits=mrdfits(filename,n_ext,/silent)
    if((size(mask_fits))[2] ne 8) then return,-1
    n_slits=mask_fits.ntargets
    ;longslit=(mask_fits.mask_name eq 'Longslit1')? 1 : 0
    longslit=(strlowcase(strmid(mask_fits.mask_name,0,8)) eq 'longslit')? 1 : 0

    if n_slits gt 0 then mask=replicate({slit:0,side:side,ra:0d,dec:0d,x:0d,y:0d,target:0ll,object:'none',type:'NONE',$
                    wstart:0.0,wend:0.0,height:0.0,width:0.0,offset:0.0,theta:0.0,$
                    bbox:dblarr(8),$
                    mask_id:-1l,mask_name:'',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
                    mask_pa:!values.d_nan,corners:dblarr(4)},n_slits) $
                    else begin
                      mask={mask_id:-1l,mask_name:'',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
                      mask_pa:!values.d_nan,corners:dblarr(4)}
                      mask.corners=mask_fits.mask_corners
                      ;;; quick and dirty fix for mask corners problem
                      if(side eq 'B' and mask.corners[0] gt 0) then begin
                         minXc=min(mask_fits.mask_corners[[0,2]],max=maxXc)
                         mask.corners[0]=-maxXc
                         mask.corners[2]=-minXc
                      endif
                    endelse
;    s_y=sort(mask_fits.slity[0:n_slits-1])
    mask.mask_id=mask_fits.mask_id
    mask.mask_name=mask_fits.mask_name
    mask.mask_ra=mask_fits.centerra
    mask.mask_dec=mask_fits.centerdec
    mask.mask_pa=mask_fits.centerpa
    if n_slits eq 0 then return, mask
    s_y=sort(total(mask_fits.poly_y[0:n_slits-1,*],2))
    mask.slit=mask_fits.slit_id[s_y]
    mask.ra=mask_fits.ra[s_y]
    mask.dec=mask_fits.dec[s_y]
    mask.x=mask_fits.slitx[s_y]
    mask.y=mask_fits.slity[s_y]
    mask.target=mask_fits.target_id[s_y]
    mask.object=mask_fits.target_name[s_y]
    mask.type=mask_fits.target_type[s_y]
    mask.wstart=3000.0
    mask.wend=10500.0
    mask.height=mask_fits.slit_length[s_y]*mask_fits.mm_per_arcsec
    mask.width=mask_fits.slit_width[s_y]*mask_fits.mm_per_arcsec
    mask.theta=mask_fits.slit_pa[s_y]
    for i=0,n_slits-1 do begin
        mask[i].bbox[[0,2,4,6]]=mask_fits.poly_x[s_y[i],*]
        mask[i].bbox[[1,3,5,7]]=mask_fits.poly_y[s_y[i],*]
        m_l1=(mask[i].bbox[0]-mask[i].bbox[2])^2+(mask[i].bbox[1]-mask[i].bbox[3])^2
        m_l2=(mask[i].bbox[2]-mask[i].bbox[4])^2+(mask[i].bbox[3]-mask[i].bbox[5])^2
        if(m_l1 lt m_l2) then mask[i].bbox=shift(mask[i].bbox,2)
        if(abs(mask[i].bbox[0]-mask[i].bbox[2]) gt 0.2*mask[i].width) then begin
;            dtheta=(atan((mask[i].bbox[2]-mask[i].bbox[4])/(mask[i].bbox[3]-mask[i].bbox[5]))*!radeg) mod 180.0
            dtheta=(atan((mask[i].bbox[0]-mask[i].bbox[2])/(mask[i].bbox[1]-mask[i].bbox[3]))*!radeg) mod 180.0
            if(dtheta lt -90d) then dtheta+=180d
            if(dtheta gt  90d) then dtheta-=180d
            mask[i].theta-=dtheta
;            mask[i].theta=mask[i].theta+atan((mask[i].bbox[0]-mask[i].bbox[4])/abs(mask[i].bbox[1]-mask[i].bbox[5]))*!radeg
        endif
        mask[i].corners=mask_fits.mask_corners
        ;;; quick and dirty fix for mask corners problem
        if(side eq 'B' and mask[i].corners[0] gt 0) then begin
            minXc=min(mask_fits.mask_corners[[0,2]],max=maxXc)
            mask[i].corners[0]=-maxXc
            mask[i].corners[2]=-minXc
        endif
    endfor

    if(longslit and n_slits eq 3) then mask=mask[1]
    return,mask
end
