pro extract_slit_image,img_full,img_slit,y_offset,ny_slit,fill=fill

if(n_params() eq 3) then ny_slit=-1
s_f=size(img_full)
nx=s_f[1]
ny=s_f[2]

if(keyword_set(fill)) then begin
    s_slit=size(img_slit)
    ny_slit=s_slit[2]
    y_min_img=y_offset
    y_max_img=y_offset+ny_slit-1L

    y_min_slit=0L
    y_max_slit=ny_slit-1L

    dy_min = (y_min_img lt 0)? -y_min_img : 0
    dy_max = (y_max_img gt (ny-1))? (y_max_img-(ny-1L)) : 0

    n0_1 = y_min_img+dy_min
    n1_1 = y_max_img+dy_max
    n0_2 = y_min_slit+dy_min
    n1_2 = y_max_slit+dy_max

;    img_full[*,y_min_img+dy_min:y_max_img+dy_max]=$
;            img_slit[*,y_min_slit+dy_min:y_max_slit+dy_max]

    safe_array_bounds, s_f[2], s_slit[2], n0_1,n1_1,n0_2,n1_2
    if((n0_1 le n1_1) and (n0_2 le n1_2)) then img_full[*,n0_1:n1_1]=img_slit[*,n0_2:n1_2]
endif else begin
    if(ny_slit eq -1) then message,'NY_slit must be provided'
    img_slit=fltarr(Nx,ny_slit)

    y_min_img=y_offset
    y_max_img=y_offset+ny_slit-1L

    y_min_slit=0L
    y_max_slit=ny_slit-1L

    dy_min = (y_min_img lt 0)? -y_min_img : 0
    dy_max = (y_max_img gt (ny-1))? (y_max_img-(ny-1L)) : 0

    n0_1 = y_min_slit+dy_min
    n1_1 = y_max_slit+dy_max
    n0_2 = y_min_img+dy_min
    n1_2 = y_max_img+dy_max

;    img_slit[*,y_min_slit+dy_min:y_max_slit+dy_max]=$
;            img_full[*,y_min_img+dy_min:y_max_img+dy_max]    

    safe_array_bounds, ny_slit, s_f[2], n0_1,n1_1,n0_2,n1_2
    if((n0_1 le n1_1) and (n0_2 le n1_2)) then img_slit[*,n0_1:n1_1]=img_full[*,n0_2:n1_2]

endelse


end

