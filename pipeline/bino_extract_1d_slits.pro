function generate_extr_krnl,dithpos,e_apw,e_meth=e_meth,$
    ccdscl=ccdscl,c_krnl=c_krnl,n_krnl=n_krnl

if(n_elements(e_meth) ne 1) then e_meth=1
if(n_elements(ccdscl) ne 1) then ccdscl=0.2

n_dith=n_elements(dithpos)

c_krnl = fix(max(abs(dithpos))/ccdscl) + fix(e_apw*3)
n_krnl = 2*c_krnl+1

extr_krnl = dblarr(n_krnl)

for i=0,n_dith-1 do begin
    if(e_meth eq 2) then begin
        d = psf_gaussian(npix=[n_krnl],ndim=1,$
            centroid=[c_krnl+dithpos[i]/ccdscl],fwhm=[e_apw],/nor,/double)
    endif else begin
        d = dblarr(n_krnl)
        d[c_krnl-floor(e_apw/2.0):c_krnl+floor(e_apw/2.0)]=1.0
        d0=d ;;;/total(d)
        d=shift_s(d0,dithpos[i]/ccdscl)
    endelse
    extr_krnl[*] = extr_krnl[*] + d*((-1.0)^i)
endfor

return,extr_krnl
end


pro bino_extract_1d_slits,logfile,obstype,$
    dith_from_box=dith_from_box, box_exp=box_exp, $
    diffmode=diffmode, detect=detect, optimal=optimal, $
    estimate=estimate, fitprofile=fitprofile, fulltrace=fulltrace, nsegments=nsegments,$
    error=error,clean=clean,n_apwmax=n_apwmax,$
    suffix=suffix,errsuffix=errsuffix,writesuffix=writesuffix,debug=debug, split1d=split1d

log=readlog(logfile)
wdir=sxpar(log,'W_DIR')
if(n_elements(suffix) ne 1) then suffix='_lin'
if(n_elements(errsuffix) ne 1) then errsuffix=suffix
if(n_elements(writesuffix) ne 1) then writesuffix=''

e_meth = sxpar(log,'EXTMETH')
if(keyword_set(optimal)) then e_meth=2 ;;;; forcing optimal extraction

e_apw = sxpar(log,'EXTAPW')

val=sxpar(log,'DITHPOS',count=cntval)
dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0

if(keyword_set(diffmode)) then begin
    val2=sxpar(log,'DITHPOS2',count=cntval2)
    dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
    dithpos=[dithpos,dithpos2]
endif

ccdscl=0.24436 ;;; 0.24 arcsec per pix -- hardcoded CCD scale

mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 
n_slits=n_slits_a+n_slits_b

inpfile=wdir+obstype+'_slits'+suffix+'.fits'
varfile=(obstype eq 'obj-sky')? wdir+'obj_slits'+errsuffix+'.fits' : inpfile
errflag=0
absflux=0
if (strmid(obstype,0,10) eq 'obj_counts' or strmid(obstype,0,7) eq 'obj_abs') then begin
    varfile=wdir+obstype+'_err_slits'+errsuffix+'.fits'
    errflag=1
    if(strmid(obstype,0,7) eq 'obj_abs') then absflux=1
endif
hpri=headfits(inpfile)
hpri_mod=hpri[0:n_elements(hpri)-2] ;remove the extra 'END'
delkw=['RA','RA ','DEC','EPOCH','PA','WCSNAME', 'CUNIT2', 'CTYPE1', 'CTYPE2', 'CRPIX1','CRPIX2','CRVAL1','CRVAL2','CD1_1','CD2_2','NAXIS','BITPIX','EXTNAME','EXTVER','INHERIT']
n_bkw=n_elements(delkw)
if(n_bkw gt 1) then for i=1,n_bkw-1 do sxdelpar,hpri_mod,delkw[i]

if(keyword_set(dith_from_box)) then begin
    if(n_elements(box_exp) ne 1) then box_exp='obj-sky'
    boxlist=where(mask.type eq 'BOX',cbox)
    for i=0,cbox-1 do begin
        t=mrdfits(wdir+box_exp+'_slits'+suffix+'.fits',boxlist[i]+1,hb,/silent)
        val=sxpar(hb,'DITHCOMP',count=cntval)
        dithpos_tmp=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0
        if(i eq 0) then dithpos_arr=dblarr(n_elements(dithpos_tmp),cbox)
        dithpos_arr[*,i]=dithpos_tmp[*]/double(cbox)
    endfor
    if(keyword_set(diffmode)) then begin
        d_dithpos=dithpos[1]-dithpos[0]
    endif
    dithpos=(cbox gt 0)? total(dithpos_arr,2) : [0.0]
    if(keyword_set(diffmode)) then dithpos=[dithpos,dithpos+d_dithpos]
    print,'Using empirically estimated dithering positions: ',dithpos
endif

extr_krnl_ref=generate_extr_krnl(dithpos,e_apw,ccdscl=ccdscl,e_meth=e_meth,$
    c_krnl=c_krnl,n_krnl=n_krnl)*e_apw

outfile=wdir+obstype+'_slits_extr'+writesuffix+'.fits'
writefits,outfile,0,hpri
for i=0,n_slits-1 do begin
    print,'Extracting from slit #',i+1,'/',n_slits
    slit_img=mrdfits(inpfile,i+1,himg,/silent)
    var_img=mrdfits(varfile,i+1,hvar,/silent)
    if (i eq 0) then himg0=himg
    if(errflag eq 1) then var_img=double(var_img)^2
    nx=sxpar(himg,'NAXIS1')
    ny=sxpar(himg,'NAXIS2')
    slitypix=sxpar(himg,'SLITYPIX')
    parse_spechdr,himg,wl=wl
    if(i eq 0) then begin
        out_img=dblarr(nx,n_slits)
        err_img=dblarr(nx,n_slits)
        sky_img=dblarr(nx,n_slits)
    endif

    mask_entry=(i lt n_slits_a)? mask_a[i] : mask_b[i-n_slits_a]
    target_offset=(mask_entry.y-(mask_entry.bbox[5]+mask_entry.bbox[1])/2.0)*5.98802 ;; target offset in arcsec, 5.98802=scale in arcsec/mm
    ;print, (mask_entry.bbox[5]-mask_entry.bbox[1])*5.98802/ccdscl, ny
    extr_krnl_ref=generate_extr_krnl(dithpos+target_offset,e_apw,ccdscl=ccdscl,e_meth=e_meth,$
        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw
    junk=''
    skipestimate=0
    obj_pos=(slitypix-(ny-1)/2.+1.)*ccdscl ;dithpos+target_offset
    ;print, 'slitypix', slitypix, (dithpos+target_offset)/ccdscl+(ny-1)/2.-1.
    obj_fwhm=e_apw
    if(keyword_set(detect)) then begin
        ;calculate profile first, even if we use it just for detecting centroid
        extr_krnl_est=bino_estimate_extraction_profile(slit_img,wl*10.0,/maskoh,fulltrace=fulltrace,nsegments=nsegments,debug=debug)
        obj_pos=sxpar(himg,'OBJPOS',count=cnt_obj)
        obj_fwhm=sxpar(himg,'OBJFWHM')
        if(cnt_obj eq 1) then print,'Object extracted in slit '+string(i+1),'  pos=',obj_pos,' fwhm=',obj_fwhm else begin
            obj_pos=(slitypix-(ny-1)/2.-1.)*ccdscl; dithpos+target_offset ;0.0 edit SM 06/25/18
            obj_fwhm=e_apw
        endelse
        ;if detect=true but estimate=false use the spatial profile to at least centroid
        find_peak, dindgen(n_elements(extr_krnl_est)), extr_krnl_est, 0.0, ipix, peaks, ypeak, bgpeak, npeak
        if (npeak ge 1) then begin
            ;limit to brightest 3
            peaks=peaks[(reverse(sort(ypeak)))[0:((npeak-1)<2)]]

            ;pick the closest peak to expected position
            detect_pos=(peaks-n_elements(extr_krnl_est)/2.0)*ccdscl
            detect_off=min(abs(detect_pos-obj_pos[0]),best)
            ;print, 'obj_pos find peak:', peaks[best]

            ;centroid on this peak to refine
            gcntrd_1d,extr_krnl_est,peaks[best],xcen,obj_fwhm*2.0,/silent,/keepcenter
            ;print, 'obj_pos centroid peak:', xcen

            ;if the best peak is far away from expected position, ignore it
            if (abs(detect_pos[best]-obj_pos[0]) lt obj_fwhm) then begin
                if (xcen ne -1) then $
                    obj_pos=(xcen-n_elements(extr_krnl_est)/2.0)*ccdscl $
                else skipestimate=1
                print, 'detected object this many pixels from expected position:', detect_off*ccdscl
            endif else skipestimate=1 ;don't use estimate profile if no peak found in expected position

            ;print, 'final obj_pos:', (obj_pos/ccdscl+ny/2.0)
            c_krnl=obj_pos
        endif else skipestimate=1
        
        if keyword_set(estimate) and ~(skipestimate) then begin
            extr_krnl=extr_krnl_est*obj_fwhm
            if keyword_set(n_apwmax) then begin
                llim=floor((obj_pos[0]-n_apwmax*obj_fwhm)/ccdscl+n_elements(extr_krnl_est)/2.0)>0
                ulim=ceil((obj_pos[0]+n_apwmax*obj_fwhm)/ccdscl+n_elements(extr_krnl_est)/2.0)<(n_elements(extr_krnl)-1)
                apmask=extr_krnl_est*0.0d
                apmask[llim:ulim]=1.0d
                extr_krnl=extr_krnl_est*apmask
                if(keyword_set(fitprofile)) then begin ;; fitting the profile with the Moffat function
                    g_extr_krnl=llim+lindgen(ulim+1-llim) ;;range(llim,ulim) ;;;where(finite(extr_krnl) eq 1, cg_extr_krnl)
                    ek_fit=mpfitpeak(double(g_extr_krnl),extr_krnl[g_extr_krnl],ek_c,/moffat,nterms=5)
                    extr_krnl_fit=extr_krnl*0d ;;;ek_c[0]/(((dindgen(ny)-ek_c[1])/ek_c[2])^2+1d)^ek_c[3]
                    extr_krnl_fit[llim:ulim]=ek_fit-(ek_c[4]>0)
                    extr_krnl=extr_krnl_fit
                endif
            endif
            ;adjust obj_fwhm for actual kernel
            halfmax=where(extr_krnl ge 0.5*max(extr_krnl),hmcnt)
            if hmcnt ge 2 then obj_fwhm=(max(halfmax)-min(halfmax))
        endif else $
            extr_krnl=(e_meth eq 2)? generate_extr_krnl([obj_pos],obj_fwhm,ccdscl=ccdscl,e_meth=e_meth,$
                                                        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw : $
                                     generate_extr_krnl([obj_pos],e_apw,ccdscl=ccdscl,e_meth=e_meth,$
                                                        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw
    endif

    if((size(extr_krnl))[0] le 1 and n_elements(extr_krnl) ne ny) then begin
        if(~keyword_set(detect)) then extr_krnl=extr_krnl_ref
        krnl_cur=dblarr(ny)
        c_k_c=(ny-1)/2
        if(ny ge n_krnl) then begin
            krnl_cur[c_k_c-c_krnl:c_k_c+c_krnl]=extr_krnl
        endif else begin
            krnl_cur[*]=extr_krnl[c_krnl-c_k_c:c_krnl+c_k_c]
        endelse
        extr_krnl=krnl_cur
    endif

    ;renormalize kernel
    extr_krnl=extr_krnl/total(extr_krnl)
     ;junk=''
     ;splot, extr_krnl
     ;soplot, extr_krnl_est, color='red'
     ;read, junk
    spec_extr=bino_extract_lin_spec(slit_img,var2d=var_img,extr_krnl,sky=sky_extr,error=error_extr,clean=clean,absflux=absflux,debug=debug)
    out_img[*,i]=spec_extr
    err_img[*,i]=error_extr
    sky_img[*,i]=sky_extr
    ;add aperture keywords to header
    minmaxgood=minmax(where(finite(spec_extr)))
    mask_extr=(finite(spec_extr) eq 0)
    if(minmaxgood[0] eq -1) then minmaxgood=[0,0] ;else if minmaxgood[0] ne minmaxgood[1] then minmaxgood[0]=minmaxgood[0]+1 ;hack because 1st pixel often bad
;;;    sxaddpar, himg0, 'OBPOS'+strn(i+1,length=3,padchar='0'), obj_pos/ccdscl+ny/2.0, 'obj center pos in pixels'
    sxaddpar, himg0, 'OBPOS'+strn(i+1), obj_pos[0]/ccdscl+(ny-1)/2.0+1.0, 'obj center pos in pixels'
    sxaddpar, himg0, 'OFWHM'+strn(i+1), obj_fwhm, 'fwhm of extract profile'
    sxaddpar, himg0, 'APNUM'+strn(i+1), strn(i+1)+' '+string(minmaxgood[0],format='(I4)')+' '+string(minmaxgood[1],format='(I4)')+' '+string(c_krnl-e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')+' '+string(c_krnl+e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')
    sxaddpar, himg0, 'APID'+strn(i+1), strcompress(mask_entry.object,/remove)+' '+string(mask_entry.ra,format='(F11.6)')+' '+string(mask_entry.dec,format='(F11.6)')+' '+string(mask_entry.target, format='(I9)')+' '+string(mask_entry.slit,format='(I4)')+' '+mask_entry.side
    sxaddpar, himg0, 'APSLT'+strn(i+1), string(mask_entry.x,format='(F11.6)')+' '+string(mask_entry.y,format='(F11.6)')+' '+string(mask_entry.height, format='(F7.3)')+' '+string(mask_entry.width, format='(F7.3)')+' '+string(mask_entry.theta, format='(F7.3)')+' '+string(sxpar(himg,'TILTEDSL'), format='(I1)')
    if keyword_set(split1d) then begin
       himg=[hpri_mod,himg]
      sxaddpar, himg, 'APNUM1', strn(i+1)+' '+string(minmaxgood[0],format='(I4)')+' '+string(minmaxgood[1],format='(I4)')+' '+string(c_krnl-e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')+' '+string(c_krnl+e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')
      sxaddpar, himg, 'APID1', strcompress(mask_entry.object,/remove)+' '+string(mask_entry.ra,format='(F11.6)')+' '+string(mask_entry.dec,format='(F11.6)')+' '+string(mask_entry.target, format='(I9)')+' '+string(mask_entry.slit,format='(I4)')+' '+mask_entry.side
      sxaddpar, himg, 'APSLT1', string(mask_entry.x,format='(F11.6)')+' '+string(mask_entry.y,format='(F11.6)')+' '+string(mask_entry.height, format='(F7.3)')+' '+string(mask_entry.width, format='(F7.3)')+' '+string(mask_entry.theta, format='(F8.3)')+' '+string(sxpar(himg,'TILTEDSL'), format='(I1)') 
      sxaddpar, himg, 'OBJPOS', (obj_pos[0]/ccdscl+ny/2.0), 'obj center pos in pixels'
      sxaddpar, himg, 'OBJFWHM', obj_fwhm, 'fwhm of extract profile'
      wave1d=wl[minmaxgood[0]:minmaxgood[1]]
      sxdelpar,himg,'NAXIS'
      sxdelpar,himg,'NAXIS1'
      sxdelpar,himg,'NAXIS2'
      sxdelpar,himg,'BITPIX'
      sxaddpar,himg,'BITPIX', -32, after='SIMPLE'
      sxaddpar,himg,'NAXIS', 2, after='BITPIX'
      sxaddpar,himg,'NAXIS1', minmaxgood[1]-minmaxgood[0]+1,after='NAXIS'
      sxaddpar,himg,'NAXIS2', 4,after='NAXIS1'
      sxdelpar, himg, 'EXTNAME'
      sxdelpar, himg, 'XTENSION'
      sxaddpar, himg, 'BANDID1', 'flux'
      sxaddpar, himg, 'BANDID2', 'error'
      sxaddpar, himg, 'BANDID3', 'sky'
      sxaddpar, himg, 'BANDID4', 'mask'
      ;trim each spectrum and wcs to only the valid wavelength range
      sxaddpar, himg, 'CRVAL1', wl[minmaxgood[0]]*10.0
      sxaddpar, himg, 'CDELT1', sxpar(himg,'CDELT1')*10.0
      sxaddpar, himg, 'CD1_1', sxpar(himg,'CD1_1')*10.0
      sxaddpar, himg, 'CD2_2', 1
      sxaddpar, himg, 'CUNIT1', 'angstrom'
      sxaddpar, himg, 'APID2', 'error'
      sxaddpar, himg, 'APID3', 'sky'
      sxaddpar, himg, 'APID4', 'mask'
      sxaddpar, himg, 'APNUM2', '2 0 1.00 1.00'
      sxaddpar, himg, 'APNUM3', '3 0 1.00 1.00'
      sxaddpar, himg, 'APNUM4', '4 0 1.00 1.00'
      sxaddpar, himg, 'WAT0_001', 'system=equispec'
      if strmid(mask_entry.mask_name,0,4) eq 'Long' then sxaddpar, himg, 'OBJECT', strcompress(sxpar(himg,'CATID'),/remove)+'_'+mask_entry.side else sxaddpar, himg, 'OBJECT', strcompress(sxpar(himg,'SLITOBJ'),/remove)+'_'+strcompress(string(sxpar(himg,'SLITTARG')),/remove)
;      sxaddpar, himg, 'RA', strcompress(sxpar(himg,'SLITRA'),/remove)
;      sxaddpar, himg, 'DEC', strcompress(sxpar(himg,'SLITDEC'),/remove)
      sxaddpar, himg, 'RA', double(sxpar(himg,'SLITRA'))
      sxaddpar, himg, 'DEC', double(sxpar(himg,'SLITDEC'))
      sxaddpar, himg, 'COMMENT', 'APNUM: #, spec start pixel, end pixel, min extract coord, max extr coord'
      sxaddpar, himg, 'COMMENT', 'APID: Targ #, RA, DEC, Object, Slit#, Side'
      sxaddpar, himg, 'COMMENT', 'APSLT: Slit X, Slit Y (mm), Height, Width, Angle, Tilted?(0/1)'
      if(NOT file_test(wdir+obstype+'_1D',/directory)) then file_mkdir, wdir+obstype+'_1D'
      if strmid(mask_entry.mask_name,0,4) eq 'Long' then outfile1D=wdir+obstype+'_1D/'+strcompress(sxpar(himg,'CATID'),/remove)+'_'+mask_entry.side+writesuffix+'.fits'  else outfile1D=wdir+obstype+'_1D/slit'+mask_entry.side+strcompress(string(mask_entry.slit,format='(i03)'),/remove)+'_'+strcompress(mask_entry.object,/remove)+'t'+strcompress(string(mask_entry.target),/remove)+writesuffix+'.fits'
      mwrfits, float([[spec_extr[minmaxgood[0]:minmaxgood[1]]],[error_extr[minmaxgood[0]:minmaxgood[1]]],[sky_extr[minmaxgood[0]:minmaxgood[1]]],[mask_extr[minmaxgood[0]:minmaxgood[1]]]]), outfile1D, himg, /create
    endif
;    goodk = where(abs(krnl_cur) gt 1e-4)
;
;    for x=0,nx-1 do begin
;        out_img[x,i]=(keyword_set(error))? $
;            sqrt(total((transpose(slit_img[x,goodk])*krnl_cur[goodk])^2,nan=diffmode)) : $
;            total(transpose(slit_img[x,goodk])*krnl_cur[goodk],nan=diffmode)
;    endfor

 endfor
himg=himg0
sxaddpar, himg, 'COMMENT', 'APNUM: #, spec start pixel, spec end pixel,...'
sxaddpar, himg, 'COMMENT', '  min extract coord, max extract coord'
sxaddpar, himg, 'COMMENT', 'APID: Targ #, RA, DEC, Object, Slit#, Side'
sxaddpar, himg, 'COMMENT', 'APSLT: Slit X, Slit Y (mm), Height, Width, Angle, Tilted?(0/1)'
sxdelpar,himg,'YOFFSET'
sxaddpar,himg,'DITHPOS',sxpar(log,'DITHPOS')
if(n_slits eq 1) then sxdelpar,himg,'NAXIS2'

delkw=['TILTEDSL', 'AIRMASS', 'RA', 'DEC', 'EQUINOX','EPOCH','RADECSYS','SIDE']

for i=0,n_elements(himg)-1 do begin
    if(strmid(himg[i],0,4) eq 'SLIT') then delkw=[delkw,strcompress(strmid(himg[i],0,8),/remove)]
endfor
n_bkw=n_elements(delkw)
if(n_bkw gt 1) then for i=1,n_bkw-1 do sxdelpar,himg,delkw[i]

sxaddpar,himg,'EXTNAME','FLUX'
mwrfits,out_img,outfile,himg
sxaddpar,himg,'EXTNAME','ERROR'
mwrfits,err_img,outfile,himg
sxaddpar,himg,'EXTNAME','SKY'
mwrfits,sky_img,outfile,himg
mwrfits,mask_a,outfile
mwrfits,mask_b,outfile

end
