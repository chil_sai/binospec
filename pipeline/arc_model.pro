;arc line spectrum model generation

function arc_model,wl,FWHM,linetab_list=linetab_list,filter=filter,weight_tab=weight_tab

;wavelength scale vector in Angstrom
;FWHM - spectral resolution in px
;

wl_0=wl[0]
d_wl=wl[1]-wl[0]
Nx=N_elements(wl)

; reading the arc line table
if(n_elements(linetab_list) eq 0) then linetab_list=[getenv('BINO_PIPELINE_PATH')+'calib_Bino/linelist/linesAr_NIST.tab']
n_tab=n_elements(linetab_list)
if(n_elements(weight_tab) ne n_tab) then weight_tab=dblarr(n_tab)+1d
arc_model_spectrum=wl*0.0

for i=0,n_tab-1 do begin
    linetab=linetab_list[i]
    weight=weight_tab[i]

    N_tab=long(file_lines(linetab))
    table=read_asc(linetab)
    if(keyword_set(filter)) then begin
        table=filter_linelist(table,d_wl*FWHM)
    endif
    
    wl_line=float(transpose(table[0,*]))
    flux_line=float(transpose(table[2,*]))
    
    ; filtering the input line list
    N_lines=N_elements(wl_line)
    R=where(wl_line gt wl[0] and wl_line lt wl[Nx-1])
    wl_line=wl_line[R]
    flux_line=flux_line[R]
    N_lines=N_elements(wl_line)
    x=findgen(Nx)
    x_lin=interpol(x,wl,wl_line)
    
    sigma=FWHM/2.35
    for k=0,N_lines-1 do begin
       arc_model_spectrum=arc_model_spectrum+weight*flux_line[k]*exp(-((x-x_lin[k])/sigma)^2/2)
    endfor
endfor

return,arc_model_spectrum
end
    