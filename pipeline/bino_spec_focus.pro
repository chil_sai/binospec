pro bino_spec_focus,arc_f,nx=nx,ny=ny,psout=psout,dataout=dataout,verbose=verbose,ct=ct,clean=clean

    if(n_elements(ct) ne 1) then ct=39

    disp_name=!d.name
    focarr=spec_focus(arc_f,nx=nx,ny=ny,verbose=verbose,$
        xminarr=xminarr,xmaxarr=xmaxarr,yminarr=yminarr,ymaxarr=ymaxarr,clean=clean)

    im_a=mrdfits(arc_f,1,ha,/silent)
    med_flux_a=median(im_a)
    sig_a=robust_sigma(im_a)
    im_b=mrdfits(arc_f,2,hb,/silent)
    med_flux_b=median(im_b)
    sig_b=robust_sigma(im_b)
    xs=sxpar(ha,'NAXIS1')
    ys=sxpar(ha,'NAXIS2')

    med_a=median(focarr[*,*,0])
    med_b=median(focarr[*,*,1])
    mean_a=total(focarr[*,*,0])/n_elements(focarr[*,*,0])
    mean_b=total(focarr[*,*,1])/n_elements(focarr[*,*,1])
    min_a=min(focarr[*,*,0],/nan,max=max_a)
    min_b=min(focarr[*,*,1],/nan,max=max_b)

    ps=n_elements(psout)
    if(ps eq 1) then begin
        set_plot,'ps'
        device,filename=psout,/encaps,/landscape,xsize=26,ysize=19.5,/col
    endif else window,0,xs=1024,ys=768

    loadct,39    
    plot,[0,xs-1],[0,ys-1],/nodata,xs=1,ys=1,/norm,/dev,pos=[0.1,0.05,0.4,0.45],$
        title='median='+string(med_a,form='(f4.1)')+$
              ' ('+string(min_a,form='(f4.1)')+'/'+string(max_a,form='(f4.1)')+')'
    tv,bytscl(congrid(focarr[*,*,0],$
        !p.clip[2]-!p.clip[0]+1,!p.clip[3]-!p.clip[1]+1),$
        med_a*0.7,med_a*1.3),0.1,0.05,/data,/norm
    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.1,0.05,0.4,0.45]

    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.6,0.05,0.9,0.45],$
        title='median='+string(med_b,form='(f4.1)')+$
              ' ('+string(min_b,form='(f4.1)')+'/'+string(max_b,form='(f4.1)')+')'
    tv,bytscl(congrid(focarr[*,*,1],$
        !p.clip[2]-!p.clip[0]+1,!p.clip[3]-!p.clip[1]+1),$
        med_b*0.7,med_b*1.3),0.6,0.05,/data,/norm
    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.6,0.05,0.9,0.45]


    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.1,0.55,0.4,0.95]
    tv,bytscl(congrid(im_a,$
        !p.clip[2]-!p.clip[0]+1,!p.clip[3]-!p.clip[1]+1,/interp),$
        med_flux_a-sig_a,med_flux_a+20.0*sig_a),0.1,0.55,/data,/norm
    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.1,0.55,0.4,0.95]

    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.6,0.55,0.9,0.95]
    tv,bytscl(congrid(im_b,$
        !p.clip[2]-!p.clip[0]+1,!p.clip[3]-!p.clip[1]+1,/interp),$
        med_flux_b-sig_b,med_flux_b+20.0*sig_b),0.6,0.55,/data,/norm
    plot,[0,xs-1],[0,ys-1],/nodata,/noer,xs=1,ys=1,/norm,/dev,pos=[0.6,0.55,0.9,0.95]

    if(ps eq 1) then begin
        device,/close
        set_plot,disp_name
    endif

    if(n_elements(dataout) eq 1) then begin
        openw,u,dataout,/get_lun
        printf,u,'====================================='
        printf,u,'Side A:'
        printf,u,'median(FWHM) = '+string(med_a,format='(f5.2)')
        printf,u,'mean(FWHM)   = '+string(mean_a,format='(f5.2)')
        printf,u,'min(FWHM)    = '+string(min_a,format='(f5.2)')
        printf,u,'max(FWHM)    = '+string(max_a,format='(f5.2)')
        printf,u,'-------------------------------------'
        printf,u,'   X       Y    FWHM'
        for i=0,n_elements(focarr[*,*,0])-1 do $
            printf,u,((xminarr)[i]+(xmaxarr)[i])/2.0,((yminarr)[i]+(ymaxarr)[i])/2.0,(focarr[*,*,0])[i],format='(i6,2x,i6,2x,f5.2)'
        printf,u,'====================================='
        printf,u,'Side B:'
        printf,u,'median(FWHM) = '+string(med_b,format='(f5.2)')
        printf,u,'mean(FWHM)   = '+string(mean_b,format='(f5.2)')
        printf,u,'min(FWHM)    = '+string(min_b,format='(f5.2)')
        printf,u,'max(FWHM)    = '+string(max_b,format='(f5.2)')
        printf,u,'-------------------------------------'
        printf,u,'   X       Y    FWHM'
        for i=0,n_elements(focarr[*,*,1])-1 do $
            printf,u,((xminarr)[i]+(xmaxarr)[i])/2.0,((yminarr)[i]+(ymaxarr)[i])/2.0,(focarr[*,*,1])[i],format='(i6,2x,i6,2x,f5.2)'
        close,u
        free_lun,u
    endif

end
