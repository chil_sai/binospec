function bino_linearisation_params,grating,tilt,filter,wl0=wl0,dwl=dwl,wl_min=wl_min,wl_max=wl_max

    if(n_params() eq 2) then filter=''
    if(grating eq '270') then begin
        dwl=1.29
        wl0=3400.0+(tilt-32.5)*1170.0-768*dwl
        wl0=round(wl0/20d)*20d
        npix=4096+1536
        if(filter eq 'redblock') then begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endif else begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endelse
    endif

    if(grating eq '600') then begin
        dwl=0.62
        wl0=3870.0+(tilt-37.0)*526.0-768*dwl
        wl0=round(wl0/10d)*10d
        npix=4096+1536
        if(filter eq 'redblock') then begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endif else begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endelse
    endif

    if(grating eq '1000') then begin
        dwl=0.37
        wl0=3630.0+(tilt-41.5)*295.0-768*dwl
        wl0=round(wl0/5d)*5d
        npix=4096+1536
        if(filter eq 'redblock') then begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endif else begin
            wl_min=wl0
            wl_max=wl0+dwl*npix
        endelse
    endif

    return,{grating:grating, filter:filter, tilt:tilt, wl0:wl0, dwl:dwl, npix:npix, wl_min:wl_min, wl_max:wl_max}

end
