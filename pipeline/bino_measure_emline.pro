function slit_measure_emline,slit_lin,wl,wl_line,win_line,nterms=nterms,status=status,$
    slit_poly=slit_poly,auto_slit_poly=auto_slit_poly
    if(n_elements(nterms) ne 1) then nterms=4
    if(n_elements(slit_poly) ne 1) then slit_poly=-1
    status=1
    s_im=size(slit_lin)
    ny=(s_im[0] eq 2)? s_im[2] : 1
    res_arr=dblarr(nterms,ny)+!values.d_nan

    gwl=where(wl ge wl_line-win_line and wl le wl_line+win_line,cgwl)
    if(cgwl lt 10) then begin
        status=2
        return,res_arr
    endif
    gline=0
    for y=0,ny-1 do begin
        gpix=where(finite(slit_lin[gwl,y]) eq 1, cgpix)
        if(cgpix lt 8) then continue
        pfit=mpfitpeak(wl[gwl[gpix]],slit_lin[gwl[gpix],y],a,nterms=nterms)
        res_arr[*,y]=a
        gline++
    endfor
    if(gline gt 0) then status=0
    gline_idx=where(finite(res_arr[0,*]) eq 1, cgline_idx)
    if(keyword_set(auto_slit_poly)) then begin
        if(gline lt 50) then slit_poly=0
        if(gline ge 50 and gline lt 150) then slit_poly=1
        if(gline ge 150 and gline lt 400) then slit_poly=2
        if(gline ge 400 and gline lt 1500) then slit_poly=3
        if(gline ge 1500 and gline lt 3000) then slit_poly=4
        if(gline ge 3000) then slit_poly=5
    endif
    if(slit_poly eq 0 and cgline_idx gt 0) then res_arr[*,gline_idx]=(median(res_arr,dim=2)) # (dblarr(gline)+1d)
    if(slit_poly gt 0 and cgline_idx gt 0) then begin
        if(gline gt slit_poly+2) then begin
            for i=0,nterms-1 do begin
                p_k=robust_poly_fit(double(gline_idx),res_arr[i,gline_idx],slit_poly)
;                res_arr[i,gline_idx]=poly(double(gline_idx),p_k)
                res_arr[i,*]=poly(dindgen(ny),p_k)
            endfor
        endif
    endif

    return,res_arr
end

function bino_measure_emline,logfile,image_type,wl_line,win_line=win_line,$
    slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,force_image_type_meta=force_image_type_meta,nterms=nterms

    if(n_params() eq 2) then begin
        wl_line=image_type
        image_type='obj'
    endif

    if(n_elements(slit_poly) ne 1) then slit_poly=-1

    if(n_elements(win_line) ne 1) then win_line=1.0 ;;; 1nm=10A
    wdir=def_wdir(logfile)
    
    image_type_meta=(image_type eq 'obj-sky' or $
                     image_type eq 'obj_abs' or $
                     image_type eq 'obj_abs_err' or $
                     image_type eq 'obj_counts' or $
                     image_type eq 'obj_counts_err' or $
                     ~file_test(wdir+image_type+'_dark.fits'))? 'obj' : image_type
    if(n_elements(force_image_type_meta) eq 1) then image_type_meta=force_image_type_meta
    h_a0=headfits(wdir+image_type_meta+'_dark.fits',ext=1)
    h_b0=headfits(wdir+image_type_meta+'_dark.fits',ext=2)

    mask_a=read_mask_bino_ms(wdir+image_type_meta+'_dark.fits',side='A')
    mask_b=read_mask_bino_ms(wdir+image_type_meta+'_dark.fits',side='B')

    good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 

    if n_slits_a gt 0 then h_slit_a=headfits(wdir+image_type+'_slits_lin.fits',ext=1) else h_slit_a=''
    if n_slits_b gt 0 then h_slit_b=headfits(wdir+image_type+'_slits_lin.fits',ext=1+n_slits_a) else h_slit_b=''
    ny_a=sxpar(h_a0,'NAXIS2')
    ny_b=sxpar(h_b0,'NAXIS2')
    disper_a=mrdfits(wdir+'disper_table.fits',1,/silent)
    disper_b=mrdfits(wdir+'disper_table.fits',2,/silent)

    line_str={wl_line:wl_line,$
        wl:!values.d_nan,flux:!values.d_nan,sigma:!values.d_nan,bgr:!values.d_nan,$
        x_mask:!values.f_nan,y_mask:!values.f_nan,slit:-1l}
    line_par_a=replicate(line_str,ny_a)
    line_par_b=replicate(line_str,ny_b)

  if n_slits_a gt 0 then begin
    for i=0,n_slits_a-1 do begin
        print,'Computing line flux in slit: ',i+1
        slit_lin=mrdfits(wdir+image_type+'_slits_lin.fits',i+1,h,/silent)
        ny_cur=sxpar(h,'NAXIS2')
        y_off=sxpar(h,'YOFFSET')
        parse_spechdr,h,wl=wl
        val_arr=slit_measure_emline(slit_lin,wl,wl_line,win_line,status=status,slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,nterms=nterms)
        if(status ne 0) then continue
        gline=where(finite(val_arr[0,*]) eq 1,cgline)
        yidx=y_off+lindgen(ny_cur)
        line_par_a[gline+y_off].wl_line=wl_line
        line_par_a[yidx].wl=transpose(val_arr[1,*])
        line_par_a[yidx].flux=transpose(val_arr[0,*])
        line_par_a[yidx].sigma=transpose(val_arr[2,*])
        line_par_a[yidx].bgr=transpose(val_arr[3,*])
        line_par_a[yidx].x_mask=disper_a[yidx].x_mask
        line_par_a[yidx].y_mask=disper_a[yidx].y_mask
        line_par_a[yidx].slit=disper_a[yidx].slit
    endfor
  endif
  if n_slits_b gt 0 then begin   
    for i=0,n_slits_b-1 do begin
        print,'Computing line flux in slit: ',i+1+n_slits_a
        slit_lin=mrdfits(wdir+image_type+'_slits_lin.fits',i+1+n_slits_a,h,/silent)
        ny_cur=sxpar(h,'NAXIS2')
        y_off=sxpar(h,'YOFFSET')
        parse_spechdr,h,wl=wl
        val_arr=slit_measure_emline(slit_lin,wl,wl_line,win_line,status=status,slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,nterms=nterms)
        if(status ne 0) then continue
        gline=where(finite(val_arr[0,*]) eq 1,cgline)
        yidx=y_off+lindgen(ny_cur)
        line_par_b[gline+y_off].wl_line=wl_line
        line_par_b[yidx].wl=transpose(val_arr[1,*])
        line_par_b[yidx].flux=transpose(val_arr[0,*])
        line_par_b[yidx].sigma=transpose(val_arr[2,*])
        line_par_b[yidx].bgr=transpose(val_arr[3,*])
        line_par_b[yidx].x_mask=disper_b[yidx].x_mask
        line_par_b[yidx].y_mask=disper_b[yidx].y_mask
        line_par_b[yidx].slit=disper_b[yidx].slit
    endfor
  endif
    return,{slits_a:line_par_a,slits_b:line_par_b}

end
