pro bino_fluxcal,logfile,image_type=image_type,nosky_image_type=nosky_image_type,counts=counts,n_exp=n_exp,double=double,noflat=noflat,noerror=noerror

if(n_elements(n_exp) ne 1) then n_exp=1
if(n_elements(image_type) ne 1) then image_type='obj-sky'
if(n_elements(nosky_image_type) ne 1) then nosky_image_type='obj'
wdir=def_wdir(logfile)
mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 

nslit=n_slits_a+n_slits_b

h_a=headfits(wdir+'obj_dark.fits',ext=1)
filter=strcompress(sxpar(h_a,'FILTER'),/remove_all)
grating_str=strcompress(strmid(sxpar(h_a,'DISPERS1'),1),/remove_all)

filt_suff=((filter eq 'blueblock' or filter eq 'LP3500') and grating_str eq '1000')? '_lp3500' : ''

flat_prof_a=mrdfits(getenv('BINO_PIPELINE_PATH')+'calib_Bino/throughput/nflat_'+grating_str+'gpm'+filt_suff+'.fits',1,/silent)
flat_prof_b=flat_prof_a

thr_a=mrdfits(getenv('BINO_PIPELINE_PATH')+'calib_Bino/throughput/throughput_'+grating_str+'.fits',1,/silent)
thr_b=thr_a

inpfile=wdir+image_type+'_slits_lin.fits'
noskyfile=wdir+nosky_image_type+'_slits_lin.fits'
suff=(keyword_set(counts))? '_counts' : '_abs'
outfile=wdir+nosky_image_type+suff+'_slits_lin.fits'
outfile_err=wdir+nosky_image_type+suff+'_err_slits_lin.fits'

hpri=headfits(inpfile)
writefits,outfile,0,hpri
if(~keyword_set(noerror)) then writefits,outfile_err,0,hpri

rdnoise=4.0
message,/inf,'Performing flux calibration in '+((keyword_set(counts))?'counts': 'absolute units')
;scan slits for min/max wavelengths and then peak flux
maxvals_a=!null
maxvals_b=!null
for i=0,nslit-1 do begin
    himg=headfits(inpfile,ext=i+1)
    side=strcompress(sxpar(himg,'SIDE'),/remove)
    parse_spechdr,himg,wl=wl
    if side eq 'A' then begin
         wave_overlap_a=where((flat_prof_a.wave lt max(wl)) and (flat_prof_a.wave ge min(wl)))
         if maxvals_a eq !null then  maxvals_a=max(flat_prof_a.flux[wave_overlap_a]) else maxvals_a=[maxvals_a,max(flat_prof_a.flux[wave_overlap_a])]
      endif else begin
         wave_overlap_b=where((flat_prof_b.wave lt max(wl)) and (flat_prof_b.wave ge min(wl)))
         if maxvals_b eq !null then  maxvals_b=max(flat_prof_b.flux[wave_overlap_b]) else maxvals_b=[maxvals_b,max(flat_prof_b.flux[wave_overlap_b])]
      endelse    
endfor
    ;normalize flat_prof
    if maxvals_a ne !null then if n_elements(maxvals_a) gt 1 then flat_prof_a.flux=flat_prof_a.flux/median(maxvals_a) else  flat_prof_a.flux=flat_prof_a.flux/maxvals_a
    if maxvals_b ne !null then if n_elements(maxvals_b) gt 1 then flat_prof_b.flux=flat_prof_b.flux/median(maxvals_b) else flat_prof_b.flux=flat_prof_b.flux/maxvals_b

for i=0,nslit-1 do begin
    img=mrdfits(inpfile,i+1,himg,/silent)*n_exp
    if(~keyword_set(noerror)) then imgnosky=mrdfits(noskyfile,i+1,/silent)*n_exp
    side=strcompress(sxpar(himg,'SIDE'),/remove)
    sxaddpar,himg,'BUNIT','1E-17 erg/cm^2/s/Angstrom'

    parse_spechdr,himg,wl=wl
    flat_prof=(side eq 'A')? interpol(flat_prof_a.flux,flat_prof_a.wave,wl) : interpol(flat_prof_b.flux,flat_prof_b.wave,wl)
    thr_prof=(side eq 'A')? interpol(thr_a.c_flux_erg,thr_a.wave/10d,wl) : interpol(thr_b.c_flux_erg,thr_b.wave/10d,wl)
    thr_prof=thr_prof*1e18
    ny=sxpar(himg,'NAXIS2')
    exptime=sxpar(himg,'EXPTIME')
    if(exptime eq 0.) then exptime=1.0
    if(keyword_set(counts)) then thr_prof[*]=exptime
    if keyword_set(noflat) then flat_prof[*]=1.0
    flat_img=(flat_prof) # (dblarr(ny)+1d)
    thr_img=(thr_prof) # (dblarr(ny)+1d)

    img_out=img*flat_img*thr_img/exptime
    if(~keyword_set(noerror)) then errimg_out=sqrt((imgnosky*flat_img+rdnoise^2.0)>1)*thr_img/exptime
    if(not keyword_set(double)) then begin
        img_out=float(img_out)
        if(~keyword_set(noerror)) then errimg_out=float(errimg_out)
    endif
    mwrfits,img_out,outfile,himg,/silent
    if(~keyword_set(noerror)) then mwrfits,errimg_out,outfile_err,himg,/silent
endfor

end
