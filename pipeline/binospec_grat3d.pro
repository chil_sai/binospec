pro grat3d

degtorad=1.74532925e-2
colcamd=45.0

;collimator coefficients
a=[0.052253986,-8.0058549e-007,3.6370679e-008,-1.1136857e-007,-4.6633669e-012,5.9133806e-011]
;camera coefficients
b=[7.0444380,0.00037968341,-0.00066758479,0.016357838,0.026785597,-0.075174838,0.24346297]

;read, prompt='Enter mask X:',mx
;read, prompt='Enter mask Y: ',my
;read, prompt='Enter wavelength in microns: ',w
;read, prompt='Enter grating gpm: ',gpm
;read, prompt='Enter angle of incidence (deg): ',inangd

mx=0.
my=0.
w=0.8257
gpm=600.
ord=1
inangd=35.15

camangd=colcamd-inangd
camangr=camangd*degtorad
inangr=inangd*degtorad
sig=1000./gpm

r=sqrt(mx^2+my^2)

colangd=a[0]*r+a[1]*r^2+a[2]*r^3+a[3]*(r/(w-0.38)) $
  + a[4]*(r^2/(w-0.38)^2) + a[5]*(r^3/(w-0.38))
colangr=degtorad*colangd
  
print,colangd,colangr

;calculate incident direction cosines

;we know ry, remember that mask y is z in these coordinates
;we know that rx^2+ry^2+rz^2=1, so rx^2+rz^2=1-ry^2
;ssumsq is sqrt(rx^2+rz^2)

ry=cos(colangr)
ssumsq=sqrt(1-ry^2)

ratio=0.
if my ne 0. then ratio=mx/my

rzcom=0.
if my ne 0. then rzcom=sqrt(ratio^2+1.)

rz=0.
if my ne 0. then rz=ssumsq/rzcom

rx=ssumsq
if my ne 0. then rx=ratio*rz

print,rx,ry,rz

alp= rx*cos(inangr)+ry*sin(inangr)
bet=-rx*sin(inangr)+ry*cos(inangr)
gam=rz

alpp=((ord*w)/sig)-alp
gamp=-rz
betp=sqrt(1-alpp^2-gamp^2)

alpc= alpp*cos(camangr)+betp*sin(camangr)
betc=-alpp*sin(camangr)+betp*cos(camangr)
gamc=gamp

print,alp,bet,gam
print,alpp,betp,gamp
print,alpc,betc,gamc

totang=acos(betc)/degtorad
ta=totang
totr=b[0]*ta + b[1]*ta^2 + b[2]*ta^3 + b[3]*ta*(w-.7386)+ b[4]*ta*(w-.7386)^2 $
  + b[5]*ta*(w-.7386)^3 + b[6]*ta*(w-.7386)^4

print,ta,totr

;dx and dy are mm in detector coordinates

dy=0.
if gamc ne 0. then dy=-totr/sqrt(1+(alpc/gamc)^2)
dx=-totr
if gamc ne 0. then dx=-(alpc/gamc)*dy

print,dx,dy

end