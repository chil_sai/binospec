pro bino_linearisation,logfile,obstype,adj=adj,$
    subskybox=subskybox,subskytarget=subskytarget,$
    telluric=telluric,masklowflat=masklowflat,flatthr=flatthr,$
    usebadpixmask=usebadpixmask,maskbadpixels=maskbadpixels,$
    barycorr=barycorr,baryv=baryv,skylinecorr=skylinecorr,double=double,$
    debug=debug,noflat=noflat

if(n_params() eq 1) then obstype=['obj']

grating=def_grating(logfile,filter=filter)
wdir=def_wdir(logfile)
log=readlog(logfile)
bright=sxpar(log,'BRIGHT')
if(keyword_set(subskytarget)) then bright=1
f_disp=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
wl_data_a=mrdfits(wdir+f_disp,1,/silent)
wl_data_b=mrdfits(wdir+f_disp,2,/silent)

Ny_a=n_elements(wl_data_a)
Ny_b=n_elements(wl_data_b)

dist_map_a=mrdfits(wdir+'dist_map_A.fits',1)
dist_map_b=mrdfits(wdir+'dist_map_B.fits',1)
if keyword_set(noflat) then begin
  mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
endif else begin
  mask_a=read_mask_bino_ms(wdir+'flat_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'flat_dark.fits',side='B')
endelse
good_sides=[1,1]
n_slits_a=n_elements(mask_a)
if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
  good_sides[0]=0
  n_slits_a=0
endif 
n_slits_b=n_elements(mask_b)
if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
  good_sides[1]=0
  n_slits_b=0
endif 

if(keyword_set(skylinecorr)) then begin
    for sideext=1,2 do begin
        if good_sides[sideext-1] eq 0 then continue
        disp_rel=(sideext eq 1)? wl_data_a : wl_data_b
        skyline_files=file_search(wdir+'skyline_flux_data_*.fits')
        if(skyline_files[0] ne '') then begin
            fit_skyline=0
            sl_flux=0d
            for f_s=0,n_elements(skyline_files)-1 do begin
                skyline_flux_data_tmp=mrdfits(skyline_files[f_s],sideext,/silent)
                sl_flux_cur=median(skyline_flux_data_tmp.flux)
                if(sl_flux_cur gt sl_flux) then begin
                    skyline_flux_data=skyline_flux_data_tmp
                    g_skyline=where((finite(skyline_flux_data.wl) eq 1) and (finite(skyline_flux_data.x_mask) eq 1),cg_skyline)
                    if((cg_skyline gt 10) and keyword_set(fit_skyline)) then begin
                        t_wl=robust_poly_fit(double(g_skyline),skyline_flux_data[g_skyline].wl,5)
                        skyline_flux_data[g_skyline].wl=poly(double(g_skyline),t_wl)
                    endif
                    mean_wl_offset=median(skyline_flux_data.wl-skyline_flux_data.wl_line)*10d
                    sl_flux=sl_flux_cur
                 endif else cg_skyline=0
            endfor
            for s=0,cg_skyline-1 do begin
                disp_rel[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line)*10d -mean_wl_offset/double(cg_skyline)
            endfor
            if(sideext eq 1) then wl_data_a=disp_rel else wl_data_b=disp_rel
        endif
    endfor
endif

h_pri=headfits(wdir+obstype[0]+'_slits.fits')
date_obs=sxpar(h_pri,'DATE-OBS')
mjd=sxpar(h_pri,'MJD')
exptime=sxpar(h_pri,'EXPTIME')
if(keyword_set(barycorr)) then begin
    jd=mjd+2400000.5d
;    ra_mean=total([mask_a.ra,mask_b.ra])/(n_slits_a+n_slits_b)
;    dec_mean=total([mask_a.dec,mask_b.dec])/(n_slits_a+n_slits_b)
    ra_field=sxpar(h_pri,'RA')*15d/!radeg
    dec_field=sxpar(h_pri,'DEC')/!radeg
    baryvel,jd,2000.0,vh,vb
    vel=vb[0]*cos(dec_field)*cos(ra_field)+vb[1]*cos(dec_field)*sin(ra_field)+vb[2]*sin(dec_field)
    if(n_elements(baryv) ne 1) then baryv=vel
endif else baryv=0d

if keyword_set(noflat) then h_a=headfits(wdir+'obj_dark.fits',ext=1) else h_a=headfits(wdir+'flat_dark.fits',ext=1)
tilt=sxpar(h_a,'TAPEPOS1')

l_par = bino_linearisation_params(grating,tilt,filter)
wl0=l_par.wl0
dwl=l_par.dwl
npix=l_par.npix
wl_min=l_par.wl_min
wl_max=l_par.wl_max

ccdscl=0.24436 ;;0.25 ;;; 0.2 arcsec per pix -- hardcoded CCD scale


for k=0,n_elements(obstype)-1 do begin
    filename=wdir+obstype[k]+'_slits.fits'
    fileout=wdir+obstype[k]+'_slits_lin.fits'
    hpri=headfits(filename)
    writefits,fileout,0,hpri

    val=sxpar(log,'DITHPOS',count=cntval)
    dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0
    n_dith=n_elements(dithpos)
    diffmode = (obstype[k] eq 'obj_diff-sky' or obstype[k] eq 'obj_diff')? 1 : 0
    if (diffmode eq 1) then begin
        val2=sxpar(log,'DITHPOS2',count=cntval2)
        dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
        if(cntval2 eq 1) then begin
            dithpos=[dithpos,dithpos2]
            n_dith=n_dith+n_elements(dithpos2)
        endif
    endif

    if(keyword_set(subskybox) or keyword_set(subskytarget)) then begin
        e_apw=10.0
        c_mask = round(max(abs(dithpos))/ccdscl + e_apw*3)
        n_mask = 2*c_mask+1

        sky_mask = bytarr(n_mask)
        for i=0,n_dith-1 do begin
            d = dblarr(n_mask)
            d[c_mask-floor(e_apw/2.0):c_mask+floor(e_apw/2.0)]=1.0
            d0=d
            d=shift_s(d0,dithpos[i]/ccdscl)
            sky_mask[*] = sky_mask[*] + d
        endfor
    endif

    for s=0,n_slits_a+n_slits_b-1 do begin
        maskentry=((s lt n_slits_a) and (n_slits_a gt 0))? mask_a[s] : mask_b[s-n_slits_a]
        obs=mrdfits(filename,s+1,h,/silent)
        y_off=sxpar(h,'YOFFSET')
        side=strmid(sxpar(h,'SIDE'),0,1)
        print,'Resampling slit: ',s+1,' SIDE:',side
        Nx_cur=sxpar(h,'NAXIS1')
        Ny_cur=sxpar(h,'NAXIS2')
        tilted_slit=sxpar(h,'TILTEDSL')
        prof_med = median(obs,dim=1)

        if(keyword_set(usebadpixmask) and (obstype[k] eq 'obj_diff-sky')) then begin
            bpmask_obs=mrdfits(wdir+obstype[k]+'_bpmask_slits.fits',s+1,/silent)
            bpix=where(bpmask_obs ne 0,cbpix)
            if(cbpix gt 0) then begin
                obs[bpix]=!values.f_nan
                if(not (keyword_set(maskbadpixels))) then begin
                    obsmed3=djs_median(obs,width=3,boundary='reflect')
                    obs[bpix]=obsmed3[bpix]
                    bpix2nd=where(finite(obs) ne 1,cbpix2nd)
                    if(cbpix2nd gt 0) then begin
                        obsmed5=djs_median(obs,width=5,boundary='reflect')
                        obs[bpix2nd]=obsmed5[bpix2nd]
                    endif
                endif
            endif
        endif

        Ny=(side eq 'A')? Ny_a : Ny_b
        dist_map=(side eq 'A')? dist_map_a : dist_map_b
        wl_data=(side eq 'A')? wl_data_a : wl_data_b
        inverse=0 ;; (side eq 'A')?1:0
        obs_n=bino_rectify_slit(obs,dist_map,y_off,side=side,edge=~tilted_slit, inverse=inverse)
        obs=obs_n
    
        obs_lin=fltarr(npix,Ny_cur)
        wl_sc_lin=wl0+findgen(npix)*dwl
        if(baryv ne 0d) then wl_sc_lin/=(1d +baryv/299792.458d)
        flag_arr=bytarr(Ny_cur)
    
        for i=0,Ny_cur-1 do begin
            y_corr=y_off+i
            if(y_corr lt 0 or y_corr gt Ny-1) then continue
            wl_sc_cur=poly(findgen(Nx_cur),wl_data[y_corr].wl_sol)
            obs_lin[*,i]=interpol(obs[*,i],wl_sc_cur,wl_sc_lin)
            goodpix=where(finite(obs[*,i]) eq 1, cgoodpix, ncompl=cbad)
            if(cgoodpix gt 0) then begin
                mingoodwl=min(wl_sc_cur[goodpix],minidx,max=maxgoodwl,subscript_max=maxidx)
    ;            print,'slit,minwl,maxwl=',s+1,wl_sc_cur[minidx],wl_sc_cur[maxidx]
                badwl=where((wl_sc_lin lt mingoodwl) or $
                            (wl_sc_lin gt maxgoodwl), cbadwl)
                if(cbadwl gt 1) then obs_lin[badwl,i]=!values.f_nan
            endif
        endfor
    
        wl=wl_sc_lin ;/10d

        if(grating eq '270') then begin
            wlmin_reg=wl_min+0.3*(wl_max-wl_min)
            wlmax_reg=wl_min+0.7*(wl_max-wl_min)
        endif
        if(grating eq '600') then begin
            wlmin_reg=wl_min+0.3*(wl_max-wl_min)
            wlmax_reg=wl_min+0.7*(wl_max-wl_min)
        endif
        if(grating eq '1000') then begin
            wlmin_reg=wl_min+0.3*(wl_max-wl_min)
            wlmax_reg=wl_min+0.7*(wl_max-wl_min)
        endif

        good_reg=where((wl ge wlmin_reg) and (wl le wlmax_reg))
        prof_spec = median(obs_lin[good_reg,*],dim=1)
        gprof_spec = where(finite(prof_spec) eq 1, cgprof_spec)
        xb=findgen(Ny_cur)-(Ny_cur-1)/2.0

        if(n_elements(telluric) gt 0) then begin
            ;;;;;;;;;;;;;;; telluric code here
            detected=0
            if(cgprof_spec gt 7) then begin
                fit_prof_spec=gaussfit(xb[gprof_spec],prof_spec[gprof_spec],nterms=3,cgauss,chisq=c2)
                if((cgauss[0] gt 0) and (cgauss[2] gt 0.05/ccdscl) and (cgauss[2] lt 1.0/ccdscl)) then begin ;;; 0.7/ccdscl is too low for bad seeing
                    detected = 1 ;;;;; proper condition must come here
                    print,'Object detected in slit'+string(s+1)
                    dith_offset=cgauss[1]*ccdscl
                    sxaddpar,h,'OBJPOS',dith_offset,' object position estimated from the spectrum'
                    sxaddpar,h,'OBJFWHM',cgauss[2]*ccdscl*2.355,' trace Gaussian FWHM estimated from the spectrum'
                    print,'slit,ObjPos,FWHM=',s+1,dith_offset,cgauss[2]*ccdscl*2.355,'  chi2=',c2
                    if(keyword_set(debug)) then begin
                        plot,xb*ccdscl,prof_spec,xs=1,ys=1,psym=-4,title='Slit #'+string(s+1,format='(i2)'),xtit='position, arcsec'
                        oplot,xb[gprof_spec]*ccdscl,fit_prof_spec,linest=2,col=128
;                        aaa=''
;                        read,aaa
                    endif
                endif
            endif

            if(detected eq 1) then begin
                sky_mask = bytarr(n_mask)
                for i=0,n_dith-1 do begin
                    d = dblarr(n_mask)
                    d[c_mask-floor(e_apw/2.0):c_mask+floor(e_apw/2.0)]=1.0
                    d0=d
                    d=shift_s(d0,dith_offset/ccdscl)
                    sky_mask[*] = sky_mask[*] + d
                endfor
            endif
        endif

        if(keyword_set(subskybox) or keyword_set(subskytarget)) then begin
            sky_cur=0.0
            mask_cur=bytarr(ny_cur)

            if(diffmode eq 1 and bright eq 1 and cgprof_spec ge 6) then begin
                prof_spec_pos=(prof_spec > 0.0)
                max_prof = max(smooth(prof_spec_pos,3,/nan), max_prof_idx)
                fit_prof_spec=gaussfit(xb[gprof_spec],prof_spec_pos[gprof_spec],nterms=3,cgauss)
		res_std=robust_sigma(prof_spec_pos[gprof_spec]-fit_prof_spec)
		if((cgauss[0] gt 20.0*res_std) and $
		   (cgauss[2] gt 0.1/ccdscl) and $
		   (abs(cgauss[1]) lt Ny_cur/4.0)) then begin ;;;;; detected
		    hwidth=((round(cgauss[2])) > 1)
		    dshift = round((dithpos[0]-dithpos[1])/ccdscl)

		    idx1min=0 > (cgauss[1]+(Ny_cur-1)/2.0-hwidth) < (Ny_cur-1)
		    idx1max=0 > (cgauss[1]+(Ny_cur-1)/2.0+hwidth) < (Ny_cur-1)
		    mask_cur[idx1min:idx1max]=1

		    idx2min=0 > (cgauss[1]+(Ny_cur-1)/2.0-hwidth-dshift) < (Ny_cur-1)
		    idx2max=0 > (cgauss[1]+(Ny_cur-1)/2.0+hwidth-dshift) < (Ny_cur-1)
		    mask_cur[idx2min:idx2max]=1
		    
                    goodm = where((mask_cur eq 0) and (finite(prof_spec) eq 1),cgoodm)
                    if(cgoodm gt 2) then sky_cur=median(obs_lin[*,goodm],dim=2)
		endif else begin ;;;; undetected
		    goodm = where((mask_cur eq 0) and (finite(prof_spec) eq 1),cgoodm)
                    if(cgoodm gt 2) then sky_cur=median(obs_lin[*,goodm],dim=2)
		endelse
            endif else begin
                c_m_c=(ny_cur-1)/2
                if(ny_cur ge n_mask) then $
                    mask_cur[c_m_c-c_mask:c_m_c+c_mask]=sky_mask $
                else $
                    mask_cur[*]=sky_mask[c_mask-c_m_c:c_mask+c_m_c]
                goodm = where(mask_cur eq 0, cgoodm)
                
                sky_cur=(cgoodm gt 1)? median(obs_lin[*,goodm],dim=2) : total(obs_lin,2,/nan)
            endelse

            if((keyword_set(subskybox) and strcompress(maskentry.type,/remove) eq 'BOX') or $
               (keyword_set(subskytarget) and strcompress(maskentry.type,/remove) eq 'TARGET')) then begin
                for i=0,Ny_cur-1 do obs_lin[*,i]=obs_lin[*,i]-sky_cur
                sxaddhist,'Sky additionally subtracted from linearised spectra',h
            endif
        endif

        if(maskentry.type eq 'BOX') then begin
            prof_box = median(obs_lin,dim=1)
            gprof_box = where(finite(prof_box) eq 1, cgprof_box)
            if(cgprof_box gt 7) then begin
                xb=findgen(Ny_cur)-(Ny_cur-1)/2.0
                fit_prof_box=gaussfit(xb[gprof_box],prof_box[gprof_box],nterms=3,cgauss,chisq=c2)
                dith_offset=cgauss[1]*ccdscl
                sxaddpar,h,'DITHCOMP',dith_offset,' dithering position estimated from alignbox'
                sxaddpar,h,'TRCFWHM',cgauss[2]*ccdscl*2.355,' trace Gaussian FWHM estimated from alignbox'
                print,'slit,DithPos,FWHM=',s+1,dith_offset,cgauss[2]*ccdscl,'  chi2=',c2
;                plot,xb,prof_box,xs=1
;                oplot,xb[gprof_box],fit_prof_box,psym=-4,linest=2
            endif
        endif
            
        sxaddpar,h,'CTYPE1','AWAV' ;;'WAVE'
        sxaddpar,h,'CUNIT1','nm'
        sxaddpar,h,'CRPIX1',1.0
        sxaddpar,h,'CRVAL1',wl0/10d
        sxaddpar,h,'CDELT1',dwl/10d
        sxaddpar,h,'CD1_1',dwl/10d
        sxaddpar,h,'CTYPE2',''
        sxaddpar,h,'CUNIT2',''
        sxaddpar,h,'CRPIX2',1.0
        sxaddpar,h,'CRVAL2',1.0
        sxaddpar,h,'CDELT2',1.0
        sxaddpar,h,'CD2_2',1.0
        sxaddpar,h,'DATE-OBS',date_obs
        sxaddpar,h,'MJD',mjd
        sxaddpar,h,'EXPTIME',exptime
        sxaddpar,h,'SLITID',maskentry.slit,' slit id'
        sxaddpar,h,'SLITRA',maskentry.ra,' ra object coordinate'
        sxaddpar,h,'SLITDEC',maskentry.dec,' dec object coordinate'
        sxaddpar,h,'SLITX',maskentry.x,' x slit coordinate'
        sxaddpar,h,'SLITY',maskentry.y,' y slit coordinate'
        sxaddpar,h,'SLITTARG',maskentry.target,' target'
        sxaddpar,h,'SLITOBJ',maskentry.object,' object'
        sxaddpar,h,'SLITTYPE',maskentry.type,' type'
;        sxaddpar,h,'SLITHEIG',maskentry.height,' slit height'
        sxaddpar,h,'SLITHEIG',maskentry.bbox[5]-maskentry.bbox[1],' slit height'
        sxaddpar,h,'SLITWIDT',maskentry.width,' slit width'
        sxaddpar,h,'SLITOFFS',maskentry.offset,' slit offset'
        sxaddpar,h,'SLITTHET',maskentry.theta,' slit PA (theta)'
        sxaddpar,h,'MASKID',maskentry.mask_id,' Mask ID from the database'
        sxaddpar,h,'MASKNAME',maskentry.mask_name,' Mask name from the database'
        sxaddpar,h,'MASKRA',maskentry.mask_ra,' RA of the field center'
        sxaddpar,h,'MASKDEC',maskentry.mask_dec,' Dec of the field center'
        sxaddpar,h,'MASKPA',maskentry.mask_pa,' PA of the mask'
;        sxaddpar,h,'NAXIS',3
;        sxaddpar,h,'NAXIS3',1
;        sxaddpar,h,'CTYPE2','RA---ZPN'
;        sxaddpar,h,'CTYPE3','DEC--ZPN'
;        sxaddpar,h,'CRVAL2',maskentry.ra
;        sxaddpar,h,'CRVAL3',maskentry.dec
;        sxaddpar,h,'CDELT2',ccdscl/3600d
;        sxaddpar,h,'CDELT3',1
;        sxaddpar,h,'CRPIX2',1+(Ny_cur-1)/2.0+dithpos[0]/ccdscl
;        sxaddpar,h,'CRPIX3',1
;        sxaddpar,h,'LONPOLE',90.0-double(sxpar(hpri,'ROTANGLE'))

        sxaddpar,h,'RA',maskentry.ra,' ra object coordinate'
        sxaddpar,h,'DEC',maskentry.dec,' dec object coordinate'
        sxaddpar,h,'EQUINOX',2000.0
        sxaddpar,h,'EPOCH',2000.0
        sxaddpar,h,'RADECSYS','FK5',' Coordinate System'
        sxaddpar,h,'HELIO_RV',baryv,' km/s barycentric correction applied to wavelengths'

        if(keyword_set(masklowflat)) then begin
            nflat=mrdfits(wdir+'flatn_slits_lin.fits',s+1,/silent)
            nflat_prof=median(nflat,dim=2)
            nflat=nflat/(nflat_prof # (dblarr(Ny_cur)+1d))
            if(n_elements(flatthr) ne 1) then flatthr=0.18
            lowflat=where(nflat lt flatthr or finite(nflat) ne 1,clowflat)
            if(clowflat gt 0) then obs_lin[lowflat]=!values.f_nan
        endif

        if(not keyword_set(double)) then obs_lin=float(obs_lin)
        mwrfits,obs_lin,fileout,h,/silent
    endfor
endfor

end
