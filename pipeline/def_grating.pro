function def_grating,logfile,filter=filter
;+
; NAME:
;	def_grating
; PURPOSE:
;	Getting grating ID from logfile
; DESCRIPTION: 
;	
; CALLING SEQUENCE:
;	Result=def_grating(logfile)
;
; CATEGORY:
;	MMIRS pipeline
;
; INPUTS:
;	LOGFILE = file name of the pipeline task file (FITS-header)
;	
;
; OUTPUTS:
;	Result = string scalar giving the grating ID
;		
; OPTIONAL OUTPUT:
;	no
;
; OPTIONAL INPUT KEYWORDS:
;	no	
;
; RESTRICTIONS:
;	no
;
; NOTES:
;	no
;
; PROCEDURES USED:
;	SXPAR,READLOG
;
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, CfA, Jan/2012
;-

on_error,2

log=readlog(logfile)

result=strcompress(sxpar(log,'GRATING',count=nmatches),/remove_all)
if(nmatches ne 1) then result='270' ;;;; defaults to HK
filter=strcompress(sxpar(log,'FILTER',count=nmatches),/remove_all)
if(nmatches ne 1) then filter='' ;;;; defaults to none

return,result

end
