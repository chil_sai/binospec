pro bino_create_wavesol_ms,logfile,slit=slit,ndeg=ndeg,smy=smy,pady=pady,$
    debug=debug,plot=plot,y_ndeg=y_ndeg,oh=oh,no3d=no3d,quick=quick,adjusted=adjusted

aaa=''
if(n_elements(ndeg) ne 1) then ndeg=3
if(n_elements(y_ndeg) ne 1) then y_ndeg=3
wdir=def_wdir(logfile)
grating=def_grating(logfile,filter=filter)
if(keyword_set(quick)) then begin
    Ndeg=(grating eq '270')? 4 : 3
endif

dist_map_a=mrdfits(wdir+'dist_map_A.fits',1)
dist_map_b=mrdfits(wdir+'dist_map_B.fits',1)

if keyword_set(quick) then begin
  h1=headfits(wdir+'obj_dark.fits',ext=1)
  h2=headfits(wdir+'obj_dark.fits',ext=2)
  Nx=sxpar(h1,'NAXIS1')
  Ny_a=sxpar(h1,'NAXIS2')
  Ny_b=sxpar(h2,'NAXIS2') 
  mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
endif else begin
  h1=headfits(wdir+'arc_dark.fits',ext=1)
  h2=headfits(wdir+'arc_dark.fits',ext=2)
  Nx=sxpar(h1,'NAXIS1')
  Ny_a=sxpar(h1,'NAXIS2')
  Ny_b=sxpar(h2,'NAXIS2') 
  mask_a=read_mask_bino_ms(wdir+'arc_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'arc_dark.fits',side='B')
endelse
good_sides=[1,1]
n_slits_a=n_elements(mask_a)
if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
  good_sides[0]=0
  n_slits_a=0
endif 
n_slits_b=n_elements(mask_b)
if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
  good_sides[1]=0
  n_slits_b=0
endif 
if(n_elements(slit) eq 0) then slit=findgen(n_slits_a+n_slits_b)
n_slits=n_slits_a+n_slits_b

if n_slits_a gt 0 then y_slits_a=bino_get_slit_region(mask_a,Nx=Nx,Ny=Ny_a,pady=pady,dist_map=dist_map_a,slit_geom=slit_geom_a) else y_slits_a=-1
if n_slits_b gt 0 then y_slits_b=bino_get_slit_region(mask_b,Nx=Nx,Ny=Ny_b,pady=pady,dist_map=dist_map_b,slit_geom=slit_geom_b) else y_slits_b=-1

pix_mask_str={y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
    w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1,$
    wl_sol:dblarr(Ndeg+1)+!values.d_nan,wl_s_err:!values.d_nan}
pix_mask_data_a=replicate(pix_mask_str,Ny_a)
pix_mask_data_b=replicate(pix_mask_str,Ny_b)
disper_all_a=dblarr(Ndeg+2,Ny_a)
disper_all_b=dblarr(Ndeg+2,Ny_b)
if good_sides[0] then begin
  tags=tag_names(slit_geom_a[0])
  for i=0,n_elements(tags)-1 do pix_mask_data_a.(i)=slit_geom_a.(i)
endif
if good_sides[1] then begin
  tags=tag_names(slit_geom_b[0])
  for i=0,n_elements(tags)-1 do pix_mask_data_b.(i)=slit_geom_b.(i)
endif

if(~keyword_set(no3d) and (n_slits_a ge 8)) then begin
    data_3d_a=dblarr(5,500000l)
    errdata_a=dblarr(500000l)
    np_a=0l
    flag_3d_a=1
endif else begin
    data_3d_a=dblarr(5,500000l)
    errdata_a=dblarr(500000l)
    flag_3d_a=0
    np_a=0l
endelse

if(~keyword_set(no3d) and (n_slits_b ge 8)) then begin
    data_3d_b=dblarr(5,500000l)
    errdata_b=dblarr(500000l)
    np_b=0l
    flag_3d_b=1
endif else begin
    data_3d_b=dblarr(5,500000l)
    errdata_b=dblarr(500000l)
    flag_3d_b=0
    np_b=0l
endelse
for j=0,n_slits-1 do begin
    i=slit[j]
    ymin_cur = ((j lt n_slits_a) and (n_slits_a gt 0))? y_slits_a[0,i] : y_slits_b[0,i-n_slits_a]
    ymax_cur = ((j lt n_slits_a) and (n_slits_a gt 0))? y_slits_a[1,i] : y_slits_b[1,i-n_slits_a]

    maskentry=((j lt n_slits_a) and (n_slits_a gt 0))? mask_a[i] : mask_b[i-n_slits_a]
    if(~keyword_set(quick) and (strcompress(maskentry.type,/remove_all) ne 'TARGET')) then continue

    print,'Processing slit: ',i+1,'/',n_slits,ymin_cur,ymax_cur
    y_scl=((j lt n_slits_a) and (n_slits_a gt 0))? 24.555832d : 24.548194d
    if((j lt n_slits_a) and (n_slits_a gt 0) and tag_exist(dist_map_a,'mask_y_scl')) then y_scl=dist_map_a.mask_y_scl
    if(j ge n_slits_a and tag_exist(dist_map_b,'mask_y_scl')) then y_scl=dist_map_b.mask_y_scl

    fwhm=(maskentry.width*y_scl) > 2.0
    
    if(keyword_set(quick)) then begin
        hdr_slit=headfits(wdir+'obj_slits.fits',ext=i+1)
        side=strmid(sxpar(hdr_slit,'SIDE'),0,1)
        tilt=sxpar(h1,'TAPEPOS'+((side eq 'A')? '1' : '2'))
        xpos=sxpar(hdr_slit,'SLITX')
        ypos=sxpar(hdr_slit,'SLITY')
        xpos_cur=xpos+dblarr(ymax_cur-ymin_cur+1)
        ypos_cur=ypos+(findgen(ymax_cur-ymin_cur+1)-(ymax_cur-ymin_cur)/2)/y_scl

        for y=ymin_cur,ymax_cur do begin
            wltmp=bino_wlsol_iguess(grating,tilt,xpos_cur[y-ymin_cur],ypos=ypos_cur[y-ymin_cur],side=side,order=order,period=period)
            if(side eq 'A') then begin
                disper_all_a[0:n_elements(wltmp)-1,y]=wltmp
                disper_all_a[n_elements(wltmp)-1:*,y]=0d
                pix_mask_data_a[y].wl_sol=wltmp
                pix_mask_data_a[y].wl_s_err=0d
            endif else begin
                disper_all_b[0:n_elements(wltmp)-1,y]=wltmp
                disper_all_b[n_elements(wltmp)-1:*,y]=0d
                pix_mask_data_b[y].wl_sol=wltmp
                pix_mask_data_b[y].wl_s_err=0d
            endelse
        endfor
    endif else begin
        status_slit=crea_disper_bino_slit(logfile,i,fwhm,Ndeg,yfit='Yfit',plot=plot,NdegY=y_ndeg,$
            smy=smy,pssuff='_slit'+string(i+1,format='(i4.4)'),pady=pady,$
            ymin_out=ymin_cur,ymax_out=ymax_cur,oh=oh,side=side,disper_par=disper_par_slit,debug=debug,pos_lines_str=pos_lines_str)
        if(status_slit eq 0) then begin
            if((flag_3d_a eq 1 and j lt n_slits_a and n_slits_a gt 0) or (flag_3d_b eq 1 and j ge n_slits_a)) then begin
                nl=n_elements(pos_lines_str)
                for k=0,nl-1 do begin
                    pos_lines_cur=pos_lines_str[k]
                    np_cur=n_elements(pos_lines_cur.xpix)
                    if(j lt n_slits_a) and (n_slits_a gt 0) then begin
                        data_3d_a[0,np_a:np_a+np_cur-1]=pos_lines_cur.xpix
                        data_3d_a[1,np_a:np_a+np_cur-1]=pos_lines_cur.xmask
                        data_3d_a[2,np_a:np_a+np_cur-1]=pos_lines_cur.ypix
                        data_3d_a[3,np_a:np_a+np_cur-1]=pos_lines_cur.wl
                        data_3d_a[4,np_a:np_a+np_cur-1]=i
                        errdata_a[np_a:np_a+np_cur-1]=1.0/((pos_lines_cur.flux)>0.01)^0.25
                        np_a+=np_cur
                    endif else begin
                        data_3d_b[0,np_b:np_b+np_cur-1]=pos_lines_cur.xpix
                        data_3d_b[1,np_b:np_b+np_cur-1]=pos_lines_cur.xmask
                        data_3d_b[2,np_b:np_b+np_cur-1]=pos_lines_cur.ypix
                        data_3d_b[3,np_b:np_b+np_cur-1]=pos_lines_cur.wl
                        data_3d_b[4,np_b:np_b+np_cur-1]=i
                        errdata_b[np_b:np_b+np_cur-1]=1.0/((pos_lines_cur.flux)>0.01)^0.25
                        np_b+=np_cur
                    endelse
                endfor
            endif
            ;;dtmp=readfits(wdir+'disper.fits',/silent)
            dtmp=disper_par_slit
            if(side eq 'A') then begin
                disper_all_a[*,ymin_cur:ymax_cur]=dtmp[*,*]
                pix_mask_data_a[ymin_cur:ymax_cur].wl_sol=dtmp[0:Ndeg,*]
                pix_mask_data_a[ymin_cur:ymax_cur].wl_s_err=transpose(dtmp[Ndeg+1,*])
            endif else begin
                disper_all_b[*,ymin_cur:ymax_cur]=dtmp[*,*]
                pix_mask_data_b[ymin_cur:ymax_cur].wl_sol=dtmp[0:Ndeg,*]
                pix_mask_data_b[ymin_cur:ymax_cur].wl_s_err=transpose(dtmp[Ndeg+1,*])
            endelse
            if(keyword_set(debug)) then begin
                aaa=''
                npix_cur=(side eq 'A')? n_elements(where(finite(pix_mask_data_a[ymin_cur:ymax_cur].y_mask) eq 1)) : n_elements(where(finite(pix_mask_data_b[ymin_cur:ymax_cur].y_mask) eq 1))
                print,'npix=',ymax_cur-ymin_cur+1,'/',npix_cur                    
                ;read,aaa
            endif
        endif
    endelse
endfor

writefits,wdir+'disper.fits',disper_all_a
mwrfits,disper_all_b,wdir+'disper.fits'
writefits,wdir+'disper_table.fits',0
mwrfits,pix_mask_data_a,wdir+'disper_table.fits'
mwrfits,pix_mask_data_b,wdir+'disper_table.fits'

if(flag_3d_a eq 1 or flag_3d_b eq 1 and ~keyword_set(quick)) then begin
    writefits,wdir+'wl_data_3d.fits',0
    h_empty=headfits(wdir+'wl_data_3d.fits')
    if(np_a gt 1 and flag_3d_a eq 1) then gdata_a=where(finite(total(data_3d_a[*,0:np_a-1],1)+errdata_a[0:np_a]) eq 1, np_a) else flag_3d_a=0
    if(flag_3d_a eq 1) then begin
        data_3d_a=data_3d_a[*,gdata_a]
        errdata_a=errdata_a[gdata_a]
    endif else begin
        data_3d_a=dblarr(5)
        errdata_a=0
        np_a=1
    endelse
    if(np_b gt 1 and flag_3d_b eq 1) then gdata_b=where(finite(total(data_3d_b[*,0:np_b-1],1)+errdata_b[0:np_b]) eq 1, np_b) else flag_3d_b=0
    if(flag_3d_b eq 1) then begin
        data_3d_b=data_3d_b[*,gdata_b]
        errdata_b=errdata_b[gdata_b]
    endif else begin
        data_3d_b=dblarr(5)
        errdata_b=0
        np_b=1
    endelse
    sxaddpar,h_empty,'NP_A',np_a
    sxaddpar,h_empty,'NP_B',np_b
    writefits,wdir+'wl_data_3d.fits',0,h_empty
    mwrfits,data_3d_a,wdir+'wl_data_3d.fits',/silent
    mwrfits,data_3d_b,wdir+'wl_data_3d.fits',/silent
    mwrfits,errdata_a,wdir+'wl_data_3d.fits',/silent
    mwrfits,errdata_b,wdir+'wl_data_3d.fits',/silent

    bino_adjust_wavesol_ms,logfile,suff='_adj',wlndeg=ndeg,flag_3d_a=flag_3d_a,flag_3d_b=flag_3d_b,slit=slit, pady=pady
    adjusted=1
endif else adjusted=0

end
