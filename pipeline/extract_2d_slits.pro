pro extract_2d_slits,logfile,image_type,unproc=unproc,nflat=nflat,out_img_type=out_img_type, noflat=noflat, pady=pady

if(n_params() eq 1) then image_type='obj'
wdir=def_wdir(logfile)
instrument=def_inst(logfile)
log=readlog(logfile)
val=sxpar(log,'DITHPOS',count=cntval)
dithpos=(cntval eq 1)? double((strsplit(val,',',/extract))[0]) : 0.0
pady=0

if(keyword_set(nflat)) then begin
    image_type='flatn'
    img_a=mrdfits(wdir+'flat_norm.fits',1,ha)
    img_b=mrdfits(wdir+'flat_norm.fits',2,hb)
endif else begin
    img_a=(keyword_set(unproc))? rotate(mrdfits(wdir+image_type[0]+'_dark.fits',1,ha),5) : mrdfits(wdir+image_type[0]+'_ff.fits',1,ha)
    img_b=(keyword_set(unproc))? mrdfits(wdir+image_type[0]+'_dark.fits',2,hb) : mrdfits(wdir+image_type[0]+'_ff.fits',2,hb)
endelse
nx=sxpar(ha,'NAXIS1')
ny=sxpar(hb,'NAXIS2')

if keyword_set(noflat) then begin
  mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
endif else begin
  mask_a=read_mask_bino_ms(wdir+'flat_dark.fits',side='A')
  mask_b=read_mask_bino_ms(wdir+'flat_dark.fits',side='B')
endelse

dist_map_a=mrdfits(wdir+'dist_map_A.fits',1,/silent)
dist_map_b=mrdfits(wdir+'dist_map_B.fits',1,/silent)

good_sides=[1,1]
n_slits_a=n_elements(mask_a)
if n_slits_a eq 1 and not tag_exist(mask_a, 'slit') then begin
  good_sides[0]=0
  n_slits_a=0
endif
n_slits_b=n_elements(mask_b)
if n_slits_b eq 1 and not tag_exist(mask_b, 'slit') then begin
  good_sides[1]=0
  n_slits_b=0
endif

if good_sides[0] then slit_reg_a=bino_get_slit_region(mask_a,nx=nx,ny=ny,dist_map=dist_map_a,slit_trace=slit_trace_a, pady=pady, obj_trace=obj_trace_a) else slit_reg_a=-1
if good_sides[1] then slit_reg_b=bino_get_slit_region(mask_b,nx=nx,ny=ny,dist_map=dist_map_b,slit_trace=slit_trace_b, pady=pady, obj_trace=obj_trace_b) else slit_reg_b=-1

if(n_elements(out_img_type) ne 1) then out_img_type=image_type[0]
f_out=wdir+out_img_type+'_slits.fits'

h=ha
sxdelpar,h,'NAXIS1'
sxdelpar,h,'NAXIS2'
sxaddpar,h,'NAXIS',0

writefits,f_out,0,h

for i=0,n_slits_a+n_slits_b-1 do begin
    if((i gt 0) and (n_elements(image_type) eq n_slits_a+n_slits_b)) then begin
        if(image_type[i] ne image_type[i-1]) then begin
            img_a=(keyword_set(unproc))? rotate(mrdfits(wdir+image_type[i]+'_dark.fits',1),5) : mrdfits(wdir+image_type[i]+'_ff.fits',1,ha)
            img_b=(keyword_set(unproc))? mrdfits(wdir+image_type[i]+'_dark.fits',2) : mrdfits(wdir+image_type[i]+'_ff.fits',2,hb)
        endif
    endif

    ny_img=((i lt n_slits_a) and (n_slits_a gt 0))? n_elements(img_a[0,*]) : n_elements(img_b[0,*])

;    print,'Processing ',i+1,'/',n_slits_a+n_slits_b
    cur_y_tr=((i lt n_slits_a) and (n_slits_a gt 0))? slit_trace_a[i].y_trace :  slit_trace_b[i-n_slits_a].y_trace
    cur_objy_tr=((i lt n_slits_a) and (n_slits_a gt 0))? obj_trace_a[i].y_trace :  obj_trace_b[i-n_slits_a].y_trace
    ;slit_h_cur=(i lt n_slits_a)? fix(slit_trace_a[i].slit_h/2.0) : fix(slit_trace_b[i-n_slits_a].slit_h/2.0)
    slit_h_cur=((i lt n_slits_a) and (n_slits_a gt 0))? fix((slit_reg_a[1,i]-slit_reg_a[0,i])/2.0) : fix((slit_reg_b[1,(i-n_slits_a)]-slit_reg_b[0,(i-n_slits_a)])/2.0)
    slit_h_cur=slit_h_cur+pady
    ymean=fix(cur_y_tr[nx/2-1])
    yobjmean=mean(cur_objy_tr-cur_y_tr)
    print,'Processing ',i+1,'/',n_slits_a+n_slits_b,' ymean=',ymean
    yhsize=max(ceil(abs(cur_y_tr-ymean)))+slit_h_cur
    slit_img=fltarr(nx,2*yhsize+1)+!values.f_nan
    y_off =fix(((i lt n_slits_a) and (n_slits_a gt 0))? slit_reg_a[0,i] : slit_reg_b[0,(i-n_slits_a)])-max(ceil(abs(cur_y_tr-ymean)))-pady
    ;y_off = ymean-yhsize

    for x=0,nx-1 do begin
        y_cur=cur_y_tr[x]
        ymin_cur=(y_cur-slit_h_cur) ;;; > 0
        ymax_cur=(y_cur+slit_h_cur) ;;; < (ny-1)
        dymin = (ymin_cur-y_off lt 0)?  -(ymin_cur-y_off) : 0.0
        dymax = (ymax_cur ge ny)? ymax_cur-ny-1 : 0.0
        
        slit_img_y0=fix(1e-5+y_cur-y_off-slit_h_cur+dymin)
        slit_img_y1=fix(1e-5+y_cur-y_off+slit_h_cur-dymax)
        img_y0=fix(1e-5+ymin_cur+dymin)
        img_y1=fix(1e-5+ymax_cur-dymax)
        ;if (slit_img_y1-slit_img_y0) ne (img_y1- img_y0) then stop
        safe_array_bounds,2*yhsize+1,ny_img,slit_img_y0,slit_img_y1,img_y0,img_y1

        if((slit_img_y0 le slit_img_y1) and (img_y0 le img_y1) and $
           (slit_img_y1 lt 2*yhsize+1) and (img_y1 lt ny_img)) then slit_img[x,slit_img_y0:slit_img_y1]=((i lt n_slits_a) and (n_slits_a gt 0))? img_a[x,img_y0:img_y1] : img_b[x,img_y0:img_y1]
    endfor
    mkhdr,hdr_out,slit_img,/image
    maskentry=((i lt n_slits_a) and (n_slits_a gt 0))? mask_a[i] : mask_b[i-n_slits_a]
    sxaddpar,hdr_out,'EXTNAME',strupcase(maskentry.type)+string(i+1,format='(i3.3)')
    sxaddpar,hdr_out,'YOFFSET',y_off 
    sxaddpar,hdr_out,'AIRMASS',sxpar(h,'AIRMASS')
    sxaddpar,hdr_out,'SIDE',((i lt n_slits_a) and (n_slits_a gt 0))? 'A' : 'B'
    sxaddpar,hdr_out,'SLITID',maskentry.slit,' slit id'
    sxaddpar,hdr_out,'SLITRA',maskentry.ra,' ra target coordinate'
    sxaddpar,hdr_out,'SLITDEC',maskentry.dec,' dec target coordinate'
;    sxaddpar,hdr_out,'SLITX',maskentry.x,' x slit coordinate'
    sxaddpar,hdr_out,'SLITYTAR',maskentry.y,'target y mask coord (mm)'
;   quick fix for tilted slits
    sxaddpar,hdr_out,'SLITX',total(maskentry.bbox[[0,2,4,6]])/4.0,' x slit coord (mm)'
    sxaddpar,hdr_out,'SLITY',total(maskentry.bbox[[1,3,5,7]])/4.0,' y slit coord (m)'
    sxaddpar,hdr_out,'SLITTARG',maskentry.target,' target'
    sxaddpar,hdr_out,'SLITOBJ',maskentry.object,' object'
    sxaddpar,hdr_out,'SLITTYPE',maskentry.type,' type'
    sxaddpar,hdr_out,'SLITHEIG',(maskentry.bbox[5]-maskentry.bbox[1]),' slit height (mm)'
    sxaddpar,hdr_out,'SLITWIDT',maskentry.width,' slit width (mm)'
    sxaddpar,hdr_out,'SLITOFFS',maskentry.offset,' slit offset'
    sxaddpar,hdr_out,'SLITTHET',maskentry.theta,' slit PA (theta)'
    target_offset=(maskentry.y-(maskentry.bbox[5]+maskentry.bbox[1])/2.0)*5.98802
    ccdscl=0.24436 ;0.24
    slitypix=(dithpos+target_offset)/ccdscl+yhsize ;-1.0 ;;WHY -1?
    print, yobjmean+ymean-y_off, slitypix
    sxaddpar,hdr_out,'SLITYPIX',slitypix,' target pos in pixels from bottom of slit'
    ;sxaddpar,hdr_out,'SLITYPIX',((maskentry.y-maskentry.bbox[1])*5.98802)/0.24,' target pos in pixels from bottom of slit'
    sxaddpar,hdr_out,'MASKID',maskentry.mask_id,' Mask ID from the database'
    sxaddpar,hdr_out,'MASKNAME',maskentry.mask_name,' Mask name from the database'
    sxaddpar,hdr_out,'MASKRA',maskentry.mask_ra,' RA of the field center'
    sxaddpar,hdr_out,'MASKDEC',maskentry.mask_dec,' Dec of the field center'
    sxaddpar,hdr_out,'MASKPA',maskentry.mask_pa,' PA of the mask'
    sxaddpar,hdr_out,'TILTEDSL',((abs(maskentry.bbox[4]-maskentry.bbox[0]) gt 1.5*maskentry.width)? 1 : 0),' tilted slit 1=Yes/0=No'
    mwrfits,slit_img,f_out,hdr_out,/silent
endfor

end
