pro bino_combine_sky_ms,logfile,image_type,nstages=nstages,verbose=verbose

if(n_params() eq 1) then image_type='obj'
if(n_elements(nstages) ne 2) then nstages=[1,1]
wdir=def_wdir(logfile)
mask_a=read_mask_bino_ms(wdir+'obj_dark.fits',side='A')
mask_b=read_mask_bino_ms(wdir+'obj_dark.fits',side='B')
good_sides=[1,1]
    n_slits_a=n_elements(mask_a)
    if (n_slits_a eq 1) and not tag_exist(mask_a, 'slit') then begin
      good_sides[0]=0
      n_slits_a=0
    endif 
    n_slits_b=n_elements(mask_b)
    if (n_slits_b eq 1) and not tag_exist(mask_b, 'slit') then begin
      good_sides[1]=0
      n_slits_b=0
    endif 

file_out=wdir+'/sky_'+image_type+'_2d_bspl_slits.fits'
file_cnt=wdir+'/sky_'+image_type+'_2d_bspl_slits_cnt.fits'
file_blue=wdir+'/sky_'+image_type+'_2d_bspl_slits_blue.fits'
file_red=wdir+'/sky_'+image_type+'_2d_bspl_slits_red.fits'
h0=headfits(file_cnt)
writefits,file_out,0,h0
for i=0,n_slits_a+n_slits_b-1 do begin
    print,'Combining sky models for slit #'+string(i+1,format='(i4)')+'/'+string(n_elements(mask_a)+n_elements(mask_b),format='(i4)')
    s_c=mrdfits(file_cnt,i+1,h_c,/silent)
    s_out=s_c

    if((nstages[0] gt 1 and i lt n_slits_a) or $
       (nstages[1] gt 1 and i ge n_slits_a)) then begin
        s_b=mrdfits(file_blue,i+1-(nstages[0] eq 1)*n_slits_a,/silent)
        s_r=mrdfits(file_red,i+1- (nstages[0] eq 1)*n_slits_a,/silent)

        overlap_blue=where(finite(s_b) eq 1 and finite(s_c) eq 1, coverlap_blue) ;; overlap in the blue
        add_blue = (coverlap_blue gt 2)? median(s_c[overlap_blue]-s_b[overlap_blue]) : 0d ;;; additive term is used rather then multiplicative to prevent 0/0
        pix_blue=where(finite(s_b) eq 1 and finite(s_c) ne 1, cpix_blue)
        if(cpix_blue gt 0) then s_out[pix_blue]=s_b[pix_blue]+add_blue

        overlap_red=where(finite(s_r) eq 1 and finite(s_c) eq 1, coverlap_red) ;; overlap in the red
        add_red = (coverlap_red gt 2)? median(s_c[overlap_red]-s_r[overlap_red]) : 0d ;;; additive term is used rather then multiplicative to prevent 0/0
        pix_red=where(finite(s_r) eq 1 and finite(s_c) ne 1, cpix_red)
        if(cpix_red gt 0) then s_out[pix_red]=s_r[pix_red]+add_red
        if(keyword_set(verbose)) then print,'add_blue,add_red=',add_blue,add_red,coverlap_blue,coverlap_red
    endif
    mwrfits,s_out,file_out,h_c,/silent
endfor

end
