;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  This function returns the initial guess for the Binospec wavelength solution
;  Input parameters are:
;           grating -- grating ID, one of 'H', 'K', or 'HK'
;           xpos  -- X position of the slit (in a mask or a longslit)
;  Optional input keywords:
;           order -- order of dispersion. Defaults to 1
;  Return value:
;           polynomial coefficients of the wavelength solution
;           if xpos contains more than one element then the output array
;           will be two-dimensional (N_deg+1) by n_elements(xpos)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function bino_wlsol_iguess_old,grating,tilt,xpos0,ypos=ypos,side=side,order=order,period=period

if(n_elements(side) ne 1) then side='A'
if(n_elements(order) ne 1) then order=1
if(n_params() eq 1) then xpos0=[0.0]
if(n_elements(ypos) ne n_elements(xpos0)) then ypos=xpos0*0.0
if(n_elements(period) ne 1) then period='2017B'

xpos=xpos0
;;; slit curvature correction
;for i=0,n_elements(xpos)-1 do $
;    xpos[i]=xpos0[i]+poly(ypos[0],[-0.0130282,0.00200443,-0.000107925])

if(grating eq '270') then begin
    kwl_a = [2813.5780,   1.285,   8.6321460e-06,  -6.4567343e-11]
    kwl_b = [2813.5780,   1.285,   8.6321460e-06,  -6.4567343e-11]
    tiltpos0=[32.,33.,34.]
    cntwl0=[5495.,6685.,7873.]
    ck=poly_fit(tiltpos0,cntwl0,1)
    cntwl_ini1=poly(tilt,ck)
    cntwl_ini2=poly(tilt,ck)

    kwl_a[0]+=cntwl_ini1-5495.
    kwl_b[0]+=cntwl_ini1-5495.-18.
    xscl= -28.2-1.0

    slit_curv=-4.0
endif

;xpos=xpos0+poly(ypos,[0.,0.,-slit_curv/33.5^2*dwl_coeff[0]/wl0_coeff[1]])

xcnt=(side eq 'A')? 56.236d : -56.236d

if(n_elements(xpos) gt 1) then begin
    dis_arr = dblarr(n_elements(kwl_a),n_elements(xpos))
    for i=0,n_elements(xpos)-1 do dis_arr[*,i]=(side eq 'A')? kwl_a[*] : kwl_b[*]
    dis_arr[0,*]+=xscl*(transpose(xpos)-xcnt)
    
;    if(grating eq '270') then dis_arr[1,*]=transpose(poly(xpos-0*1.5*sqrt(xpos^2+ypos^2),dwl_coeff))
endif else begin
    dis_arr=(side eq 'A')? kwl_a : kwl_b
    dis_arr[0]+=xscl*(xpos-xcnt)
;    if(grating eq '270') then dis_arr[1]=poly(xpos-0*1.5*sqrt(xpos^2+ypos^2),dwl_coeff)
endelse
;
;dis_arr=dis_arr/double(order)
print,'xpos,tilt,DIS_ARR',xpos,tilt,side,'---',dis_arr

return, dis_arr

end
