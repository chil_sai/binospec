pro clean_hdr,h
sxdelpar,h,'SIMPLE'
sxdelpar,h,'XTENSION'
sxdelpar,h,'EXTNAME'
sxdelpar,h,'EXTVER'
sxdelpar,h,'INHERIT'
sxdelpar,h,'DATAMIN'
sxdelpar,h,'DATAMAX'
sxdelpar,h,'NAXIS'
sxdelpar,h,'NAXIS1'
sxdelpar,h,'NAXIS2'
sxdelpar,h,'BITPIX'
sxdelpar,h,'PCOUNT'
sxdelpar,h,'GCOUNT'
sxaddpar,h,'BZERO',0.0
h_data=where(strcompress(h,/remove_all) ne '')
h=h[h_data]
end


