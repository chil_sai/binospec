function bino_get_slit_region,maskentry,Nx=Nx,Ny=Ny,ratio=ratio,pady=pady,$
        dist_map=dist_map,slit_geom=slit_geom,slit_trace=slit_trace, obj_trace=obj_trace
    n_slits=n_elements(maskentry)

    res=dblarr(2,n_slits)
    if(n_elements(pady) ne 1) then pady=0
    if(n_elements(Nx) ne 1) then Nx=4096
    if(n_elements(Ny) ne 1) then Ny=4112
    if(n_elements(ratio) ne 1) then ratio=1.0
    slit_geom=replicate({y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
                         w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1},Ny)

    slit_trace=replicate({x_trace:dindgen(Nx),y_trace:dblarr(Nx)+!values.d_nan,slit_h:!values.d_nan},n_slits)
    obj_trace=replicate({x_trace:dindgen(Nx),y_trace:dblarr(Nx)+!values.d_nan,slit_h:!values.d_nan},n_slits)
    ;x_slits=double(maskentry.x)
    y_slitobj=-double(maskentry.y)
    ;dx_slits=double(maskentry.width)
    ;dy_slits=double(maskentry.height)
    x_slits=double(total(maskentry.bbox[[0,2,4,6]],1)/4.0)
    y_slits=-double(total(maskentry.bbox[[1,3,5,7]],1)/4.0)
    y_slitsh=-double(total(maskentry.bbox[[1,3]],1)/2.0)
    y_slitsl=-double(total(maskentry.bbox[[5,7]],1)/2.0)
    dx_slits=double(maskentry.width)
    dy_slits=abs(double(maskentry.bbox[5]-maskentry.bbox[1]))

    dy0=-200.0
;;;    y_scl=24.5772 ;; =(3878.0-216.0)/149 pix/mm
    y_scl=(maskentry[0].side eq 'A')? 24.555832d : 24.548194d

    if(n_elements(dist_map) eq 1) then begin
        if(tag_exist(dist_map,'mask_dy0')) then dy0=dist_map.mask_dy0
        if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl
    endif
    x_slits_pix=(x_slits-maskentry[0].corners[0])*y_scl+Nx/2.0
    y_slits_pix=Ny-1-((y_slits-maskentry[0].corners[1])*y_scl)+dy0
    y_slitobj_pix=Ny-1-((y_slitobj-maskentry[0].corners[1])*y_scl)+dy0
    y_slitsl_pix=Ny-1-((y_slitsl-maskentry[0].corners[1])*y_scl)+dy0
    y_slitsh_pix=Ny-1-((y_slitsh-maskentry[0].corners[1])*y_scl)+dy0
    if y_slitsh_pix[0] lt y_slitsl_pix[0] then begin
      print, 'reverse hi/lo!'
      y_slitsh_pix2=y_slitsl_pix
      y_slitsl_pix=y_slitsh_pix
      y_slitsh_pix=y_slitsh_pix2
    endif    

    if(n_elements(dist_map) eq 1) then begin
        dy=poly2d(x_slits_pix,y_slits_pix,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)
        dy_slitobj=poly2d(x_slits_pix,y_slitobj_pix,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)
        dy_slitsh_pix=poly2d(x_slits_pix,y_slitsh_pix,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)
        dy_slitsl_pix=poly2d(x_slits_pix,y_slitsl_pix,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)
    endif else begin
       dy=dblarr(n_slits)
       dy_slitobj=dblarr(n_slits)
       dy_slitsh_pix=dblarr(n_slits)
       dy_slitsl_pix=dblarr(n_slits)
    endelse
    dx_slits_pix=dx_slits*y_scl*ratio
    dy_slits_pix=dy_slits*y_scl*ratio

    for i=0,n_slits-1 do begin
        res[0,i]=0 > (round(y_slits_pix[i]+dy[i]-dy_slits_pix[i]/2.0-pady)) < (Ny-1)
        res[1,i]=0 > (round(y_slits_pix[i]+dy[i]+dy_slits_pix[i]/2.0-1+pady)) < (Ny-1)
        ;res[0,i]=0 > (round(y_slitsl_pix[i]+dy_slitsl_pix[i]-pady)) < (Ny-1)
        ;res[1,i]=0 > (round(y_slitsh_pix[i]+dy_slitsh_pix[i]+pady)) < (Ny-1)
        slit_trace[i].y_trace = (n_elements(dist_map) eq 1)? $
              poly2d(dindgen(Nx),dindgen(Nx)*0+y_slits_pix[i],dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_slits_pix[i]+dy[i] : $
              dindgen(Nx)*0.0+y_slits_pix[i]
        slit_trace[i].slit_h=(res[1,i]-res[0,i]);dy_slits_pix[i] ; 
        obj_trace[i].y_trace = (n_elements(dist_map) eq 1)? $
              poly2d(dindgen(Nx),dindgen(Nx)*0+y_slitobj_pix[i],dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_slitobj_pix[i]+dy_slitobj[i] : $
              dindgen(Nx)*0.0+y_slitobj_pix[i]
        obj_trace[i].slit_h=(res[1,i]-res[0,i]);dy_slits_pix[i] ; 
        for y=((res[0,i]-max(abs(dy))) > 0),((res[1,i]+max(abs(dy[i]))) < (Ny-1)) do begin
            slit_geom[y].y_pix=y
            slit_geom[y].x_mask=total(maskentry[i].bbox[[0,2,4,6]])/4.0+$
                (y-y_slits_pix[i])/y_scl*(maskentry[i].bbox[0]-maskentry[i].bbox[2])/$
                                         (maskentry[i].bbox[1]-maskentry[i].bbox[3])
            slit_geom[y].y_mask=total(maskentry[i].bbox[[1,3,5,7]])/4.0+(y-y_slits_pix[i])/y_scl
            slit_geom[y].w_pix=maskentry[i].width
            slit_geom[y].h_pix=(maskentry[i].bbox[5]-maskentry[i].bbox[1])/2.0+(y_slits_pix[i]-y)/y_scl
            slit_geom[y].slit=maskentry[i].slit
        endfor
    endfor
    return,res
end


