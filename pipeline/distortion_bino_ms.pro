pro distortion_bino_ms,logfile,image_type,side=side,gzip=gzip,deg1=deg1,deg2=deg2,diff=diff,force=force,quicklook=quicklook, pady=pady, fullprof=fullprof

if(n_elements(side) ne 1) then side='A'
wdir=def_wdir(logfile)
grating=def_grating(logfile,filter=filter)
slit=def_slit(logfile)

if(n_params() eq 1) then image_type='flat'
suff=(keyword_set(gzip))? '.gz' : ''

period=get_bino_period(wdir+image_type+'_dark.fits'+suff)

longslit=(slit eq 'mos')? 0 : 1
;if(slit ne 'mos') then begin ;;;; longslit assumed
;    mapfile=getenv('BINO_PIPELINE_PATH')+'calib_Bino/LS/'+period+'/'+grating+'/dist_map_'+side+'_??.fits'
;    if(~file_test(mapfile)) then mapfile=getenv('BINO_PIPELINE_PATH')+'calib_Bino/LS/default/'+grating+'/dist_map_'+side+'_??.fits'
;    file_copy,mapfile,wdir+'/dist_map_'+side+'.fits',/overwrite
;    return
;endif

shortspec = 0
dx_prof=2

if(n_elements(deg1) ne 1) then deg1=(shortspec eq 1)? 2 : 3
if(longslit eq 1) then deg2=1
if(n_elements(deg2) ne 1) then deg2=(shortspec eq 1)? 2 : 3 ;;; 1:3

h=headfits(wdir+image_type+'_dark.fits'+suff)
img=(mrdfits(wdir+image_type+'_dark.fits'+suff,((side eq 'A')? 1 : 2),himg,/silent))
if(side eq 'A') then img=rotate(img,5)
Nx=sxpar(himg,'NAXIS1')
Ny=sxpar(himg,'NAXIS2')

mask=read_mask_bino_ms(wdir+image_type+'_dark.fits'+suff,side=side)
n_slit=n_elements(mask)
if n_slit eq 1 and not tag_exist(mask, 'slit') then begin
    mapfile=getenv('BINO_PIPELINE_PATH')+'calib_Bino/MOS/'+period+'/'+grating+'/dist_map_'+side+'_??.fits'
    if(~file_test(mapfile)) then mapfile=getenv('BINO_PIPELINE_PATH')+'calib_Bino/MOS/default/'+grating+'/dist_map_'+side+'_??.fits'
    file_copy,mapfile,wdir+'/dist_map_'+side+'.fits',/overwrite
    return
endif


n_st=(shortspec eq 1 or longslit eq 1)? 8L : 32L
wx=(shortspec eq 1 or longslit eq 1)? 120 : 20
p_ref=median(img[Nx/2-wx:Nx/2+wx,*],dim=1)
if(longslit eq 1 or ~keyword_set(fullprof)) then p_ref=abs(shift(p_ref,dx_prof)-shift(p_ref,-dx_prof))

skippix=0 + 10
if(skippix gt 0) then begin
    p_ref[0:skippix-1]=0
    p_ref[ny-skippix-1:*]=0
endif
nsegments=(longslit eq 0)? deg2 + 5 : 2
overlap=0.2
z=(keyword_set(diff))? 20.0 : 10.0
dy_amp = (shortspec eq 1)? 15.0 : 35.0
dy=-dy_amp+findgen(2.0*dy_amp*z+1L)/z

dyarr=dblarr(n_st,nsegments)
c_arr=dyarr
xgrid=dblarr(n_st)
ygrid=dblarr(nsegments)
for xx=fix(n_st/2),n_st-1 do begin
    xcur=(xx+0.5)*(double(Ny)/n_st)
    xgrid[xx]=xcur
    p_cur=median(img[xcur-wx:xcur+wx,*],dim=1)
    sat_p_cur=where(finite(p_cur) ne 1, csat_p_cur) ;;; saturated values
    if(csat_p_cur gt 0) then p_cur[sat_p_cur]=max(p_cur,/nan)
    if(longslit eq 1 or ~keyword_set(fullprof)) then p_cur=abs(shift(p_cur,dx_prof)-shift(p_cur,-dx_prof))
    if(skippix gt 0) then begin
        p_cur[0:skippix-1]=0
        p_cur[ny-skippix-1:*]=0
    endif
    if(xx eq fix(n_st/2)) then begin
        xref_min=xcur-wx
        xref_max=xcur+wx
        prof_ref = p_cur
    endif
    if (keyword_set(diff) and (xx eq fix(n_st/2))) then begin
        p_ref = p_cur
        continue
    endif
    for i=0,nsegments-1 do begin
        ymin = (i eq 0)? skippix : (double(i)-overlap)*Ny/nsegments
        ymax = (i eq nsegments-1)? Ny-skippix-1 : (double(i)+1.0+overlap)*Ny/nsegments
        if(ymin lt 0) then ymin=0
        if(ymax ge Ny) then ymax=Ny-1
        ymin=fix(ymin)
        ymax=fix(ymax)
        ygrid[i]=(ymin+ymax)/2.0
        vec1=rebin(p_ref[ymin:ymax],(ymax-ymin+1L)*z)
        vec2=rebin(p_cur[ymin:ymax],(ymax-ymin+1L)*z)
        cf=c_correlate(vec1-min(vec1),vec2-min(vec2),dy*z)
        nn=max(cf,cnn)
        if((cnn eq 0) or (cnn eq (n_elements(dy)-1L)) or (nn lt 0.5)) then begin
            dyarr[xx,i]=!values.f_nan
            c_arr[xx,i]=nn
        endif else begin
            dyarr[xx,i]=(keyword_set(diff))? dy[cnn] : dy[cnn]
            c_arr[xx,i]=nn
        endelse
    endfor

    if(keyword_set(diff)) then p_ref=p_cur
endfor

for xx=fix(n_st/2),0,-1 do begin
    xcur=(xx+0.5)*(double(Ny)/n_st)
    xgrid[xx]=xcur
    p_cur=median(img[xcur-wx:xcur+wx,*],dim=1)
    sat_p_cur=where(finite(p_cur) ne 1, csat_p_cur) ;;; saturated values
    if(csat_p_cur gt 0) then p_cur[sat_p_cur]=max(p_cur,/nan)
    if(longslit eq 1 or ~keyword_set(fullprof)) then p_cur=abs(shift(p_cur,dx_prof)-shift(p_cur,-dx_prof))
    if(skippix gt 0) then begin
        p_cur[0:skippix-1]=0
        p_cur[ny-skippix-1:*]=0
    endif
    if (keyword_set(diff) and (xx eq fix(n_st/2))) then begin
        p_ref = p_cur
        continue
    endif
    for i=0,nsegments-1 do begin
        ymin = (i eq 0)? skippix : (double(i)-overlap)*Ny/nsegments
        ymax = (i eq nsegments-1)? Ny-skippix-1 : (double(i)+1.0+overlap)*Ny/nsegments
        if(ymin lt 0) then ymin=0
        if(ymax ge Ny) then ymax=Ny-1
        ymin=fix(ymin)
        ymax=fix(ymax)
        ygrid[i]=(ymin+ymax)/2.0
        vec1=rebin(p_ref[ymin:ymax],(ymax-ymin+1L)*z)
        vec2=rebin(p_cur[ymin:ymax],(ymax-ymin+1L)*z)
        cf=c_correlate(vec1,vec2,dy*z)
;        oplot,dy,cf,col=30*(i+1)
        nn=max(cf,cnn)
        if((cnn eq 0) or (cnn eq (n_elements(dy)-1L)) or (nn lt 0.5)) then begin
            dyarr[xx,i]=!values.f_nan
            c_arr[xx,i]=nn
        endif else begin
            dyarr[xx,i]=(keyword_set(diff))? -dy[cnn] : dy[cnn]
            c_arr[xx,i]=nn
        endelse
    endfor

    if(keyword_set(diff)) then p_ref=p_cur
;    read,zzz
endfor

if(keyword_set(diff)) then begin
    dyarr[fix(n_st/2),*]=(dyarr[fix(n_st/2)+1,*]+dyarr[fix(n_st/2)-1,*])/2.0
    dyarr_orig=dyarr
    for i=0,nsegments-1 do begin
        gg=where(finite(dyarr_orig[*,i]) eq 1,cgg)
        k_t = robust_poly_fit(xgrid[gg],dyarr_orig[gg,i]/(xgrid[1]-xgrid[0]),2)
        k_t_int = [0,k_t]/[1.0,1.0,2.0,3.0]
        dyarr[*,i]=poly(xgrid,k_t_int)
    endfor
    dyarr=dyarr-median(dyarr[fix(n_st/2)-1:fix(n_st/2),*])
endif

if(longslit eq 1) then begin
    dyarr_orig=dyarr
    dyarr=(congrid(dyarr_orig,n_st,7,/interp))[*,0:4]
    c_arr=(congrid(c_arr,n_st,7,/interp))[*,0:4]
    nsegments=5
    ygrid=(dindgen(nsegments)+0.5-overlap)*Ny/nsegments
endif

tilt=sxpar(himg,'TAPEPOS'+((side eq 'A')? '1' : '2'))
f_maps=file_search(getenv('BINO_PIPELINE_PATH')+'calib_Bino/MOS/'+period+'/'+grating+'/dist_map_'+side+'_*.fits')
if(f_maps[0] eq '') then f_maps=file_search(getenv('BINO_PIPELINE_PATH')+'calib_Bino/MOS/default/'+grating+'/dist_map_'+side+'_*.fits')
n_maps=n_elements(f_maps)
tilt_maps=fltarr(n_maps)
for m=0,n_maps-1 do tilt_maps[m]=float(strmid(f_maps[m],strpos(f_maps[m],'_',/reverse_search)+1,2))
mtilt=min(abs(tilt_maps-tilt),cmtilt)
mapfile=f_maps[cmtilt]
res_str_stored=mrdfits(mapfile,1,/silent)

xgrid2d=(xgrid # (dblarr(n_elements(ygrid))+1.0))
ygrid2d=((dblarr(n_elements(xgrid))+1.0) # ygrid)
g=where(finite(dyarr) eq 1,cg)
if(cg gt 0 and ~keyword_set(quicklook)) then begin
    darr=dblarr(3,cg)
    darr[0,*]=xgrid2d[g]
    darr[1,*]=ygrid2d[g]
    darr[2,*]=dyarr[g]

    e=sfit_2deg(darr,deg1,deg2,kx=kx,/irr);,/max)

    polywarp,xgrid2d,ygrid2d,$
         xgrid2d,ygrid2d-poly2d(xgrid,ygrid,kx,deg1=deg1,deg2=deg2),$
         3,Kxwr,Kywr

    polywarp,xgrid2d,ygrid2d-poly2d(xgrid,ygrid,kx,deg1=deg1,deg2=deg2),$
         xgrid2d,ygrid2d,$
         3,Kxwr_inv,Kywr_inv

    res_str={XGRID:xgrid, YGRID:ygrid, $
         KXWRAP:kxwr,KYWRAP:kywr, $
         KXWRAP_INV:kxwr_inv,KYWRAP_INV:kywr_inv, $
         dyarr:dyarr, corrarr:c_arr,$
         kx_2dfit:kx,deg1:deg1,deg2:deg2,mask_dy0:-200.0,mask_y_scl:((side eq 'A')? 24.555832d : 24.548194d)}
;;;         kx_2dfit:kx,deg1:deg1,deg2:deg2,mask_dy0:-200.0,mask_y_scl:((side eq 'A')? 24.542358d : 24.531163d)}
;;;         kx_2dfit:kx,deg1:deg1,deg2:deg2,mask_dy0:-200.0,mask_y_scl:24.5772d}
endif else res_str=res_str_stored

slit_reg=bino_get_slit_region(mask,nx=nx,ny=ny,dist_map=res_str,slit_trace=slit_trace, pady=pady)

prof_mod=dblarr(Ny)
for i=0,n_slit-1 do begin
    n_min=slit_reg[0,i]  ;(slit_trace[i].y_trace[Nx-(xref_min+xref_max)/2]-slit_trace[i].slit_h/2.0) > 0
    n_max=slit_reg[1,i] ;(slit_trace[i].y_trace[Nx-(xref_min+xref_max)/2]+slit_trace[i].slit_h/2.0) < (Ny-1)
    if(n_min gt n_max) then begin
        n_min_new=n_max
        n_max=n_min
        n_min=n_min_new
    endif
    prof_mod[n_min:n_max]=1.0*mask[i].width
endfor

psf_krnl=psf_gaussian(ndim=1,fwhm=[2.0],npix=9,/norm)
prof_mod=convol(prof_mod,psf_krnl)
if(longslit eq 1 or ~keyword_set(fullprof)) then prof_mod=abs(shift(prof_mod,dx_prof)-shift(prof_mod,-dx_prof))

nsegments=2
overlap=0.3
z=5.0
dy=-25.0+findgen(50*z+1L)/z
dy_shift=dblarr(nsegments)
c_shift=dy_shift
y_grid_shift=dblarr(nsegments)

for i=0,nsegments-1 do begin
    ymin = (i eq 0)? skippix : (double(i)-overlap)*Ny/nsegments
    ymax = (i eq nsegments-1)? Ny-skippix-1 : (double(i)+1.0+overlap)*Ny/nsegments
    if(ymin lt 0) then ymin=0
    if(ymax ge Ny) then ymax=Ny-1
    ymin=fix(ymin)
    ymax=fix(ymax)
    y_grid_shift[i]=(ymax+ymin)/2.0
    vec1=rebin(prof_ref[ymin:ymax],(ymax-ymin+1L)*z)
    vec2=rebin(prof_mod[ymin:ymax],(ymax-ymin+1L)*z)
    cf=c_correlate(vec1,vec2,dy*z)
    nn=max(cf,cnn)
    if(cnn eq 0 or cnn eq (n_elements(dy)-1L) or nn lt 0.03) then begin
        dy_shift[i]=!values.f_nan
        c_shift[i]=!values.f_nan
    endif else begin
        dy_shift[i]=dy[cnn]
        c_shift[i]=nn
    endelse
endfor

good_shift=where(finite(dy_shift) eq 1,cgood_shift)
if(cgood_shift eq 0) then dy_shift[*]=0.0
if(cgood_shift gt 0 and cgood_shift lt nsegments) then dy_shift[*]=median(dy_shift[good_shift])

coeff_shift=poly_fit(Ny-y_grid_shift[where(finite(dy_shift) eq 1)],dy_shift[where(finite(dy_shift) eq 1)],1)
res_str.mask_dy0=res_str.mask_dy0-coeff_shift[0]
res_str.mask_y_scl=(res_str.mask_y_scl)*(1d +coeff_shift[1])

;read,aaa
writefits,wdir+'dist_map_'+side+'.fits',0
mwrfits,res_str,wdir+'dist_map_'+side+'.fits'
end
