function clean_overscan_vector,overscan,w,nsig=nsig,rdnoise=rdnoise
    if(n_params() eq 1) then w=5
    if(w lt 3) then w=3
    if(n_elements(rdnoise) ne 1) then rdnoise=4.0
    if(n_elements(nsig) ne 1) then nsig=3.0

    m_overscan=median(overscan,w)
    bvec=where(abs(overscan-m_overscan) gt rdnoise*nsig, cbvec, compl=gvec, ncompl=cgvec)
    clean_overscan=overscan
    if(cbvec gt 0 and cgvec gt 0) then clean_overscan[bvec]=interpol(overscan[gvec],gvec,bvec)
    return, clean_overscan
end

function remove_overscan,image,datasec,subtract=subtract,median=median,overscan_vec=overscan_vec,sm_w=sm_w,noclean=noclean
;;; subtracts overscan given an image and a datasec array (4 numbers)
    if(n_elements(sm_w) ne 1) then sm_w=0
    s_im=size(image)

    overscan_x=[-1]
    if(datasec[0] gt 1) then overscan_x=[overscan_x,lindgen(datasec[0]-1l)]
    if(datasec[1] lt s_im[1]) then $
        overscan_x=[overscan_x,datasec[1]+lindgen(s_im[1]-datasec[1]-1l)]
    if(n_elements(overscan_x) gt 1 and keyword_set(subtract)) then begin
        overscan_x=overscan_x[1:*]
        if(keyword_set(median)) then $
            overscan_x_vec=median(image[overscan_x,*],dim=1) $
        else begin
            resistant_mean,image[overscan_x,*],3.0,overscan_x_vec,dim=1
            overscan_x_vec=reform(overscan_x_vec,n_elements(overscan_x_vec))
        endelse
        if(~keyword_set(noclean)) then overscan_x_vec=clean_overscan_vector(overscan_x_vec,5)
        if(sm_w gt 0) then overscan_x_vec=smooth(overscan_x_vec,sm_w,/nan)
        image_sub=image-((fltarr(s_im[1])+1) # overscan_x_vec)
    endif else image_sub=image

    overscan_y=[-1]
    if(datasec[2] gt 1) then overscan_y=[overscan_y,lindgen(datasec[2]-1l)]
    if(datasec[3] lt s_im[2]) then $
        overscan_y=[overscan_y,datasec[3]+lindgen(s_im[2]-datasec[3]-1l)]
    if(n_elements(overscan_y) gt 1 and keyword_set(subtract)) then begin
        overscan_y=overscan_y[1:*]
        if(keyword_set(median)) then $
            overscan_vec=median(image_sub[*,overscan_y],dim=2) $
        else begin
            resistant_mean,image_sub[*,overscan_y],3.0,overscan_vec,dim=2
            overscan_vec=reform(transpose(overscan_vec))
        endelse
        if(~keyword_set(noclean)) then overscan_vec=clean_overscan_vector(overscan_vec,5)
        if(sm_w gt 0) then overscan_vec=smooth(overscan_vec,sm_w,/nan)
        image_sub=image_sub-(overscan_vec # (fltarr(s_im[2])+1))
    endif ;;; else image_sub=image
    img_sub=image_sub[datasec[0]-1:datasec[1]-1,datasec[2]-1:datasec[3]-1]
    return,img_sub
end

function parse_secstr,secstr
;;; parses fits keywords in the format: '[1:8192,1:4112]' into lonarr(4)
    return,long(strsplit(strmid(secstr,1,strlen(secstr)-2),'([:,])',/regex,/extract))
end

pro fill_mosaic_image, mosaic_image, input_image, detsec
    detsec_cur=detsec
    input_image_cur=input_image
    if(detsec[1] lt detsec[0]) then begin
        detsec_cur[0]=detsec[1]
        detsec_cur[1]=detsec[0]
        input_image_cur=rotate(input_image_cur,5)
    endif
    if(detsec[3] lt detsec[2]) then begin
        detsec_cur[2]=detsec[3]
        detsec_cur[3]=detsec[2]
        input_image_cur=rotate(input_image_cur,7)
    endif
    mosaic_image[detsec_cur[0]-1:detsec_cur[1]-1,detsec_cur[2]-1:detsec_cur[3]-1]=input_image_cur
end

pro bino_mosaic, inpfile, outfile, subtract_overscan=subtract_overscan, $
    disable_nonlinearity=disable_nonlinearity, mask_bad_pixels=mask_bad_pixels, smooth_overscan=smooth_overscan

    if(~keyword_set(disable_nonlinearity)) then nonlin_str=mrdfits(getenv('BINO_PIPELINE_PATH')+'calib_Bino/detector/nonlinearity/scicam_bino_sep2017.fits',1,/silent)

    version=get_bino_pipeline_version()
    h0=headfits(inpfile)
    h1=headfits(inpfile,ext=1)
    detstr=sxpar(h1,'DETSIZE')
    detsize=parse_secstr(detstr)
    r_mosaic=fltarr(detsize[1]-detsize[0]+1l,detsize[3]-detsize[2]+1l)

;;    gain_arr=[1.181*1.209,1.0*1.209,1.173,1.0,$
;;              1.0,1.0/0.942,1.0/0.864*0.791,1.0*0.791]
;;    gain_arr=[1.553,1.315,1.276,1.088,$
;;              1.291,1.370,1.182,1.021]

    ;;; data from 2017/09/19
    ;gain_arr=[1.0854166,1.0361736,1.0354044,0.97356252,$
    ;          1.0282870,1.1091365,1.0381723,1.0353252] ;;; measured using statistics
    gain_arr=[1.085*[1.000000,0.964508,0.964508*0.995318,0.898667],1.028*[1d,1.08476d*1.04330d,1.0163d*1.00255d,1.0163d]]

    for i=1,8 do begin
        r_cur=mrdfits(inpfile,i,h_cur,/silent)
        r_cur=float(r_cur)*float(sxpar(h_cur,'BSCALE'))+float(sxpar(h_cur,'BZERO'))
        sxaddpar,h_cur,'BZERO',0
        sxaddpar,h_cur,'BSCALE',1
        gain=sxpar(h_cur,'GAIN')
        if(gain le 1e-6) then gain=gain_arr[i-1]
        sxaddpar,h_cur,'GAIN',1.0
        im_cur=remove_overscan(r_cur,parse_secstr(sxpar(h_cur,'DATASEC')),sub=subtract_overscan, sm_w=smooth_overscan)
        if(~keyword_set(disable_nonlinearity)) then im_cur=poly(im_cur,nonlin_str[i-1].c_poly)
        im_cur=im_cur*gain
        fill_mosaic_image,r_mosaic,im_cur,parse_secstr(sxpar(h_cur,'DETSEC'))
        sxdelpar,h_cur,'DETSIZE'
        sxdelpar,h_cur,'CCDSEC'
        sxdelpar,h_cur,'AMPSEC'
        sxdelpar,h_cur,'DATASEC'
        sxdelpar,h_cur,'DETSEC'
        if(i eq 1) then h1=h_cur[where(strtrim(h_cur,2) ne '',/null)]
        if(i eq 5) then h2=h_cur[where(strtrim(h_cur,2) ne '',/null)]
    endfor

    s_mos=size(r_mosaic)
    writefits,outfile,0,h0
    imaging_check=sxpar(h1, 'DISPERS1')
    if (typename(imaging_check) eq 'STRING') and (strtrim(imaging_check,2) eq 'mirror') then begin
      coord_check=sxpar(h1, 'RA')
      if coord_check then begin
       crval1=hms2dec(coord_check)*15.0
       crval2=hms2dec(sxpar(h1,'DEC'))
       crpix1=3419.30016 ;binospec 0,0 ~ field center
       crpix2=2065.07818
       scale=-1.0/4.08/3600./1.003
       flip_x=1.0
       flip_y=1.0
       pa=sxpar(h1,'POSANG')*flip_x*flip_y
       sxaddpar, h1, 'DATASEC', '[1055:3024,1:4112]'
       sxaddpar, h1, 'RADECSYS', 'ICRS'
       sxaddpar, h1, 'CUNIT1', 'deg'
       sxaddpar, h1, 'CUNIT2', 'deg'
       sxaddpar, h1, 'CTYPE1', 'RA---TAN'
       sxaddpar, h1, 'CTYPE2', 'DEC--TAN'
       sxaddpar, h1, 'CRPIX1', crpix1
       sxaddpar, h1, 'CRPIX2', crpix2
       sxaddpar, h1, 'CRVAL1', crval1
       sxaddpar, h1, 'CRVAL2', crval2
       sxaddpar, h1, 'CD1_1', scale*flip_x*cos(!pi/180.*pa)
       sxaddpar, h1, 'CD1_2', scale*flip_x*sin(!pi/180.*pa)
       sxaddpar, h1, 'CD2_1', scale*(-1.0)*flip_y*sin(!pi/180.*pa)
       sxaddpar, h1, 'CD2_2', scale*flip_y*cos(!pi/180.*pa)
      endif
     sxaddpar, h1, 'WCSNAMEA', 'slitmask'
     sxaddpar, h1, 'CUNIT1A', 'mm'
     sxaddpar, h1, 'CUNIT2A', 'mm'
     sxaddpar, h1, 'CTYPE1A', 'BINO_X'
     sxaddpar, h1, 'CTYPE2A', 'BINO_Y'
     sxaddpar, h1, 'CRPIX1A', 2039.47189 ;-1379.82827+3419.30016
     sxaddpar, h1, 'CRPIX2A', 2065.07818
     sxaddpar, h1, 'CRVAL1A', 56.2356 ;0.0
     sxaddpar, h1, 'CRVAL2A', 0.0
     pa_mask=4.41966651e-4
     sxaddpar, h1, 'CD1_1A', 1./(-24.5365617)*cos(pa_mask)
     sxaddpar, h1, 'CD1_2A', 1./(-24.5365617)*sin(pa_mask)
     sxaddpar, h1, 'CD2_1A', -1./24.5481549*sin(pa_mask)
     sxaddpar, h1, 'CD2_2A', 1./24.5481549*cos(pa_mask)
  endif else begin
     sxaddpar, h1, 'WCSNAME', 'disp_mask'
;     sxaddpar, h1, 'CUNIT1', 'Angstrom'
     sxaddpar, h1, 'CUNIT2', 'mm'
     sxaddpar, h1, 'CTYPE1', ''
     sxaddpar, h1, 'CTYPE2', 'BINO_Y'
     sxaddpar, h1, 'CRPIX1', 0.0
     sxaddpar, h1, 'CRPIX2', 2065.07818
     sxaddpar, h1, 'CRVAL1', 0.0
     sxaddpar, h1, 'CRVAL2', 0.0
     sxaddpar, h1, 'CD1_1', 0.0
     sxaddpar, h1, 'CD2_2', 1./24.53542928
  endelse

    imaging_check=sxpar(h2, 'DISPERS2')
    if (typename(imaging_check) eq 'STRING') and (strtrim(imaging_check,2) eq 'mirror') then begin
      coord_check=sxpar(h2, 'RA')
      if coord_check then begin
       crval1=hms2dec(coord_check)*15.0
       crval2=hms2dec(sxpar(h2,'DEC'))
       crpix1=670.152829 ;binospec 0,0 ~ field center
       crpix2=2055.27611  ;2057.72385097
       scale=-1.0/4.08/3600./1.003
       flip_x=1.0
       flip_y=1.0
       pa=flip_y*flip_x*sxpar(h2,'POSANG') ;+180.
       sxaddpar, h2, 'RADECSYS', 'ICRS'
       sxaddpar, h2, 'DATASEC', '[1067:3033,1:4112]'
       sxaddpar, h2, 'CUNIT1', 'deg'
       sxaddpar, h2, 'CUNIT2', 'deg'
       sxaddpar, h2, 'CTYPE1', 'RA---TAN'
       sxaddpar, h2, 'CTYPE2', 'DEC--TAN'
       sxaddpar, h2, 'CRPIX1', crpix1
       sxaddpar, h2, 'CRPIX2', crpix2
       sxaddpar, h2, 'CRVAL1', crval1
       sxaddpar, h2, 'CRVAL2', crval2
       sxaddpar, h2, 'CD1_1', scale*flip_x*cos(!pi/180.*pa)
       sxaddpar, h2, 'CD1_2', scale*flip_x*sin(!pi/180.*pa)
       sxaddpar, h2, 'CD2_1', scale*(-1.0)*flip_y*sin(!pi/180.*pa)
       sxaddpar, h2, 'CD2_2', scale*flip_y*cos(!pi/180.*pa)
      endif
     sxaddpar, h2, 'WCSNAMEA', 'slitmask'
     sxaddpar, h2, 'CUNIT1A', 'mm'
     sxaddpar, h2, 'CUNIT2A', 'mm'
     sxaddpar, h2, 'CTYPE1A', 'BINO_X'
     sxaddpar, h2, 'CTYPE2A', 'BINO_Y'
     sxaddpar, h2, 'CRPIX1A', 2049.437575 ;670.152829+1379.284746
     sxaddpar, h2, 'CRPIX2A', 2055.27611 ;2057.72385097 
     sxaddpar, h2, 'CRVAL1A', -56.2356 ;0.0
     sxaddpar, h2, 'CRVAL2A', 0.0
     pa_mask=-1.45294317e-3
     sxaddpar, h2, 'CD1_1A', 1./(-24.5268966)*cos(pa_mask)
     sxaddpar, h2, 'CD1_2A', 1./(-24.5268966)*sin(pa_mask)
     sxaddpar, h2, 'CD2_1A', -1./24.5354288*sin(pa_mask)
     sxaddpar, h2, 'CD2_2A', 1./24.5354288*cos(pa_mask)
  endif else begin
     sxaddpar, h2, 'WCSNAME', 'disp_mask'
     ;sxaddpar, h2, 'CUNIT1', 'Angstrom'
     sxaddpar, h2, 'CUNIT2', 'mm'
     sxaddpar, h2, 'CTYPE1', ''
     sxaddpar, h2, 'CTYPE2', 'BINO_Y'
     sxaddpar, h2, 'CRPIX1', 0.0
     sxaddpar, h2, 'CRPIX2', 2055.27611
     sxaddpar, h2, 'CRVAL1', 0.0
     sxaddpar, h2, 'CRVAL2', 0.0
     sxaddpar, h2, 'CD1_1', 0.0
     sxaddpar, h2, 'CD2_2', 1./24.53542928 
  endelse
    sxaddpar,h1,'SOFTWARE',version
    sxaddpar,h2,'SOFTWARE',version
    if(keyword_set(mask_bad_pixels)) then begin
        bpix1=mrdfits(getenv('BINO_PIPELINE_PATH')+'calib_Bino/detector/badpix_binospec.fits',1)
        bpix2=mrdfits(getenv('BINO_PIPELINE_PATH')+'calib_Bino/detector/badpix_binospec.fits',2)
        r_mosaic[bpix1.x,bpix1.y]=!values.f_nan
        r_mosaic[bpix2.x+s_mos[1]/2,bpix2.y]=!values.f_nan
        ;; temporary solution
        bx1top=[1447,1622,1781,1782,1787,1835,1942,1943,1945,1947,1965,2913,3312]-1
        for xb=0,n_elements(bx1top)-1 do r_mosaic[bx1top[xb],2056:*]=!values.f_nan
        bx1bot=[3237,3392]-1
        for xb=0,n_elements(bx1bot)-1 do r_mosaic[bx1bot[xb],0:2055]=!values.f_nan
        ;;; partial masking of detector problems (traps) in sideA
        r_mosaic[480,1635:1783]=!values.f_nan
        r_mosaic[366,1670:1711]=!values.f_nan
        r_mosaic[402,1480:1576]=!values.f_nan
        r_mosaic[403,1450:1480]=!values.f_nan
        r_mosaic[404:405,1345:1624]=!values.f_nan
        r_mosaic[406:409,1148:1472]=!values.f_nan
        r_mosaic[410,1037:1150]=!values.f_nan
        r_mosaic[411,925:1011]=!values.f_nan
        r_mosaic[412,790:906]=!values.f_nan
        r_mosaic[413,718:790]=!values.f_nan
        r_mosaic[414,617:722]=!values.f_nan
        r_mosaic[415,545:616]=!values.f_nan
        r_mosaic[1964,323:470]=!values.f_nan
        r_mosaic[2748,349:460]=!values.f_nan
        r_mosaic[3037:3038,0:260]=!values.f_nan
        yr1bad=763+lindgen(1433+1-763)
        xr1bad=fix(2801.0+(2795-2801)*(yr1bad-763)/(1433.-763.))
        r_mosaic[xr1bad,yr1bad]=!values.f_nan

        bx2top=[114,211,2100,3337,3338,4057,4058]-1
        for xb=0,n_elements(bx2top)-1 do r_mosaic[bx2top[xb]+s_mos[1]/2,2056:*]=!values.f_nan
    endif
    mwrfits,r_mosaic[0:s_mos[1]/2-1,*],outfile,h1
    mwrfits,r_mosaic[s_mos[1]/2:*,*],outfile,h2
    ;fix to avoid uncaught mrdfits error if extension does not exist
    fits_open, inpfile, fcb
    n_ext=fcb.nextend
    fits_close, fcb
    if n_ext eq 10 then begin
        mask_a=mrdfits(inpfile,9,h_mask_a,/silent)
        mwrfits,mask_a,outfile,h_mask_a
        mask_b=mrdfits(inpfile,10,h_mask_b,/silent)
        mwrfits,mask_b,outfile,h_mask_b
    endif
end
