function calc_ext,lambda,z
    ;extinction calculation
    a=0.008
    c=0.115
    extin=(a*1./((lambda/10000.)^4.)+c)/2.3
    extin=10.^(-extin/cos(z*!DTOR))
    return,extin
end

function bin_hr_spec,wl_hr,spec_hr,wl_lr
    n_wl=n_elements(wl_lr)
    s_s=size(spec_hr)
    spec_lr=(s_s[0] eq 1)? dblarr(n_wl) : (s_s[0] eq 2)? dblarr(n_wl,s_s[2]) : dblarr(n_wl,s_s[2],s_s[3])
    wl_lr_cur=dblarr(n_wl)+!values.d_nan
    spec_hr_cnt=histogram(wl_hr,min=wl_lr[0]-(wl_lr[1]-wl_lr[0])/2d,$
                                max=wl_lr[n_wl-1]+(wl_lr[n_wl-1]-wl_lr[n_wl-2])/2d,$
                                binsize=(wl_lr[1]-wl_lr[0]),reverse_ind=ind_spec_hr)
    n_bins=(n_elements(spec_hr_cnt) < n_wl)

    for i=1L,n_bins-1L do $
        if((ind_spec_hr[i]-ind_spec_hr[i-1]) gt 0) then begin
            case s_s[0] of
                1 : spec_lr[i-1]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                2 : spec_lr[i-1,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                3 : spec_lr[i-1,*,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*,*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
            endcase
            
            wl_lr_cur[i-1]=total(wl_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
        endif
    g_wl=where(finite(wl_lr_cur) eq 1, cg_wl)
    if(cg_wl eq n_elements(wl_lr_cur)) then return, spec_lr

    case s_s[0] of
        1 : spec_lr=interpol(spec_lr[g_wl],wl_lr_cur[g_wl],wl_lr,/spl)
        2 : for x=0,s_s[2]-1 do $
                spec_lr[*,x]=interpol(spec_lr[g_wl,x],wl_lr_cur[g_wl],wl_lr,/spl)
        3 : for x=0,s_s[2]-1 do $
                for y=0,s_s[3]-1 do $
                    spec_lr[*,x,y]=interpol(spec_lr[g_wl,x,y],wl_lr_cur[g_wl],wl_lr,/spl)
    endcase

    return, spec_lr
end

function bino_measure_throughput,star,absstar,n_slit=n_slit,plot=plot,$
    noext=noext,star_extracted=star_obs,wl_off=wl_off,blue_thr=blue_thr,$
    outputfile=outputfile
;+
; NAME:
;	bino_measure_throughput
; PURPOSE:
;       Binospec throughput measurement
; DESCRIPTION:
;       Binospec throughput measurement using a standard star spectrum
; CALLING SEQUENCE:
;	bino_measure_throughput,'star_slits_lin.fits','calib_Bino/stdstars/fg191b2b_lowres.dat,/plot,n_slit=1
; CATEGORY:
;	Binospec data reduction
; INPUTS:
;	star: linearized rectified stellar spectrum
;       absstar: a filename with the absolute spectrophotometry
; OUTPUTS:
;	a structure containing the throughput + conversion from counts to erg/cm^2/s/A
; OPTIONAL OUTPUT:
;	no
; OPTIONAL INPUT KEYWORDS:
;       n_slit: the slit number to contain the stellar spectrum, defaults to 1
;	plot: plot the result on the screen
; RESTRICTIONS:
;	no
; NOTES:
;	no;
; PROCEDURES USED:
;	Functions :  
;	Procedures:  
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, SAO, 2017/11/11

    if(n_elements(n_slit) ne 1) then n_slit=1
    if(n_elements(wl_off) ne 1) then wl_off=0.0

    h_pri=headfits(star)
    star_lin=mrdfits(star,n_slit,h_star)

    Nx=sxpar(h_star,'NAXIS1')
    Ny=sxpar(h_star,'NAXIS2')

    star_prof=median(star_lin[0.45*nx:0.55*nx,*],dim=1)
    max_prof=max(star_prof,idxmax,/nan)
    w_star=(30 < (Ny-idxmax-1) < idxmax)

    star_lin=star_lin[*,idxmax-w_star:idxmax+w_star]
    Ny=w_star*2+1
    sxaddpar,h_star,'NAXIS2',Ny
    xvec=findgen(Ny)

    star_prof=median(star_lin[0.45*nx:0.55*nx,*],dim=1)
    goodpix=where(finite(star_prof) eq 1,cgood,compl=badpix,ncompl=cbad)
    if(cgood gt 10) then begin
        t=gaussfit(xvec[goodpix],star_prof[goodpix],gg,nterms=4)
        peakpos=gg[1]
    endif

    w_peak=25 < (w_star-3)
    for x=0,nx-1 do begin
        goodpix=where(finite(star_lin[x,*]) eq 1 and abs(xvec-peakpos) le w_peak,cgood,compl=badpix,ncompl=cbad)
        if(cgood gt 10) then begin
            t=gaussfit(xvec[goodpix],transpose(star_lin[x,goodpix]),gg,nterms=4)
            if(cbad gt 0) then begin
                gauss_funct,xvec,gg,gauss_model,pder_model
                star_lin[x,badpix]=gauss_model[badpix]
            endif
            star_lin[x,*]=star_lin[x,*]-gg[3]
        endif else star_lin[x,*]=!values.f_nan
    endfor

    parse_spechdr,h_star,wl=wl
    wl+=wl_off
    wl*=10.0
    dlam=wl-shift(wl,1)

    Texp=sxpar(h_pri,'EXPTIME')
    Z=90.0-float(sxpar(h_pri,'EL'))

    object_name=strcompress(sxpar(h_star,'slitobj'),/remove_all)
    date_obs=strcompress(sxpar(h_pri,'DATE-OBS'))

    gain=1.0
    S=!pi*((650.0/2)^2-(100.0/2)^2) ;total square mirror of telescope in cm^2

    flux_std=read_asc(absstar)
    wl_orig=transpose(flux_std[0,*])
    atm_trans=read_asc(getenv('BINO_PIPELINE_PATH')+'calib_Bino/stdstars/atm_trans_zenith.dat')
    atm_trans_cur=interpol(((1.0/((1.0/transpose(atm_trans[1,*])-1.0)/cos(Z/!radeg)+1.0))),wl_orig,transpose(atm_trans[0,*]))
    ;;;flux_std[1,*]=flux_std[1,*]*transpose(atm_trans_cur)
    flux_orig=transpose(flux_std[1,*])*1e-16
    flux_obswl=interpol(flux_std[1,*],flux_std[0,*],wl)*1e-16
    counts_orig=flux_orig*S*wl_orig*1d-8/(6.625d-27*2.99792458d10) ;;; (wl/5500.0)*2.767e11
    counts_obs=flux_obswl*S*wl*1d-8/(6.625d-27*2.99792458d10)
;    counts_orig=flux_orig*2.8E11*S
;    counts_obs=flux_obswl*2.8E11*S
    star_obs=fltarr(Nx)

    ;computing the stellar spectrum in e-/A/sec
    w=15
    print,'Star: 0%',format='(a,$)'
    perc0=0
    for k=w,Nx-w-1 do begin
        perc=100.0*(k-w+1)/double(Nx-2*w)
        if(perc ge perc0+2) then begin
            perc0=fix(perc)
            if(20*(perc0/20) eq perc0 and perc0 lt 100) then $
                print,string(perc0,format='(i2)')+'%',format='(a,$)' $
            else $
                print,'.',format='(a,$)'
        endif
        star_obs[k]=total(star_lin[k,*],/nan)
    endfor
    print,'100%'

    ;sigma clipping on a star
    med_w=5
    star_med=median(star_obs,med_w)
    goodstar=where(finite(star_obs-star_med) eq 1,compl=badstar,ncompl=cbadstar)
    goodmedstar=where(finite(star_med) eq 1 and star_med gt 0.0,compl=badmedstar,ncompl=cbadmedstar)
    sig_star=robust_sigma((star_obs-star_med)[goodstar])
    star_bad=where((abs(star_obs-star_med) gt 8*sig_star) $
                    or (star_obs eq 0) $
                    or (finite(star_obs) ne 1),b_cnt,comp=star_good)
    if((b_cnt gt 0) and (total(star_good) ge 0)) then begin
        s_g=interpol(smooth(star_obs[star_good],med_w,/nan),star_good,findgen(n_elements(star_obs)))
        star_obs=s_g
    endif

    ext_vec=calc_ext(wl,Z)
    if(n_elements(outputfile) eq 1) then begin
        openw,u,outputfile,/get_lun
        wlgood=where(wl gt 3700 and wl lt 10000,cwlgood)
        printf,u,'   WL(A)       e-          e-/A/s   1/ext'
        for i=wlgood[0],wlgood[cwlgood-1] do printf,u,wl[i],star_obs[i],star_obs[i]*gain/Texp/dlam[i],ext_vec[i],format='(f9.3,2f12.1,f9.5)'
        close,u
        free_lun,u
    endif
    star_obs=star_obs*gain/Texp/(dlam)
    if(cbadmedstar gt 0) then star_obs[badmedstar]=!values.f_nan

    ; correction for the atmospheric extinction
    if(~keyword_set(noext)) then star_obs=star_obs/ext_vec

    ; calculation of the quantum efficiency and spectral sensitivity
    star_wl=bin_hr_spec(wl,star_obs,wl_orig)/atm_trans_cur
    n_wl_lr=n_elements(star_wl)
    dqebin=star_wl/counts_orig

    ; smoothing
    gbin_t=where(finite(dqebin) eq 1,cgbin_t)
    if(gbin_t[0] gt 0) then dqebin[0:gbin_t[0]-1]=dqebin[gbin_t[0]]
    if(gbin_t[cgbin_t-1] lt n_elements(wl_orig)-1) then dqebin[gbin_t[cgbin_t-1]:*]=dqebin[gbin_t[cgbin_t-1]]
    gbin=where(finite(dqebin) eq 1,cgbin,compl=bbin,ncompl=cbbin)

    dqebin1=smooth_lowess((dindgen(n_wl_lr))[gbin],dqebin[gbin],round(n_wl_lr/30.0))

    if(n_elements(blue_thr) ne 1) then blue_thr=4000.0
    blue_wl4000=where(wl_orig lt blue_thr and finite(dqebin) eq 1, cblue_wl4000)
    if(cblue_wl4000 gt 0) then begin
        dqebin1_poly=poly_smooth(dqebin[gbin],round(n_wl_lr/60.0))
        dqebin1[blue_wl4000]=dqebin1_poly[blue_wl4000]
        idx1=cblue_wl4000-1
        idx2=cblue_wl4000-1+10
        ww=findgen(idx2-idx1+1)/(idx2-idx1)
        dqebin1[idx1:idx2]=dqebin1_poly[idx1:idx2]*(1-ww)+dqebin1[idx1:idx2]*ww
    endif

    dqe=interpol(dqebin,wl_orig,wl,/spline)
    dqe_sm=interpol(dqebin1,wl_orig[gbin],wl,/spline)
    good_dqe_sm=where(finite(dqe_sm) eq 1,cgood_dqe_sm)
    if(cgood_dqe_sm gt 0) then dqe[good_dqe_sm]=dqe_sm[good_dqe_sm]

    if(cbadmedstar gt 0) then dqe[badmedstar]=!values.f_nan

    if(keyword_set(plot)) then begin
        plot,wl,dqe*100,xst=1,ys=1,yr=[0,40],xtitle='Wavelength, A',ytitle='Throughput %'
        oplot,wl_orig,star_wl*100/counts_orig,linestyle=1,col=80
        oplot,wl,star_obs*100/counts_obs,linestyle=1,col=128
    endif

    flux_erg=1d/(dqe*S*wl*1d-8/(6.625d-27*2.99792458d10)) ;;;1d/(dqe*2.8E11*S)
    flux_mJy=flux_erg*wl^2*3.3356E7

    dqe_struct={wave:wl,throughput:dqe,c_flux_erg:flux_erg,c_flux_mjy:flux_mJy}

    return,dqe_struct
end
