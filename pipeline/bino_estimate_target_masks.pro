;; usage: mask_slits_individual=bino_estimate_target_masks('obj_slits_lin.fits',fluxthr=1.,relfluxthr=0.1,/moffat)

function bino_estimate_target_masks,filename,$
    fluxthr=fluxthr,relfluxthr=relfluxthr,$
    plot=plot,$
    moffat=moffat,coeff_width=coeff_width,mod_intrelthr=mod_intrelthr,$
    xmin=xmin,xmax=xmax

fits_open, filename, fcb
nslits=fcb.nextend
fits_close, fcb

if(n_elements(fluxthr) ne 1) then fluxthr=3e-20*1d18
if(n_elements(mod_intrelthr) ne 1) then mod_intrelthr=1d5 ;; relative intensity of the Moffat model profile threshold in wings with respect to the flux threshold. 0.1=10%, disabled by default by setting it to 10^5
if(n_elements(relfluxthr) ne 1) then relfluxthr=0.05 ;; 5% of the peak
mslits=replicate({side:'A',slit:-1,width:0,offset:0.0,peakflux:0d,peakbgr:0d},nslits)
if(n_elements(coeff_width) ne 1) then coeff_width=(keyword_set(moffat))? 4.5 : 2.355*1.2
ccdscl=0.24436

for n=1,nslits do begin
    r=mrdfits(filename,n,h)
    print,'n_ext=',n,' side:',sxpar(h,'SIDE'),' slitid=',sxpar(h,'SLITID')
    nx=sxpar(h,'NAXIS1')
    if(n_elements(xmin) ne 1) then xmin=fix(nx*0.35)
    if(n_elements(xmax) ne 1) then xmax=fix(nx*0.65)
    ny=sxpar(h,'NAXIS2')
    yy=dindgen(ny)
    prof=median(r[xmin:xmax,*],dim=1)
    gp=where(finite(prof) eq 1,cgpo)
    if(keyword_set(moffat)) then begin
        moffat=1
        gauss=0
        nterms=6-1
    endif else begin
        moffat=0
        gauss=1
        nterms=5
    endelse
    gg=mpfitpeak(yy[gp[3:cgpo-4]],prof[gp[3:cgpo-4]],par,gauss=gauss,moffat=moffat,nterms=nterms,/positive)
    print,par
    if(keyword_set(moffat)) then begin
        prof_mod_nobgr=par[0]/(((yy-par[1])/par[2])^2+1d)^par[3]
        srcidx=where(prof_mod_nobgr ge (fluxthr < (relfluxthr*par[0])) and prof_mod_nobgr-min(prof_mod_nobgr) ge (fluxthr*mod_intrelthr), csrcidx)
        if(csrcidx gt 0) then begin
            peak_off=ccdscl*((yy[srcidx[0]]+yy[srcidx[csrcidx-1]])/2-(ny-1)/2)
            peak_width=(yy[srcidx[csrcidx-1]]-yy[srcidx[0]])/2
            print,'Peak_off,peak_width,cnt=',peak_off,peak_width,(ny-1)/2+peak_off/ccdscl
            print,'Peak_flux,thr=',max(prof_mod_nobgr),relfluxthr*par[0],fluxthr*mod_intrelthr
        endif else begin
            peak_off=0.0
            peak_width=1
        endelse
    endif else begin
        peak_off=ccdscl*(par[1]-(ny-1)/2)
        peak_width=round(coeff_width*abs(par[2]))
    endelse
    if(keyword_set(plot) and n_elements(gg) gt 8) then begin
        title_pref=(par[0] lt fluxthr or par[1] lt 1 or par[1] gt ny-2)? 'NOT IN THE MASK ' : ''
        plot,prof,title=title_pref+'slitid:'+sxpar(h,'SIDE')+string(sxpar(h,'SLITID')),xs=1,ys=3,yr=[min(gg),max(gg)]
        oplot,yy[gp[3:cgpo-4]],gg,col=254
        oplot,[-peak_width,-peak_width,0,0,peak_width,peak_width]+(ny-1)/2+peak_off/ccdscl,[-1,1,1,-1,-1,1]*1e4,col=128
        ss=''
        read,ss
    endif
    mslits[n-1].side=strcompress(sxpar(h,'SIDE'),/remove_all)
    mslits[n-1].slit=sxpar(h,'SLITID')
    mslits[n-1].width=peak_width ;; round(coeff_width*abs(par[2]))
    mslits[n-1].offset=peak_off ;; 0.24*(par[1]-(ny-1)/2)
    mslits[n-1].peakflux=par[0]
    mslits[n-1].peakbgr=(keyword_set(moffat))? par[4] : par[3]
    if(par[0] lt fluxthr or par[1] lt 1 or par[1] gt ny-2) then mslits[n-1].width=-1
endfor
    
for i=0,nslits-1 do if(mslits[i].width gt 1) then $
    print,'{side:'''+mslits[i].side+''',slit:'+string(mslits[i].slit,format='(i3)')+$
          ',width:'+string(mslits[i].width,format='(i3)')+',offset:'+string(mslits[i].offset,format='(f6.2)')+'},$ '

return, mslits

end

