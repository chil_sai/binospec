;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  binospec 3D grating equation
;  based on the routine by D.Fabricant
;  INPUT
;       x_mask, y_mask -- scalars or vectors of slit positions in the mask plane (mm)
;       wl -- scalar or vector: wavelength in A
;       inangd -- angle of incidence, degrees
;       gpm -- gpm, defaults to 270
;       dphi -- an optional angle tilt for the mask plane in degrees, defaults to 0
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function bino_grateq3d,x_mask_inp,y_mask_inp,wl,inangd,gpm,$
    dphi=dphi,colcamd=colcamd,ord=ord,pixels=pixels,$
    degree=degree,sclcorr=sclcorr,camsclcorr=camsclcorr

    if(n_elements(sclcorr) ne 1) then sclcorr=1d/1.0005d
    if(n_elements(camsclcorr) ne 1) then camsclcorr=1d
    if(n_params() eq 4) then gpm=270d

    if(n_elements(colcamd) ne 1) then colcamd=45d
    if(n_elements(ord) ne 1) then ord=1
    if(n_elements(degree) ne 1) then degree=3

    if(n_elements(dphi) ne 1) then dphi=0d

    n_mask=n_elements(x_mask_inp)
    if(n_elements(y_mask_inp) ne n_mask) then message,'MX and MY must have the same number of elements'
    x_mask=(dphi eq 0d)? x_mask_inp :  x_mask_inp*cos(dphi/!radeg) + y_mask_inp*sin(dphi/!radeg)
    y_mask=(dphi eq 0d)? y_mask_inp : -x_mask_inp*sin(dphi/!radeg) + y_mask_inp*cos(dphi/!radeg)
    n_wl=n_elements(wl)

    out_arr=replicate({x_mask:0d,y_mask:0d,wl:wl,x_det:dblarr(n_wl),y_det:dblarr(n_wl),wlsol:dblarr(degree+1)},n_mask)
    out_arr.x_mask=x_mask_inp
    out_arr.y_mask=y_mask_inp

    ;collimator coefficients
    a=[0.052253986d,-8.0058549d-007,3.6370679d-008,-1.1136857d-007,-4.6633669d-012,5.9133806d-011]*sclcorr
    ;camera coefficients
    b=[7.0444380d,0.00037968341d,-0.00066758479d,0.016357838d,0.026785597d,-0.075174838d,0.24346297d]*camsclcorr

    ;read, prompt='Enter mask X:',mx
    ;read, prompt='Enter mask Y: ',my
    ;read, prompt='Enter wavelength in microns: ',w
    ;read, prompt='Enter grating gpm: ',gpm
    ;read, prompt='Enter angle of incidence (deg): ',inangd

    w=wl/1d4 ;;0.8257
    w_blue=where(w lt 0.59,cw_blue)
    if(cw_blue gt 0) then begin
        dw=0.0/2000.0*(w-0.59)
        w[w_blue]+=dw[w_blue]
    endif
    ;inangd=35.15

    camangd=colcamd-inangd
    camangr=camangd*(1d/!radeg)
    inangr=inangd*(1d/!radeg)
    sig=1000./gpm

    for i=0,n_mask-1 do begin
        mx=x_mask[i]
        my=y_mask[i]
        r=sqrt(mx^2+my^2)

        colangd=a[0]*r+a[1]*r^2+a[2]*r^3+a[3]*(r/(w-0.38)) + $
                a[4]*(r^2/(w-0.38)^2) + a[5]*(r^3/(w-0.38))
        colangr=colangd/!radeg
  
        ;print,'colangd',colangd ;;,colangr

        ;calculate incident direction cosines

        ;we know ry, remember that mask y is z in these coordinates
        ;we know that rx^2+ry^2+rz^2=1, so rx^2+rz^2=1-ry^2
        ;ssumsq is sqrt(rx^2+rz^2)

        ry=cos(colangr)
        ssumsq=sqrt(1d -ry^2)

        ratio=0d
        rzcom=0d
        rz=dblarr(n_wl)
        rx=ssumsq
        if(my ne 0d) then begin
            ratio=-mx/abs(my)
            rzcom=sqrt(ratio^2+1d)
            rz=ssumsq/rzcom
            rx=ratio*rz
        endif

        ;print,'rx:',rx,'ry:',ry,'rz:',rz

        alp= rx*cos(inangr)+ry*sin(inangr)
        bet=-rx*sin(inangr)+ry*cos(inangr)
        gam=rz

        alpp=((ord*w)/sig)-alp
        gamp=-rz
        betp=sqrt(1-alpp^2-gamp^2)

        alpc= alpp*cos(camangr)+betp*sin(camangr)
        betc=-alpp*sin(camangr)+betp*cos(camangr)
        gamc=gamp

        ;print,alp,bet,gam
        ;print,alpp,betp,gamp
        ;print,'alpc:',alpc,'betc:',betc,'gamc:',gamc

        totang=acos(betc)*!radeg
        ta=totang
        totr=b[0]*ta + b[1]*ta^2 + b[2]*ta^3 + $
             b[3]*ta*(w-.7386d)+ b[4]*ta*(w-.7386d)^2 + $
             b[5]*ta*(w-.7386d)^3 + b[6]*ta*(w-.7386d)^4

        ;print,'ta:',ta,'totr:',totr

        ;dx and dy are mm in detector coordinates

        dy=dblarr(n_wl)
        dx=-totr*signum(alpc)
        good_gamc=where(gamc ne 0d, cgood_gamc)
        if(cgood_gamc gt 0) then begin
            dy=-totr/sqrt(1+(alpc/gamc)^2)
            dx=-(alpc/gamc)*dy
            ;dy*=signum(my)
        endif
        ;print,'dx:',dx,'dy:',dy

        ;;sclcorr=1d/1.0005d;;1.0106538d
        out_arr[i].x_det=-dx ;;*sclcorr
        out_arr[i].y_det=dy ;;*sclcorr
    endfor
    if(keyword_set(pixels)) then begin
        out_arr.x_det=2047d +out_arr.x_det/0.015d
        out_arr.y_det=2055d +out_arr.y_det/0.015d ;; 65.9639
    endif
    if(n_wl gt degree+1) then begin
        for i=0,n_mask-1 do begin
            wl_cur=wl
            wl_blue=where(wl_cur lt 5900.,cwl_blue)
            if(cwl_blue gt 0) then begin
                dwl=-0.0/2000.0*(wl_cur-5900.)
                wl_cur[wl_blue]+=dwl[wl_blue]
            endif
            pix_in_range=(keyword_set(pixels))? $
                         where(out_arr[i].x_det gt -500 and out_arr[i].x_det lt 4600, cpix_in_range) : $
                         where(abs(out_arr[i].x_det) lt 2500*0.015d,cpix_in_range)
            if(cpix_in_range lt degree+1) then pix_in_range=lindgen(n_wl)
            out_arr[i].wlsol=poly_fit(out_arr[i].x_det[pix_in_range],wl_cur[pix_in_range],degree)
        endfor
    endif

    return,out_arr
end
