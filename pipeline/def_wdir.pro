function def_wdir,logfile,ext=ext
;+
; NAME:
;	DEF_WDIR
; PURPOSE:
;	DEFINIRION DIRECTORY FOR WRITING DATA 
; DESCRIPTION: 
;	
; CALLING SEQUENCE:
;	Result=def_wdir(logfile)
;
; CATEGORY:
;	CfA MMIRS pipeline
;
; INPUTS:
;	LOGFILE = the pipeline control file
;	
;
; OUTPUTS:
;	Result = string scalar name directory for writing
;		
; OPTIONAL OUTPUT:
;	ext = file extensions
;
; OPTIONAL INPUT KEYWORDS:
;	no	
;
; RESTRICTIONS:
;	no
;
; NOTES:
;	no
;
; PROCEDURES USED:
;	SXPAR
;
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, Observatoire de Paris, Oct 2007
;       Modified by Igor Chilingarian, CfA, Oct 2011
;-

on_error,2

log=readlog(logfile)

result=sxpar(log,'W_DIR')
ext=sxpar(log,'EXT',count=nmatches)
if(nmatches ne 1) then ext=''

return, result
end
