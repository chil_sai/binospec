pro bino_combine,inpfilelist,outfile,norm=norm,nsig=nsig,readn=readn,series=series,n_img_clean=n_img_clean
    n_inp=n_elements(inpfilelist)

    h_0=headfits(inpfilelist[0])
    h_a=headfits(inpfilelist[0],ext=1)
    h_b=headfits(inpfilelist[0],ext=2)
    nx_a=sxpar(h_a,'NAXIS1')
    ny_a=sxpar(h_a,'NAXIS2')
    c_a=dblarr(nx_a,ny_a,n_inp)
    nx_b=sxpar(h_b,'NAXIS1')
    ny_b=sxpar(h_b,'NAXIS2')
    c_b=dblarr(nx_b,ny_b,n_inp)
    exptime_a=0.0
    exptime_b=0.0

    for i=0,n_inp-1 do begin
        c_a[*,*,i]=mrdfits(inpfilelist[i],1,h_a_c)
        exptime_a+=sxpar(h_a_c,'EXPTIME')
        c_b[*,*,i]=mrdfits(inpfilelist[i],2,h_b_c)
        exptime_b+=sxpar(h_b_c,'EXPTIME')
    endfor

    if(keyword_set(norm)) then begin
        n_a=median(c_a[nx_a/2-500:nx_a/2+500,ny_a/2-500:ny_a/2+500,0])
        n_b=median(c_b[nx_b/2-500:nx_b/2+500,ny_b/2-500:ny_b/2+500,0])
        for i=1,n_inp-1 do begin
            n_a_cur=median(c_a[nx_a/2-500:nx_a/2+500,ny_a/2-500:ny_a/2+500,i])
            c_a[*,*,i]=c_a[*,*,i]/n_a_cur*n_a
            n_b_cur=median(c_b[nx_b/2-500:nx_b/2+500,ny_b/2-500:ny_b/2+500,i])
            c_b[*,*,i]=c_b[*,*,i]/n_b_cur*n_b
        endfor
    endif
    sxaddpar,h_a,'EXPTIME',exptime_a
    sxaddpar,h_b,'EXPTIME',exptime_b

    comb_a=(keyword_set(series))? clean_cosmic_stack(c_a,nsig=nsig,readn=readn,out_img_cube=clean_c_a,n_img_clean=n_img_clean): clean_cosmic_stack(c_a,nsig=nsig,readn=readn,out_img_cube=clean_c_a, n_img_clean=n_img_clean)
    comb_b=(keyword_set(series))? clean_cosmic_stack(c_b,nsig=nsig,readn=readn,out_img_cube=clean_c_b,n_img_clean=n_img_clean): clean_cosmic_stack(c_b,nsig=nsig,readn=readn,out_img_cube=clean_c_b, n_img_clean=n_img_clean)

    outfile_all=(keyword_set(series))? strarr(n_inp+1)+outfile : outfile
    if(keyword_set(series)) then begin
        outfile_base=strmid(outfile_all[0],0,strpos(outfile_all[0],'_',/reverse_search))
        outfile_suff=strmid(outfile_all[0],strpos(outfile_all[0],'_',/reverse_search))
        outfile_all[1:*]=outfile_base+'_clean'+string(lindgen(n_inp),format='(i03)')+outfile_suff
    endif

    for i=0,n_elements(outfile_all)-1 do begin
        writefits,outfile_all[i],0,h_0
        if keyword_set(series) and (i gt 0) then begin
           h_a=headfits(inpfilelist[i-1],ext=1)
           h_b=headfits(inpfilelist[i-1],ext=1)
        endif
        mwrfits,((i eq 0)? comb_a : clean_c_a[*,*,i-1]),outfile_all[i],h_a
        mwrfits,((i eq 0)? comb_b : clean_c_b[*,*,i-1]),outfile_all[i],h_b

        fits_open, inpfilelist[0], fcb
        n_ext=fcb.nextend
        fits_close, fcb
        if n_ext eq 4 then begin
            mask_a=mrdfits(inpfilelist[0],3,h_mask_a,/silent)
            mwrfits,mask_a,outfile_all[i],h_mask_a
            mask_b=mrdfits(inpfilelist[0],4,h_mask_b,/silent)
            mwrfits,mask_b,outfile_all[i],h_mask_b
        endif
    endfor
end
