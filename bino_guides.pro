pro bino_guides,center_ra,center_dec,pa,epoch=epoch,mjd=mjd,pmra=pmra,pmdec=pmdec,filter=filter,curr=curr, cds=cds, cfa=cfa, longslit=longslit, sideb=sideb, dy=dy, filewrite=filewrite, do_offset=do_offset, bright=bright, imaging_dx=imaging_dx, ifu=ifu

  if not keyword_set(dy) then dy=0.0 else dy=-1.0*dy/3600.
  if keyword_set(sideb) then dx=-337.28/3600. else dx=337.28/3600.
  if keyword_set(ifu) then begin
     dx=0.0
     imaging_dy=dy
     dy=(-594.21557)/3600.0+imaging_dy
  endif
  if keyword_set(imaging_dx) then dx = dx+1.0*imaging_dx/3600.
  if not keyword_set(center_ra) then begin
     telpos=1
     if keyword_set(curr) then spawn, 'msgget TELESCOPE curr_ra', center_ra else spawn, 'msgget TELESCOPE ra', center_ra
      if strmatch(center_ra, 'server down') then begin
        print, 'telescope server down. Please supply ra,dec,pa manually'
        print, 'bino_guides, RA, DEC, PA'
        return
      endif else center_ra=double(center_ra[0])*15.0d
      if center_ra le -1.0 then begin
         print, 'telescope parked. Please provide an ra, dec'
         return
      endif
  endif else telpos=0
  if not keyword_set(center_dec) then begin
     if keyword_set(curr) then spawn, 'msgget TELESCOPE curr_dec', center_dec else spawn, 'msgget TELESCOPE dec', center_dec
     center_dec=double(center_dec[0])
  endif
  if not keyword_set(mjd) then begin
     spawn, 'msgget TELESCOPE mjd', mjd
          if strmatch(mjd, 'server down') then begin
             mjd=systime(/julian,/utc)-2400000.5d
          endif else mjd=float(mjd[0])
  endif
  if not keyword_set(pa) then begin
     spawn, 'msgget TELESCOPE posang', pa
     pa=double(pa[0])
  endif
  if not keyword_set(epoch) then begin
     if keyword_set(curr) then spawn, 'msgget TELESCOPE curr_epoch', epoch else spawn, 'msgget TELESCOPE epoch', epoch
        if strmatch(epoch, 'server down') then begin
             epoch=2000.0
          endif else epoch=float(epoch[0])
  endif
  
  if not keyword_set(pmra) then pmra=0.0
  if not keyword_set(pmdec) then pmdec=0.0

  if keyword_set(longslit) or keyword_set(ifu) then begin
     center_ra_orig=center_ra
     center_dec_orig=center_dec
     center_ra=center_ra-(dx*cos(pa*!pi/180.)+dy*sin(pa*!pi/180.))/cos(center_dec*!pi/180.)
     center_dec=center_dec-(-1.0*dx*sin(pa*!pi/180.)+dy*cos(pa*!pi/180.))
  endif
  
  gmag_lim=19.0  
  gs_min=0

  if keyword_set(cfa) or keyword_set(cds) then begin
     gaia=QueryVizier('I/345/gaia2',[center_ra,center_dec],30,constraint='Gmag<'+strtrim(string(gmag_lim),2), cfa=cfa)
     gaia[where(finite(gaia.pmra,/nan),/null)].pmra=0.0
     gaia[where(finite(gaia.pmde,/nan),/null)].pmde=0.0
  endif else begin
     sra=string(center_ra,format='(F10.6)')
     sdec=string(center_dec,format='(F10.6)')
     query='select 3600 * q3c_dist(ra, dec, '+sra+', '+sdec+') as rdist, * from gaiaguide where q3c_radial_query(ra, dec, '+sra+', '+sdec+', 30.0 / 60) order by rdist'
     gaia=pgsql_query(query, connect_info='host=dbshare port=5442 user=bino password=safT1st dbname=gaia')
     if isa(gaia, 'STRUCT') then begin
       n_g=n_elements(gaia)
       if n_g ge 1 then gaia=gaia[where(gaia.gmag lt gmag_lim)]
       ;print, 'using local gaia'
     endif
   endelse
  n_g=n_elements(gaia)
guidelist=replicate($
    {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:2015.5d,mag:-1.0,type:'GUIDE',priority:0.0},n_g)

for i=0, n_g-1 do begin
   in=gaia[i]
   out=guidelist[i]
   struct_assign,in,out, /nozero
   guidelist[i]=out
   if keyword_set(cfa) or keyword_set(cds) then begin
     guidelist[i].object=gaia[i].source
     guidelist[i].target=gaia[i].source
     guidelist[i].ra=gaia[i].ra_icrs
     guidelist[i].dec=gaia[i].de_icrs
     guidelist[i].pmdec=gaia[i].pmde
   endif else begin
     guidelist[i].object=gaia[i].source_id
     guidelist[i].target=gaia[i].source_id
   endelse
   guidelist[i].mag=gaia[i].gmag   
endfor
;print, guidelist[0].epoch

;if(n_elements(gs_geometry) ne 1) then gs_geometry=slit_geometry
guide_limits={wfs_mag_lim:15.0, wfs_bright_lim:12.75,guide_mag_lim:gmag_lim,guide_bright_lim:11.5, ng_limit:10}

if keyword_set(bright) then guide_limits.wfs_bright_lim=10.0
gs_geometry={height:10,width:10,gap:0.75}

if keyword_set(ifu) then field_geo_name=getenv('BINO_PIPELINE_PATH')+'../fitmask_idl/config/IFU_geometry.cfg' else field_geo_name=getenv('BINO_PIPELINE_PATH')+'../fitmask_idl/config/field_geometry.cfg'
field_geometry=binospec_read_field_geometry(field_geo_name)

mask_params={ $
        side:'a',$
        label:'none',$
        guide:'Ok',$
        mjdstart:mjd,$
        duration:10d,$
        ra:center_ra,$
        dec:center_dec,$
        ha:0.0,$
        airmass:1.0d,$
        parang:0d,$
        posa:pa,$
        rotator:0d,$
        wavelength:6500.,$
        arc2mm:0.167d,$
        boxw:10,$
        boxh:10,$
        corners:dblarr(4),$
        inst:'bino.mdf',$
        rank:1,$
        filter:'Open',$
        grism:270,$
        telepoly:dblarr(7),$
        telerevs:dblarr(7) $
    }

mask_params.telepoly=[0.0,599.508,7.25994,0.0,73.5279,0.0,-40.5352]
mask_params.telerevs=[0.0,0.00166808,-3.42784e-08,0.0,-9.15714e-13,0.0,-1.88181e-18]
mask_params.corners=field_geometry.mask_a

binospec_expand_mask_params,mask_params


mjd_in=mask_params.mjdstart+mask_params.duration/60d/24d/2d
    j_now=(mjd_in-51544.4d)/365.25d + 2000d
    ra_corr=guidelist.ra+guidelist.pmra*(j_now-guidelist.epoch)/cos(guidelist.dec/!radeg)/(1000d*3600d) ;milliarsec/yr
    dec_corr=guidelist.dec+guidelist.pmdec*(j_now-guidelist.epoch)/(3600d*1000d)
    if keyword_set(longslit) or keyword_set(ifu) then begin
        ;see where the target would go with a simple correction
        ;center_ra=center_ra-337.2/3600.*cos(pa*!pi/180.)
        ;center_dec=center_dec+337.2/3600.*sin(pa*!pi/180.)
       binospec_radec2xy,$
          center_ra_orig,center_dec_orig,$
          mask_params.ra,mask_params.dec,mask_params.posa,$
           mjd=mjd_in,targx,targy
                                ;this won't be quite right, so lets refine using the error
        ;print, targx, targy
        if keyword_set(sideb) then diffx=-56.2356d - targx else diffx=56.2356d - targx    
        if keyword_set(ifu) then diffx=0.0 - targx
        if keyword_set(imaging_dx) then diffx = diffx + imaging_dx * 0.167d
        diffy=dy * 3600. * 0.167d + targy
        dra=(diffx/0.167d/3600.d*cos(!pi/180.0d*pa)+diffy/0.167d/3600.d*sin(!pi/180.d*pa))/cos(mask_params.dec*!pi/180.)
        ddec=-diffx/0.167d/3600.d*sin(!pi/180.0d*pa)+diffy/0.167d/3600.d*cos(!pi/180.d*pa)
        dra_tot=-1.0*(dx*cos(pa*!pi/180.)+dy*sin(pa*!pi/180.))-dra
        ddec_tot=(dx*sin(pa*!pi/180.)-dy*cos(pa*!pi/180.))-ddec
        ;print, dra*3600., ddec*3600
        dx_tot=dra_tot*cos(pa*!pi/180.)-ddec_tot*sin(pa*!pi/180.)
        dy_tot=dra_tot*sin(pa*!pi/180.)+ddec_tot*cos(pa*!pi/180.)
        mask_params.ra=center_ra-dra
        mask_params.dec=center_dec-ddec
        binospec_radec2xy,$
          center_ra_orig,center_dec_orig,$
          mask_params.ra,mask_params.dec,mask_params.posa,$
           mjd=mjd_in,targx,targy
        ;print, center_ra_orig+dra_tot, center_dec_orig+ddec_tot, format='(f11.6)'
        ;print, mask_params.ra, mask_params.dec, format='(f11.6)'
    endif
    binospec_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,$
        mjd=mjd_in,objx,objy
 ;endelse

    
gs_id = where(guidelist.type eq 'GUIDE',n_guidestars)
guidestars=replicate(binospec_guidestar_stub(),n_guidestars)

guidestars.ra=ra_corr[gs_id]
guidestars.dec=dec_corr[gs_id]
guidestars.x=objx[gs_id]
guidestars.xmask=guidestars.x
guidestars.y=objy[gs_id]
guidestars.ymask=guidestars.y;4321.72d*asin(objy[gs_id]/4321.72d)
guidestars.mag=guidelist[gs_id].mag
guidestars.pmra=guidelist[gs_id].pmra
guidestars.pmdec=guidelist[gs_id].pmdec
guidestars.epoch=guidelist[gs_id].epoch
guidestars.object=guidelist[gs_id].object



;new function to select the guide stars
if keyword_set(filter) then begin
   guidestars=binospec_filter_guides(guidestars, mask_params,gs_geometry,field_geometry, guide_limits, gs_min,valid=valid, verbose=verbose)
endif else    guidestars=binospec_filter_guides(guidestars, mask_params,gs_geometry,field_geometry, guide_limits, gs_min,valid=valid, verbose=verbose, /nofilter)

   if (valid eq 0) then begin
      print, 'no good stars found'
      return
   endif
gcam2=guidestars[where((guidestars.wfs eq 1) or (guidestars.wfs eq 3),/null)]
if n_elements(gcam2) ge 1 then gcam2=gcam2[where(gcam2.y lt -97.5,/null)]
if n_elements(gcam2) gt 1 then gcam2=gcam2[sort(gcam2.mag)]  
gcam1=guidestars[where((guidestars.wfs eq 2) or (guidestars.wfs eq 4),/null)]
if n_elements(gcam1) ge 1 then gcam1=gcam1[where(gcam1.y gt 97.5,/null)]
if n_elements(gcam1) gt 1 then gcam1=gcam1[sort(gcam1.mag)]  
wfs=guidestars[where(guidestars.wfs eq 10,/null)]
if n_elements(wfs) gt 1 then wfs=wfs[sort(wfs.mag)]  
if keyword_set(filewrite) then begin
  openw,lun1, '/data/mmti/etc/bino/G1stars.txt',/get_lun
  printf, lun1,'#X Y MAG'
  for i=0,n_elements(gcam1)-1 do printf, lun1, gcam1[i].x,gcam1[i].y,gcam1[i].mag, format='(F8.3," ",F8.3," ",F5.2)'
  close, lun1
  free_lun, lun1
  openw,lun2, '/data/mmti/etc/bino/G2stars.txt',/get_lun
  printf, lun2,'#X Y MAG'
  for i=0,n_elements(gcam2)-1 do printf, lun2,gcam2[i].x,gcam2[i].y,gcam2[i].mag, format='(F8.3," ",F8.3," ",F5.2)'
  close, lun2
  free_lun, lun2
  openw,lunW, '/data/mmti/etc/bino/WFSstars.txt',/get_lun
  printf, lunW,'#X Y MAG'
  for i=0,n_elements(wfs)-1 do printf, lunW, wfs[i].x,wfs[i].y,wfs[i].mag, format='(F8.3," ",F8.3," ",F5.2)'
  close, lunW
  free_lun, lunW
endif else begin
  print, mask_params.ra, mask_params.dec, pa, format='("Guide stars for position: RA ",F11.6," DEC ",F11.6," PA ", F7.2)'
  print, 'Gcam1:'
  print, 'X, Y, MAG, RA, DEC'
  for i=0,n_elements(gcam1)-1 do print, gcam1[i].x,gcam1[i].y,gcam1[i].mag, gcam1[i].ra, gcam1[i].dec
  print, 'Gcam2:'
  print, 'X, Y, MAG, RA, DEC'
  for i=0,n_elements(gcam2)-1 do print, gcam2[i].x,gcam2[i].y,gcam2[i].mag, gcam2[i].ra, gcam2[i].dec
  print, 'WFS:'
  print, 'X, Y, MAG, RA, DEC'
  for i=0,n_elements(wfs)-1 do print, wfs[i].x,wfs[i].y,wfs[i].mag, wfs[i].ra, wfs[i].dec
endelse

if keyword_set(longslit) and not keyword_set(filewrite) then begin
           print, targx, targy, format='("Offset Ra, Dec puts desired target on slit xy: ",F8.4," ", F7.4)'
        ;print, dra_tot*3600., ddec_tot*3600., format='("Please offset by dRA, dDEC: ",F11.6," ", F11.6)'
        print, -1.0*dx_tot*3600., dy_tot*3600., format='("Please offset in InstAz, InstEl: ",F11.6," ", F11.6)'
endif
if keyword_set(longslit) then begin
    openw,lunO, '/data/mmti/etc/bino/LongOffset.txt',/get_lun
    printf, lunO,'#INSTAZ INSTEL'
    printf, lunO, string(-1.0*dx_tot*3600.,format='(F8.3)')+' '+string(dy_tot*3600., format='(F8.3)')
    close, lunO
    free_lun, lunO
    if keyword_set(do_offset) and (telpos eq 1) then begin
        spawn, 'msgcmd TELESCOPE instoff '+string(-1.0*dx_tot*3600.,format='(F8.3)')+' '+string(dy_tot*3600., format='(F8.3)'), result
        ;TBD: check this result
    endif
 endif else file_delete, '/data/mmti/etc/bino/LongOffset.txt', /allow_nonexistent
if keyword_set(ifu) then begin
    openw,lunO, '/data/mmti/etc/bino/IFUOffset.txt',/get_lun
    printf, lunO,'#INSTAZ INSTEL'
    printf, lunO, string(-1.0*dx_tot*3600.,format='(F8.3)')+' '+string(dy_tot*3600., format='(F8.3)')
    close, lunO
    free_lun, lunO
    if keyword_set(do_offset) and (telpos eq 1) then begin
        spawn, 'msgcmd TELESCOPE instoff '+string(-1.0*dx_tot*3600.,format='(F8.3)')+' '+string(dy_tot*3600., format='(F8.3)'), result
        ;TBD: check this result
    endif
 endif else file_delete, '/data/mmti/etc/bino/IFUOffset.txt', /allow_nonexistent
end
