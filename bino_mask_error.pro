;PRO bino_mask_error
; DESCRIPTION: given a mask id (masks.pk), and optionally a time (if
; other than NOW), print the median and max expected slit losses due
; to the difference in refraction across the mask versus the design
; time
; INPUTS: mask_id
; OPTIONAL INPUTS:
;  mjd  - calculate slit losses if mask observed at MJD. If omitted,
;         mjd defaults to NOW
;  hours - calculate for a time X hours from NOW, or X hours from MJD
;          if specified 
; OPTIONAL OUTPUTS:
; maxerr - offset between slit center and target center, perpendicular
;          to slit (Binospec X direction) for the *worst* slit
; mederr - offset between slit center and target center, perpendicular
;          to slit (Binospec X direction) for the *median* slit
;
pro bino_mask_error,mask_id,mjd=mjd,hours=hours, maxerr=maxerr, mederr=mederr, airmass_out=airmass_out

  if mask_id eq !null then begin
     printf, -2, 'No mask id given. Call like: bino_mask_error, <ID>'
     return
  endif
  if not keyword_set(mjd) then begin
     ;spawn, 'msgget TELESCOPE mjd', mjd
      ;    if strmatch(mjd, 'server down') then begin
             mjd=systime(/julian,/utc)-2400000.5d
       ;   endif else mjd=float(mjd[0])
  endif
  if keyword_set(hours) then mjd=mjd+float(hours)/24.0
  
  st='select masks.pk as mask_id,raw_json from binospec_ops.masks where masks.pk='+strtrim(string(mask_id),2)
  dat=pgsql_query(st, connect_info='host=localhost user=bino password=safT1st dbname=binospec')
  if isa(dat, 'STRUCT') then begin
     ndat=n_elements(dat)
     if ndat eq 1 then fullconfig=json_parse(dat[0].raw_json,/tostruct) else return
  endif
  sidea=(fullconfig.sidea)->toarray(/promote_type)
  sideb=(fullconfig.sideb)->toarray(/promote_type)

  n_a=n_elements(sidea)
  n_b=n_elements(sideb)
  n_t=n_a+n_b
  slits=[sidea,sideb]
  n_g=n_elements(fullconfig.guides)
 ; guides=(fullconfig.guides)->toarray(/promote_type)
  poly_x0=fltarr(n_t)
  if n_a ge 1 then for i=0, n_a-1 do poly_x0[i]=((slits[i].polygon).toarray())[0]-56.2356
  if n_b ge 1 then for i=n_a, n_t-1 do poly_x0[i]=((slits[i].polygon).toarray())[0]+56.2356
  target_order=slits.target
  minmaxpri=minmax(slits.priority)
  objlist=replicate($
        {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:0d,mag:-1.0,type:'TARGET',priority:1000.0},n_t)
  for i=0, n_t-1 do begin
     in=slits[i]
   out=objlist[i]
   struct_assign,in,out, /nozero
   objlist[i]=out
   reduced_pri=(objlist[i].priority-minmaxpri[0])/(minmaxpri[1]-minmaxpri[0])*5.0
   objlist[i].priority=100000.0/(10.^(reduced_pri))
  endfor
  n_g=n_elements(fullconfig.guides)
  guidelist=replicate($
    {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:0d,mag:-1.0,type:'GUIDE',priority:0.0},n_g)

  for i=0, n_g-1 do begin
   in=fullconfig.guides[i]
   out=guidelist[i]
   struct_assign,in,out, /nozero
   guidelist[i]=out
  endfor

field_geometry=binospec_read_field_geometry(getenv('BINO_PIPELINE_PATH')+'../fitmask_idl/config/field_geometry.cfg')
mask_params={ $
        side:'a',$
        label:fullconfig.mask_config.label,$
        guide:'Ok',$
        mjdstart:mjd,$
        duration:double(fullconfig.mask_config.duration),$
        ra:fullconfig.mask_config.ra,$
        dec:fullconfig.mask_config.dec,$
        ha:0.0,$
        airmass:1.0d,$
        parang:0d,$
        posa:float(fullconfig.mask_config.posa),$
        rotator:0d,$
        wavelength:float(fullconfig.mask_config.wavelength),$
        arc2mm:0.167d,$
        boxw:float(fullconfig.mask_config.boxw),$
        boxh:float(fullconfig.mask_config.boxh),$
        corners:dblarr(4),$
        inst:'bino.mdf',$
        rank:1,$
        filter:'Open',$
        grism:fullconfig.mask_config.grism,$
        telepoly:dblarr(7),$
        telerevs:dblarr(7) $
    }

mask_params.telepoly=[0.0,599.508,7.25994,0.0,73.5279,0.0,-40.5352]
mask_params.telerevs=[0.0,0.00166808,-3.42784e-08,0.0,-9.15714e-13,0.0,-1.88181e-18]
mask_params.corners=field_geometry.mask_a
gs_min=1
optimize_type=2

binospec_expand_mask_params,mask_params
if (mask_params.airmass lt 1.0) or (mask_params.airmass gt 3.5) then begin
   mederr=!null
   maxerr=!null
   airmass_out=!null
   print, ' '
   print, '********************************'
   print, 'Mask target not above airmass 3.5 at the requested time. Try another time.'
   return
endif

objlist=[objlist,guidelist]
posa_arr=mask_params.posa
ra_arr=mask_params.ra
dec_arr=mask_params.dec
slit_geometry={height:float(fullconfig.slitlen),width:float(fullconfig.slitwidth),gap:0.75}
gs_geometry={height:float(fullconfig.guidewidth),width:float(fullconfig.guidelen),gap:0.75}
;need to fix field_geometry for the guides regions, using requested
;guide box height
gs_boxh=mask_params.arc2mm*gs_geometry.height
for i=0, n_elements(field_geometry.guide[0,*])-1 do begin
   ;field geometry now reflects the allowed *centers* for guide stars at
   ;outer edges, but two tweaks need to be made
   ;For the FCS 'avoid' regions, these need to be tweaked to subtract a
                                ;boxh/2.0 buffer
   ;printf, -2, i
   case (i MOD 2) of
      0:field_geometry.guide[3,i]=field_geometry.guide[3,i]-gs_boxh/2.
      1:field_geometry.guide[1,i]=field_geometry.guide[1,i]+gs_boxh/2.
   endcase
   ;also need to tweak the inner X limits (+-16.136) so that boxes are
   ;actually on the mask 
   case 1 of
      (i lt 4):field_geometry.guide[2,i]-=gs_boxh/2.
      (i ge 4):field_geometry.guide[0,i]+=gs_boxh/2.
   endcase
endfor
b=binospec_mask_optimize(objlist,mask_params,field_geometry,$
        slit_geometry,gs_geometry=gs_geometry,$
        slits_a=slits_a_clean,slits_b=slits_b_clean,guidestars=guidestars,$
                         posa_arr=posa_arr,ra_arr=ra_arr,dec_arr=dec_arr,$
                         optimize_type=optimize_type,obstype='mask', $
                         mask_params_out=mask_params_out,gs_min=gs_min,merit_arr=merit_arr, extend_slits=extend_slits)

;now grab the slit polygons and compare to the originals
if b ne -1 then begin
   if n_elements(slits_a_clean) ge 1 then $
      if n_elements(slits_b_clean) ge 1 then $
         new_poly=[slits_a_clean,slits_b_clean] else $
            new_poly=slits_a_clean else $
               if n_elements(slits_b_clean ge 1) then $
                  new_poly=slits_b_clean else return
   ;can just use one X-val on each slit
   new_target_order=new_poly.target
   diff=fltarr(n_elements(new_target_order))
   for i=0, n_elements(diff)-1 do diff[i]=new_poly[i].polygon[0]-poly_x0[where(new_target_order[i] eq target_order)]
   mederr=median(abs(diff)/0.167)
   maxerr=max(abs(diff)/0.167)
   airmass_out=mask_params.airmass
   print, ' '
   print, '********************************'
   print, 'Mask designed for airmass, HA:', fullconfig.mask_config.airmass, ((fullconfig.mask_config.ha/15.) gt 12.) ? fullconfig.mask_config.ha/15.-24:fullconfig.mask_config.ha/15.
   print, 'Requested observation at airmass, HA:', mask_params.airmass, ((mask_params.ha/15.) gt 12.)?(mask_params.ha/15.-24):(mask_params.ha/15.)
   print, 'Median slit is mis-centered by (arcsec): ',mederr 
   print, 'Worst slit (arcsec): ', maxerr
   print, '********************************'
endif else begin
   mederr=!null
   maxerr=!null
   airmass_out=!null
   print, ' '
   print, '********************************'
   if merit_arr eq -1 then print, 'Mask not observable due to rotator limit. Try a different time'
   if merit_arr eq -2 then print, 'Guide or WFS stars are not suitable at the reuqested time. Try a different time'   
   print, '********************************'
   return
endelse
end
